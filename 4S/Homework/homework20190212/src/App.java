import db.DBInterface;
import view.View;

public class App {
    private DBInterface db;
    private View view;

    public App(DBInterface db, View view) {
        this.db = db;
        this.view = view;
    }


    public String render(int id) {
        return view.showUser(db.get(id));
    }

    public String renderAll() {
        return view.showUsers(db.get());
    }

}
