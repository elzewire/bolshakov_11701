package view.impl;

import db.entities.User;
import view.View;

import java.util.List;

public class HTMLView implements View {
    @Override
    public String showUser(User user) {
        return "<h1>" + user.getUsername() + "</h1>";
    }

    @Override
    public String showUsers(List<User> users) {
        String result = "<table>";
        for (User user : users) {
            result += "<tr>" +
                    wrapTD(user.getUsername()) +
                    wrapTD(user.getCity()) +
                    wrapTD(Integer.toString(user.getYear())) +
                    "</tr>";
        }
        return result + "</table>";
    }

    private String wrapTD(String x) {
        return "<td>" + x + "</td>";
    }

}
