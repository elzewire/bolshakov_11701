package view.impl;

import com.google.gson.Gson;
import db.entities.User;
import view.View;

import java.util.List;

public class JSONView implements View {

    @Override
    public String showUser(User user) {
        Gson gson = new Gson();
        return gson.toJson(user);
    }

    @Override
    public String showUsers(List<User> users) {
        Gson gson = new Gson();
        return gson.toJson(users);
    }
}
