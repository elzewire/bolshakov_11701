package view;

import db.entities.User;

import java.util.List;

public interface View {
    String showUser(User user);
    String showUsers(List<User> users);
}
