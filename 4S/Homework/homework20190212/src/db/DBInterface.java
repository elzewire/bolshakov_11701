package db;

import db.entities.User;

import java.util.List;

public interface DBInterface {
    List<User> get();
    User get(int id);
}
