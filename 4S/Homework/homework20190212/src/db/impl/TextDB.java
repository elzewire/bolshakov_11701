package db.impl;

import db.DBInterface;
import db.entities.User;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class TextDB implements DBInterface {

    private String file = "res/users.txt";

    @Override
    public List<User> get() {
        try {
            List<User> users = new ArrayList<>();
            BufferedReader bf = new BufferedReader(new FileReader(new File(file)));
            String line = bf.readLine();
            while (line != null) {
                users.add(mapUser(line));
                line = bf.readLine();
            }
            return users;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public User get(int id) {
        try {
            BufferedReader bf = new BufferedReader(new FileReader(new File(file)));
            String line = bf.readLine();
            while (line != null) {
                if (line.split(";")[0].equals(Integer.toString(id))) {
                    return mapUser(line);
                } else {
                    line = bf.readLine();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private User mapUser(String line) {
        String[] args = line.split(";");
        return new User(
                args[1],
                args[2],
                Integer.parseInt(args[3])
        );
    }
}
