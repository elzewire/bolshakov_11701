package db.impl;

import db.DBInterface;
import db.entities.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class PostgresDB implements DBInterface {

    private static final String URL = "jdbc:postgresql://localhost:5433/classwork20190212";
    private static final String USERNAME = "postgres";
    private static final String PASSWORD = "Postgres";
    private static Connection conn;

    private static Connection getConn() {
        try {
            Class.forName("org.postgresql.Driver");
            conn = DriverManager.getConnection(
                    URL,
                    USERNAME,
                    PASSWORD
            );
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }

        return conn;
    }

    @Override
    public List<User> get() {
        Connection conn = getConn();
        try {
            PreparedStatement ps = conn.prepareStatement(
                    "SELECT * FROM \"user\";"
            );
            ResultSet rs = ps.executeQuery();
            return mapUsers(rs);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public User get(int id) {
        Connection conn = getConn();
        try {
            PreparedStatement ps = conn.prepareStatement(
                    "SELECT * FROM \"user\" WHERE id = ?;"
            );
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            return mapUser(rs);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    private User mapUser(ResultSet rs) throws SQLException {
        rs.next();
        return new User(
                rs.getString("username"),
                rs.getString("city"),
                rs.getInt("year")
        );
    }

    private List<User> mapUsers(ResultSet rs) {
        List<User> users = new ArrayList<>();
        try {
            User u = null;
            u = mapUser(rs);
            while (u != null) {
                users.add(u);
                u = mapUser(rs);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return users;
    }
}
