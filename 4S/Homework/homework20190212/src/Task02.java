import db.entities.User;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Task02 {
    public static void main(String[] args) {
        System.out.println(getParameters(new User("Edward", "Kazan", 19)));
    }

    public static String getParameters(Object o) {
        try {
            String regex = "get([a-zA-Z0-9]+)";
            Pattern p = Pattern.compile(regex);
            Map<String, Object> fields = new HashMap<>();
            for (Method method : o.getClass().getDeclaredMethods()) {
                String name = method.getName();
                Matcher m = p.matcher(name);
                if (m.matches()) {
                    fields.put(m.group(1).toLowerCase(), method.invoke(o));
                }
            }
            String str = "<table>";
            for (String key : fields.keySet()) {
                str += "<tr>" + wrapTD(key) + wrapTD(fields.get(key).toString()) + "</tr>";
            }
            return str;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private static String wrapTD(String x) {
        return "<td>" + x + "</td>";
    }

}
