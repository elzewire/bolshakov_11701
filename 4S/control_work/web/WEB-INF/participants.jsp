<%@ page import="db.entities.Record" %><%--
  Created by IntelliJ IDEA.
  User: op_23
  Date: 07.03.2019
  Time: 10:42
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Participants</title>
</head>
<body>
    <ul>
    <% for (Record record : records) {%>
        <li><b>${record.getName()}</b> ${record.getCity()} ${record.getEmail()</li>
    <% } %>
    </ul>
</body>
</html>
