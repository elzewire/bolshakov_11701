package controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import services.RegisterService;

@Controller
@RequestMapping("/participants")
public class ParticipantsController {
    private RegisterService registerService;

    public ParticipantsController(RegisterService registerService) {
        this.registerService = registerService;
    }

    @RequestMapping(method = RequestMethod.GET)
    public String registerView(ModelMap model) {
        model.put("records", registerService.get());
        return "participants";
    }
}
