package controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import services.RegisterService;

@Controller
@RequestMapping("/register")
public class RegisterController {
    private RegisterService registerService;

    public RegisterController(RegisterService registerService) {
        this.registerService = registerService;
    }

    @RequestMapping(method = RequestMethod.GET)
    public String registerView(Model model) {
        return "register";
    }

    @RequestMapping(method = RequestMethod.POST)
    public String register(Model model, @RequestParam String name,
                           @RequestParam String city, @RequestParam String email) {
        registerService.register(name, city, email);
        return "redirect:participants";
    }
}
