package services;

import db.DAO;
import db.entities.Record;

import java.util.ArrayList;
import java.util.List;

public class RegisterService {

    private DAO registerDao;

    public RegisterService(DAO registerDao) {
        this.registerDao = registerDao;
    }

    public void register(String name, String city, String email) {
        Record record = new Record(name, city, email);
        //registerDao.create(record);
    }

    public List<Record> get() {
        List<Record> list = new ArrayList<>();
        list.add(new Record("a", "b", "c"));
        return list;
        //return registerDao.get();
    }
}
