package aspects;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.web.bind.annotation.RequestMapping;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

@Aspect
public class Log {
    @After("execution(* controllers.*.*(..)))")
    public void logMethod(JoinPoint jp){
        DateFormat df = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss");
        String path = Arrays.toString(jp.getSignature().getClass().getAnnotation(RequestMapping.class).path());
        String str = "user visited " + path + " at " + df.format(new Date());
        System.out.println(str);
    }
}

