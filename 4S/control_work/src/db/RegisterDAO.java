package db;

import db.entities.Record;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class RegisterDAO implements DAO {
    private static final String URL = "jdbc:postgresql://localhost:5433/control_work";
    private static final String USERNAME = "postgres";
    private static final String PASSWORD = "Postgres";
    private static Connection conn;

    private static Connection getConn() {
        try {
            Class.forName("org.postgresql.Driver");
            conn = DriverManager.getConnection(
                    URL,
                    USERNAME,
                    PASSWORD
            );
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }

        return conn;
    }

    @Override
    public void create(Record record) {
        Connection conn = getConn();
        try {
            PreparedStatement ps = conn.prepareStatement(
                    "INSERT INTO \"record\" (name, city, email) VALUES (?, ?, ?);"
            );
            ps.setString(1, record.getName());
            ps.setString(2, record.getCity());
            ps.setString(3, record.getEmail());
            ps.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<Record> get() {
        Connection conn = getConn();
        try {
            PreparedStatement ps = conn.prepareStatement(
                    "SELECT * FROM \"record\";"
            );
            ResultSet rs = ps.executeQuery();
            return mapRecords(rs);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    private Record mapRecord(ResultSet rs) throws SQLException {
        rs.next();
        return new Record(
                rs.getString("name"),
                rs.getString("city"),
                rs.getString("email")
        );
    }

    private List<Record> mapRecords(ResultSet rs) {
        List<Record> records = new ArrayList<>();
        try {
            Record r = null;
            r = mapRecord(rs);
            while (r != null) {
                records.add(r);
                r = mapRecord(rs);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return records;
    }
}
