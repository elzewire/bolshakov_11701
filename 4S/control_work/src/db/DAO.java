package db;

import db.entities.Record;

import java.util.List;

public interface DAO {
    void create(Record record);
    List<Record> get();
}
