package ru.kpfu.itis.socialnet.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.kpfu.itis.socialnet.domain.Like;
import ru.kpfu.itis.socialnet.domain.Post;
import ru.kpfu.itis.socialnet.domain.Subscription;
import ru.kpfu.itis.socialnet.domain.User;
import ru.kpfu.itis.socialnet.repos.LikeRepo;
import ru.kpfu.itis.socialnet.repos.PostRepo;
import ru.kpfu.itis.socialnet.repos.SubscriptionRepo;

import java.util.HashSet;
import java.util.Set;

@Service
public class PostService {
    @Autowired
    private PostRepo postRepo;
    @Autowired
    private SubscriptionRepo subscriptionRepo;
    @Autowired
    private LikeRepo likesRepo;

    public Set<Post> getFeed(User user) {
        Set<Subscription> subscriptions = subscriptionRepo.getBySubscriber(user);
        Set<User> users = new HashSet<>();
        for (Subscription sub : subscriptions) {
            users.add(sub.getTarget());
        }
        return postRepo.getByAuthorIn(users);
    }

    public Set<Post> getLikedPosts(User user) {
        Set<Like> likes = likesRepo.getByLikeAuthor(user);
        Set<Post> posts = new HashSet<>();
        for (Like l : likes) {
            posts.add(l.getLikedPost());
        }
        return posts;
    }
}
