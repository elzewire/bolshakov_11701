package ru.kpfu.itis.socialnet.repos;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.kpfu.itis.socialnet.domain.Subscription;
import ru.kpfu.itis.socialnet.domain.User;

import java.util.Set;

public interface SubscriptionRepo extends JpaRepository<Subscription, Long> {
    Subscription getByTargetAndSubscriber(User target, User subscriber);
    Set<Subscription> getBySubscriber(User subscriber);
    Set<Subscription> getByTarget(User user);
}
