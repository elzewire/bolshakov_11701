package ru.kpfu.itis.socialnet.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import ru.kpfu.itis.socialnet.domain.Chat;
import ru.kpfu.itis.socialnet.domain.Message;
import ru.kpfu.itis.socialnet.domain.User;
import ru.kpfu.itis.socialnet.repos.ChatRepo;
import ru.kpfu.itis.socialnet.repos.MessageRepo;
import ru.kpfu.itis.socialnet.service.FriendsService;

import javax.validation.Valid;
import java.util.Date;

@Controller
@RequestMapping("/messages")
public class MessageController {

    @Autowired
    private ChatRepo chatRepo;
    @Autowired
    private FriendsService friendsService;
    @Autowired
    private MessageRepo messageRepo;

    @GetMapping
    public String getChats(@AuthenticationPrincipal User user, Model model) {
        model.addAttribute("chats", chatRepo.getByUser1OrUser2(user, user));
        model.addAttribute("friends", friendsService.getFriends(user));
        model.addAttribute("auth_user", user);

        return "chats";
    }

    @PostMapping
    public String createChat(@AuthenticationPrincipal User auth_user, @RequestParam("target") User user) {
        if (chatRepo.getByUser1AndUser2(auth_user, user) != null) {
            return "redirect:/messages/" + chatRepo.getByUser1AndUser2(auth_user, user).getId();
        } else if (chatRepo.getByUser1AndUser2(user, auth_user) != null) {
            return "redirect:/messages/" + chatRepo.getByUser1AndUser2(user, auth_user).getId();
        } else {
            Chat chat = new Chat();
            chat.setUser1(auth_user);
            chat.setUser2(user);
            chat = chatRepo.save(chat);

            return "redirect:/messages/" + chat.getId();
        }
    }

    @GetMapping("/{chat}")
    public String getChat(@AuthenticationPrincipal User user, @PathVariable Chat chat, Model model) {
        model.addAttribute("messages", messageRepo.getByChat(chat));
        User receiver;
        if (chat.getUser1().getId() != user.getId()) {
            receiver = chat.getUser1();
        } else {
            receiver = chat.getUser2();
        }
        model.addAttribute("receiver", receiver);
        model.addAttribute("auth_user", user);
        return "chat_details";
    }

    @PostMapping("/{chat}")
    public String sendMessage(@AuthenticationPrincipal User user, @PathVariable Chat chat,
                              @Valid Message message, BindingResult bindingResult) {
        message.setSender(user);
        message.setChat(chat);
        message.setDatetime(new Date());
        if (!bindingResult.hasErrors()) {
            messageRepo.save(message);
            chat.setLastMessage(message);
            chatRepo.save(chat);
        }
        return "redirect:/messages/" + chat.getId();
    }
}
