package ru.kpfu.itis.socialnet.repos;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.kpfu.itis.socialnet.domain.Chat;
import ru.kpfu.itis.socialnet.domain.User;

import java.util.Set;

public interface ChatRepo extends JpaRepository<Chat, Long> {
    Set<Chat> getByUser1OrUser2(User user1, User user2);
    Chat getByUser1AndUser2(User user1, User user2);
}
