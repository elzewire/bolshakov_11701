package ru.kpfu.itis.socialnet.domain;

import java.util.Date;

public class JsonUser {
    private long id;
    private String username;
    private String password;
    private boolean active;
    private String email;
    private String activationCode;
    private String firstName;
    private String lastName;
    private String status;
    private Date birthDate;
    private String avatar;
    private long defaultAlbum;

    public JsonUser(User user) {
        if (user != null) {
            this.id = user.getId();
            this.username = user.getUsername();
            this.password = user.getPassword();
            this.active = user.isActive();
            this.email = user.getEmail();
            this.activationCode = user.getActivationCode();
            this.firstName = user.getFirstName();
            this.lastName = user.getLastName();
            this.status = user.getStatus();
            this.birthDate = user.getBirthDate();
            if (user.getAvatar() != null) {
                this.avatar = user.getAvatar().getFilename();
            } else {
                this.avatar = "";
            }
            this.defaultAlbum = user.getDefaultAlbum().getId();
        }
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getActivationCode() {
        return activationCode;
    }

    public void setActivationCode(String activationCode) {
        this.activationCode = activationCode;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public long getDefaultAlbum() {
        return defaultAlbum;
    }

    public void setDefaultAlbum(long defaultAlbum) {
        this.defaultAlbum = defaultAlbum;
    }
}
