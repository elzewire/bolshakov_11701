package ru.kpfu.itis.socialnet.controller;

import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import ru.kpfu.itis.socialnet.domain.JsonUser;
import ru.kpfu.itis.socialnet.domain.Post;
import ru.kpfu.itis.socialnet.domain.User;
import ru.kpfu.itis.socialnet.repos.PostRepo;
import ru.kpfu.itis.socialnet.repos.UserRepo;
import ru.kpfu.itis.socialnet.service.PostService;

import java.util.HashSet;
import java.util.Set;

@Controller
@RequestMapping("/search")
public class SearchController {

    @Autowired
    private UserRepo userRepo;
    @Autowired
    private PostRepo postRepo;
    @Autowired
    private PostService postService;

    @GetMapping("/user")
    @ResponseBody
    public String searchUsers(@RequestParam("q") String q) {
        Gson gson = new Gson();
        Set<JsonUser> set = new HashSet<>();
        for (User user : userRepo.findByFirstNameContainsOrLastNameContains(q, q)) {
            set.add(new JsonUser(user));
        }
        return gson.toJson(set);
    }

    @GetMapping("/post")
    public String getFeed(@AuthenticationPrincipal User user, @RequestParam("q") String q,  Model model) {
        Set<Post> posts = postRepo.findByTextContains("#" + q);
        model.addAttribute("posts", posts);
        model.addAttribute("auth_user", user);
        model.addAttribute("liked_posts", postService.getLikedPosts(user));
        return "feed";
    }
}
