package ru.kpfu.itis.socialnet.repos;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.kpfu.itis.socialnet.domain.Chat;
import ru.kpfu.itis.socialnet.domain.Message;

import java.util.Set;

public interface MessageRepo extends JpaRepository<Message, Long> {
    Set<Message> getByChat(Chat chat);
}
