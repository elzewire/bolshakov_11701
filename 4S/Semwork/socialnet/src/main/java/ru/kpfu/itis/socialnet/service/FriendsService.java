package ru.kpfu.itis.socialnet.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.kpfu.itis.socialnet.domain.Subscription;
import ru.kpfu.itis.socialnet.domain.User;
import ru.kpfu.itis.socialnet.repos.SubscriptionRepo;

import java.util.HashSet;
import java.util.Set;

@Service
public class FriendsService {
    @Autowired
    private SubscriptionRepo subscriptionRepo;

    public Set<User> getFriends(User user) {
        Set<Subscription> me = subscriptionRepo.getBySubscriber(user);
        Set<Subscription> onMe = subscriptionRepo.getByTarget(user);
        Set<User> friends = new HashSet<>();

        Set<User> meSubbedOn = getMeSubbedOn(user, me, onMe);
        Set<User> mySubs = getMySubs(user, me, onMe);

        for (User u : meSubbedOn) {
            if (mySubs.contains(u)) {
                friends.add(u);
            }
        }

        return friends;
    }

    public Set<User> getOutcoming(User user) {
        Set<Subscription> me = subscriptionRepo.getBySubscriber(user);
        Set<Subscription> onMe = subscriptionRepo.getByTarget(user);

        Set<User> outcoming = new HashSet<>();

        Set<User> meSubbedOn = getMeSubbedOn(user, me, onMe);
        Set<User> mySubs = getMySubs(user, me, onMe);

        for (User u : meSubbedOn) {
            if (!mySubs.contains(u)) {
                outcoming.add(u);
            }
        }

        return outcoming;
    }

    public Set<User> getIncoming(User user) {
        Set<Subscription> me = subscriptionRepo.getBySubscriber(user);
        Set<Subscription> onMe = subscriptionRepo.getByTarget(user);

        Set<User> incoming = new HashSet<>();

        Set<User> meSubbedOn = getMeSubbedOn(user, me, onMe);
        Set<User> mySubs = getMySubs(user, me, onMe);

        for (User u : mySubs) {
            if (!meSubbedOn.contains(u)) {
                incoming.add(u);
            }
        }

        return incoming;
    }

    private Set<User> getMeSubbedOn(User user, Set<Subscription> me, Set<Subscription> onMe) {
        Set<User> meSubbedOn = new HashSet<>();

        for (Subscription s : me) {
            meSubbedOn.add(s.getTarget());
        }

        return meSubbedOn;
    }

    private Set<User> getMySubs(User user, Set<Subscription> me, Set<Subscription> onMe) {
        Set<User> mySubs = new HashSet<>();

        for (Subscription s : onMe) {
            mySubs.add(s.getSubscriber());
        }

        return mySubs;
    }


}
