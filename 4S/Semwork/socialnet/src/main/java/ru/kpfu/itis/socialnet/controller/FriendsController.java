package ru.kpfu.itis.socialnet.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.kpfu.itis.socialnet.domain.User;
import ru.kpfu.itis.socialnet.repos.SubscriptionRepo;
import ru.kpfu.itis.socialnet.service.FriendsService;

@Controller
@RequestMapping("/friends")
public class FriendsController {

    @Autowired
    private SubscriptionRepo subscriptionRepo;
    @Autowired
    private FriendsService friendsService;

    @GetMapping
    public String getFriends(@AuthenticationPrincipal User user, Model model) {
        model.addAttribute("friends", friendsService.getFriends(user));
        model.addAttribute("incoming", friendsService.getIncoming(user));
        model.addAttribute("outcoming", friendsService.getOutcoming(user));
        model.addAttribute("auth_user", user);
        return "friends";
    }
}
