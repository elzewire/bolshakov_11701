package ru.kpfu.itis.socialnet.domain;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.kpfu.itis.socialnet.service.PostService;

@Controller
@RequestMapping("/feed")
public class FeedController {


    @Autowired
    private PostService postService;

    @GetMapping
    public String getFeed(@AuthenticationPrincipal User user, Model model) {
        model.addAttribute("posts", postService.getFeed(user));
        model.addAttribute("liked_posts", postService.getLikedPosts(user));
        model.addAttribute("auth_user", user);
        return "feed";
    }

}
