package ru.kpfu.itis.socialnet.bot;

import org.telegram.telegrambots.ApiContextInitializer;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardButton;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import ru.kpfu.itis.socialnet.domain.Post;
import ru.kpfu.itis.socialnet.repos.PostRepo;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.BiConsumer;

public class TestSemWorkBot extends TelegramLongPollingBot {

    private Map<String, BiConsumer<SendMessage, Update>> replies;
    private PostRepo postRepo;

    public TestSemWorkBot() {
        replies.put("/start", this::start);
        replies.put("/myname", this::sayMyName);
        replies.put("/getUserPosts", this::getUserPosts);
        replies.put("/mylastname", this::sayLastName);
        replies.put("Inline buttons please!", this::sendInlineButtons);
    }

    private void getUserPosts(SendMessage message, Update update) {
        long id = Long.parseLong(update.getMessage().getText().split(" ")[1]);
        Set<Post> posts = postRepo.getByAuthorId(id);

        StringBuilder msg = new StringBuilder();
        for (Post post : posts) {
            msg.append(post.getDatetime());
            msg.append("\n");
            msg.append(post.getText());
            msg.append("\n");
            msg.append("\n");
        }

        message.setText(msg.toString());
    }

    public static void main(String[] args) {
        ApiContextInitializer.init();
        TelegramBotsApi telegramBotsApi = new TelegramBotsApi();
        try {
            telegramBotsApi.registerBot(new TestSemWorkBot());
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }

    private void start(SendMessage message, Update update) {
        message.setText("Our commands: \n/myname - returns your name;\n/mylastname - returns your lastname");
    }

    private void sayMyName(SendMessage message, Update update) {
        message.setText(update.getMessage().getFrom().getFirstName());
    }

    private void sayLastName(SendMessage message, Update update) {
        String lastname = update.getMessage().getFrom().getLastName();
        if (lastname != null) {
            message.setText(lastname);
        } else {
            message.setText("You have no lastname!");
        }
    }

    private void sendInlineButtons(SendMessage sendMessage, Update update) {
        InlineKeyboardMarkup inlineKeyboardMarkup= new InlineKeyboardMarkup();

        List<InlineKeyboardButton> row1 = new ArrayList<>();
        row1.add(new InlineKeyboardButton().setText("Hello!").setCallbackData("You pressed Hello!"));
        row1.add(new InlineKeyboardButton().setText("Hi!").setCallbackData("You pressed Hi!"));

        List<InlineKeyboardButton> row2 = new ArrayList<>();
        row2.add(new InlineKeyboardButton().setText("Bye!").setCallbackData("You pressed Bye!"));

        List<List<InlineKeyboardButton>> keyboard = new ArrayList<>();
        keyboard.add(row1);
        keyboard.add(row2);
        inlineKeyboardMarkup.setKeyboard(keyboard);
        sendMessage.setText("Choose:").setReplyMarkup(inlineKeyboardMarkup);
    }

    private synchronized void setButtons(SendMessage sendMessage) {
        ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
        sendMessage.setReplyMarkup(replyKeyboardMarkup);

        KeyboardRow keyboardFirstRow = new KeyboardRow() {{
            add(new KeyboardButton("Inline buttons please!"));
        }};

        KeyboardRow keyboardSecondRow = new KeyboardRow() {{
            add(new KeyboardButton("Help!"));
        }};

        List<KeyboardRow> keyboard = new ArrayList<>();
        keyboard.add(keyboardFirstRow);
        keyboard.add(keyboardSecondRow);

        replyKeyboardMarkup.setKeyboard(keyboard);
    }

    @Override
    public void onUpdateReceived(Update update) {
        SendMessage message = new SendMessage();
        if (update.hasMessage()) {
            message.setChatId(update.getMessage().getChatId());

            String command = update.getMessage().getText();
            final BiConsumer<SendMessage, Update> replier = replies.get(command);
            if (replier != null) {
                replier.accept(message, update);
            }

            try {
                execute(message);
            } catch (TelegramApiException e) {
                e.printStackTrace();
            }
        }

        if (update.hasCallbackQuery()) {
            try {
                execute(message.setText(update.getCallbackQuery().getData())
                        .setChatId(update.getCallbackQuery().getMessage().getChatId()));
            } catch (TelegramApiException e) {
                e.printStackTrace();
            }
        }
    }


    @Override
    public String getBotUsername() {
        return "cortana_semwork_itis_bot";
    }

    @Override
    public String getBotToken() {
        return "646045651:AAFEFKyEIHtuCEU86jGZt6yZ2Bf6TXKHhZ4";
    }
}
