package ru.kpfu.itis.socialnet.repos;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.kpfu.itis.socialnet.domain.Like;
import ru.kpfu.itis.socialnet.domain.Post;
import ru.kpfu.itis.socialnet.domain.User;

import java.util.Set;

public interface LikeRepo extends JpaRepository<Like, Long> {
    Like getByLikeAuthorAndLikedPost(User author, Post post);
    void deleteById(Long id);
    Set<Like> getByLikeAuthor(User user);
}
