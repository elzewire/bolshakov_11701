function search(input) {
    if (input.value.length > 0) {
        $("#query-results").dropdown('show');
        $.ajax({
            type: "get",
            url: "/search/user",
            data: {"q" : input.value},
            dataType: "text",
            success: function(msg) {
                $("#query-results").empty();
                JSON.parse(msg).forEach(function (entry) {
                    var a = document.createElement('a');
                    a.href = "/profile/" + entry.id;
                    a.innerHTML = entry.firstName + " " + entry.lastName;
                    a.classList.add("dropdown-item");
                    $("#query-results").append(a)
                })
            }
        })
    } else {
        $("#query-results").dropdown('hide');
    }
}
