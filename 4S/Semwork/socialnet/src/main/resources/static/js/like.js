function like(el) {
    var url = el.name;

    var token = $("input[name='_csrf']").attr("value");

    $.ajax({
        url: url,
        type: 'post',
        data: {},
        beforeSend: function(xhr) {
            xhr.setRequestHeader("X-CSRF-TOKEN", token);
        },
        success: function () {
            count = el.nextSibling;
            console.log(count);
            if (el.classList.contains('liked')) {
                count.innerHTML = parseInt(count.innerHTML) - 1;
                el.classList.remove('liked');
            } else {
                count.innerHTML = parseInt(count.innerHTML) + 1;
                el.classList.add('liked');
            }
        }
    })
}