<#import "common/base_menu.ftl" as bm>

<@bm.head>
    <link rel="stylesheet" href="/static/css/image-picker.css">
    <link rel="stylesheet" href="/static/css/profile.css">
    <script type="text/javascript" src="/static/js/image-picker.js"></script>
    <title>Товарищи</title>
</@>

<@bm.body>
    <div class="col-10 friends">
        <div class="row my-3">
            <div class="col">
                <div class="friends segment p-3 bg-light">
                    <div class="row p-3">
                        <div class="col">
                            <h6>Товарищи:</h6>
                            <#if friends?has_content>
                                <#list friends?chunk(4) as row>
                                <div class="row">
                                    <#list row as cell>
                                        <div class="col-3 p-2">
                                            <#if cell.avatar??>
                                                <img class="rounded rounded-circle my-auto" src="/img/${cell.avatar.filename}" width="60" height="60" alt="">
                                            <#else>
                                                <img class="rounded rounded-circle my-auto" src="/static/res/default.png" width="60" height="60" alt="">
                                            </#if>
                                            <a href="/profile/${cell.id}" class="text-danger stretched-link">${cell.firstName} ${cell.lastName}</a>
                                        </div>
                                    </#list>
                                </div>
                                </#list>
                            <#else>
                                <div>Нет товарищей</div>
                            </#if>
                        </div>
                    </div>
                    <hr>
                    <div class="row p-3">
                        <div class="col">
                            <h6>Подписчики:</h6>
                            <#if incoming?has_content>
                                <#list incoming?chunk(4) as row>
                                <div class="row">
                                    <#list row as cell>
                                        <div class="col-3 p-2">
                                            <#if cell.avatar??>
                                                <img class="rounded rounded-circle my-auto" src="/img/${cell.avatar.filename}" width="60" height="60" alt="">
                                            <#else>
                                                <img class="rounded rounded-circle my-auto" src="/static/res/default.png" width="60" height="60" alt="">
                                            </#if>
                                            <a href="/profile/${cell.id}" class="text-danger stretched-link">${cell.firstName} ${cell.lastName}</a>
                                        </div>
                                    </#list>
                                </div>
                                </#list>
                            <#else>
                                <div>Нет подписчиков</div>
                            </#if>
                        </div>
                    </div>
                    <hr>
                    <div class="row p-3">
                        <div class="col">
                            <h6>Подписки:</h6>
                            <#if outcoming?has_content>
                                <#list outcoming?chunk(4) as row>
                                <div class="row">
                                    <#list row as cell>
                                        <div class="col-3 p-2">
                                            <#if cell.avatar??>
                                                <img class="rounded rounded-circle my-auto" src="/img/${cell.avatar.filename}" width="60" height="60" alt="">
                                            <#else>
                                                <img class="rounded rounded-circle my-auto" src="/static/res/default.png" width="60" height="60" alt="">
                                            </#if>
                                            <a href="/profile/${cell.id}" class="text-danger stretched-link">${cell.firstName} ${cell.lastName}</a>
                                        </div>
                                    </#list>
                                </div>
                                </#list>
                            <#else>
                                <div>Нет подписок</div>
                            </#if>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript" src="/static/js/profile.js"></script>
</@bm.body>