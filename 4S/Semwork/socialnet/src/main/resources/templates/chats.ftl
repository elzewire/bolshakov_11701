<#import "common/base_menu.ftl" as bm>

<@bm.head>
    <link rel="stylesheet" href="/static/css/profile.css">
    <title>Почтовые ящики</title>
</@>

<@bm.body>
    <div class="col-10">
        <div class="row my-3">
            <div class="col">
                <div class="segment p-3 bg-light">
                    <div class="mb-3">
                        <button class="btn btn-block edit text-danger" data-toggle="modal" data-target="#chats-modal">Создать почтовый ящик</button>
                    </div>
                    <#list chats as chat>
                        <div class="row mt-1">
                            <div class="col-1">
                                <#if chat.user1.id == auth_user.id>
                                    <#if chat.user2.avatar??>
                                        <img class="user-photo rounded rounded-circle my-auto" src="/img/${chat.user2.avatar.filename}" width="60" height="60" alt="">
                                    <#else>
                                        <img class="user-photo rounded rounded-circle my-auto" src="/static/res/default.png" width="60" height="60" alt="">
                                    </#if>
                                <#else>
                                    <#if chat.user1.avatar??>
                                        <img class="user-photo rounded rounded-circle my-auto" src="/img/${chat.user1.avatar.filename}" width="60" height="60" alt="">
                                    <#else>
                                        <img class="user-photo rounded rounded-circle my-auto" src="/static/res/default.png" width="60" height="60" alt="">
                                    </#if>
                                </#if>
                            </div>
                            <div class="col-9 pt-2">
                                <#if chat.user1.id == auth_user.id>
                                    <a href="/messages/${chat.id}" class="text-danger stretched-link font-weight-bold">${chat.user2.firstName} ${chat.user2.lastName}</a>
                                <#else>
                                    <a href="/messages/${chat.id}" class="text-danger stretched-link font-weight-bold">${chat.user1.firstName} ${chat.user1.lastName}</a>
                                </#if>
                                <#if chat.lastMessage??>
                                    <div class="text-secondary">${chat.lastMessage.text}</div>
                                </#if>
                            </div>
                            <div class="col-1 pl-0 pt-2">
                                <#if chat.lastMessage??>
                                    <div class="text-secondary">${chat.lastMessage.datetime}</div>
                                </#if>
                            </div>
                        </div>
                        <hr>
                    <#else>
                        <div class="text-center">Нет почтовых ящиков</div>
                    </#list>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="chats-modal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Создать чат</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="album-form" method="post" action="/messages">
                        <input type="hidden" name="_csrf" value="${_csrf.token}">
                        <div class="form-group">
                            <label for="target">Выберите товарища</label>
                            <select name="target" id="target">
                                <#list friends as friend>
                                    <option value="${friend.id}">${friend.firstName} ${friend.lastName}</option>
                                </#list>
                            </select>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Отмена</button>
                    <button type="submit" class="btn btn-danger" form="album-form">Добавить</button>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript" src="/static/js/profile.js"></script>
</@bm.body>