<#import "common/base_menu.ftl" as bm>

<@bm.head>
    <link rel="stylesheet" href="/static/css/profile.css">
    <link rel="stylesheet" href="/static/css/messages.css">
    <title>Телеграммы</title>
</@>

<@bm.body>
    <div class="col-10">
        <div class="row my-3">
            <div class="col">
                <div class="modal-dialog modal-dialog-scrollable modal-xl">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h6 class="modal-title">${receiver.firstName} ${receiver.lastName}</h6>
                            <#if receiver.avatar??>
                                <img class="nav-avatar rounded rounded-circle my-auto" src="/img/${receiver.avatar.filename}" width="30" height="30" alt="">
                            <#else>
                                <img class="nav-avatar rounded rounded-circle my-auto" src="/static/res/default.png" width="30" height="30" alt="">
                            </#if>
                        </div>
                        <div class="modal-body">
                            <#list messages?sort_by("datetime") as message>
                                <div class="row mt-1">
                                    <div class="col-1">
                                        <#if message.sender.avatar??>
                                            <img class="user-photo rounded rounded-circle my-auto" src="/img/${message.sender.avatar.filename}" width="60" height="60" alt="">
                                        <#else>
                                            <img class="user-photo rounded rounded-circle my-auto" src="/static/res/default.png" width="60" height="60" alt="">
                                        </#if>
                                    </div>
                                    <div class="col-9 pt-2">
                                        <a href="/profile/${message.sender.id}" class="text-danger stretched-link font-weight-bold">${message.sender.firstName} ${message.sender.lastName}</a>
                                        <div>${message.text}</div>
                                    </div>
                                    <div class="col-1 pl-0 pt-2">
                                        <div class="text-secondary">${message.datetime}</div>
                                    </div>
                                </div>
                                <hr>
                            <#else>
                                <div class="text-center">Нет телеграмм</div>
                            </#list>
                        </div>
                        <div class="modal-footer">
                            <div class="col">
                                <form method="post">
                                    <input type="hidden" name="_csrf" value="${_csrf.token}">
                                    <div class="form-group row">
                                        <div class="col-11">
                                            <input type="text" name="text" class="form-control w-100" id="text" placeholder="Начните набор текста Вашей телеграммы...">
                                        </div>
                                        <div class="col-1">
                                            <label for="submit" class="mx-2 post-item text-danger text-decoration-none">&#128233;</label>
                                            <button type="submit" id="submit" style="display: none;"></button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript" src="/static/js/profile.js"></script>
</@bm.body>