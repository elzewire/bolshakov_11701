/**
* @author Edward Bolshakov
* 11-701
* Problem Set 3 Task 01 - 02
**/

import java.util.Scanner;
import java.util.Random;

public class Game {

	public static void start() {

		Scanner sc = new Scanner(System.in);
		Random random = new Random();

		System.out.println("(Enter -1 instead of power to exit)");

		System.out.print("Player 1, enter your name: ");
		String name1 = sc.nextLine();

		System.out.print("PLayer 2, enter your name: ");
		String name2 = sc.nextLine();
		
		Player p1 = new Player(name1);
		Player p2 = new Player(name2);

		int turn = 1;
		int maxPower = 9;

		while (p1.getHp() > 0 && p2.getHp() > 0) {

			System.out.print(p1.getName() + "'s hp: " + p1.getHp() + "  ");
			System.out.println(p2.getName() + "'sc hp: " + p2.getHp());
			System.out.println();

			if (turn > 0) {
				System.out.print(p1.getName() + ", enter the power of hit(1-9): ");				
			} else {
				System.out.print(p2.getName() + ", enter the power of hit(1-9): ");
			}
			int power = sc.nextInt();

			if (power < 1 && power != -1 || power > 9) {
				System.out.println("Wrong input!\n");
				continue;
			} else if (power == -1) {
				break;
			}

			double amount = (double)((maxPower + 1.0) - ((maxPower + 1.0) / power));
			double chance = random.nextDouble() * 10;

			if (amount <= chance) {
				if (turn > 0) {
					p1.punch(p2, power);
				} else {
					p2.punch(p1, power);
				}
			} else {
				System.out.println("You've missed!\n");
			}

			turn *= -1;

		}

		if (p1.getHp() <= 0) {
			System.out.println("PLayer " + p2.getName() + " wins!");
		} else if (p2.getHp() <= 0) {
			System.out.println("PLayer " + p1.getName() + " wins!");
		}

	}

}