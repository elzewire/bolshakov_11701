/**
* @author Edward Bolshakov
* 11-701
* Problem Set 3 Task 01 - 02
**/

public class Main {
	
	public static void main(String [] args) {

		Game game = new Game();
		game.start();

	}

}