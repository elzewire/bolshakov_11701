/**
* @author Edward Bolshakov
* 11-701
* Problem Set 3 Task 07
**/
package ProblemSet3.Task07PS3;

import ProblemSet3.Task04PS3.RationalFraction;
import ProblemSet3.Task06PS3.RationalVector2D;

public class RationalMatrix2x2 {

	private final int N = 2;
	private RationalFraction[][] mat = new RationalFraction[N][N];

	public RationalMatrix2x2(RationalFraction[][] mat) { 
		this.mat = mat;
	}

	public RationalMatrix2x2(RationalFraction x) {
		for (int i = 0; i < N; i++) {
			for (int j = 0; j < N; j++) {
				mat[i][j] = x;
			}
		}
	}

	public RationalMatrix2x2() {
		this(new RationalFraction());
	}

	public RationalMatrix2x2 add(RationalMatrix2x2 matrix) {
		RationalFraction[][] newMat = new RationalFraction[N][N];
		for (int i = 0; i < N; i++) {
			for (int j = 0; j < N; j++) {
				newMat[i][j] = mat[i][j].add(matrix.getMat()[i][j]);
			}
		}

		return new RationalMatrix2x2(newMat);
	}

	public RationalMatrix2x2 mult(RationalMatrix2x2 matrix) {
		RationalFraction[][] newMat = new RationalFraction[N][N];
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
            	newMat[i][j] = new RationalFraction();
                for (int k = 0; k < N; k++) {
                    newMat[i][j].add(mat[i][k].mult(matrix.getMat()[k][j])); 
                }
            }
        }
		return new RationalMatrix2x2(newMat);
	}

	public double det() {
		return ((mat[0][0].mult(mat[1][1])).sub(mat[0][1].mult(mat[1][0]))).value();
	}

	public RationalVector2D multVector(RationalVector2D rv) {
		RationalFraction x = (mat[0][0].mult(rv.getX())).add(mat[0][1].mult(rv.getY()));
		RationalFraction y = (mat[1][0].mult(rv.getX())).add(mat[1][1].mult(rv.getY()));
		return new RationalVector2D(x, y);
	}

	public RationalFraction[][] getMat() {
		return mat;
	}

	public String toString() {
		String arr = "";
		for (int i = 0; i < N; i++) {
			for (int j = 0; j < N; j++) {
				arr += mat[i][j] + " ";
			}
			arr += "\n";
		}

		return arr;
	}

	public static void main(String[] args) {
		
		RationalFraction rf1 = new RationalFraction(1, 2);
		RationalFraction rf2 = new RationalFraction(-1, 4);
		RationalFraction rf3 = new RationalFraction(3, 4);


		RationalMatrix2x2 mat1 = new RationalMatrix2x2(rf1);
		RationalMatrix2x2 mat2 = new RationalMatrix2x2(rf2);
		RationalMatrix2x2 mat3 = new RationalMatrix2x2();

		RationalVector2D vec = new RationalVector2D(rf2, rf3);

		System.out.println(mat3);
		System.out.println(mat1.add(mat2));
		System.out.println(mat1.mult(mat2));
		System.out.println(mat1.multVector(vec));
		System.out.println(mat1.det());

	}

}
