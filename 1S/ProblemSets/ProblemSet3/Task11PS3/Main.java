/**
* @author Edward Bolshakov
* 11-701
* Problem Set 3 Task 11
**/

package ProblemSet3.Task11PS3;

import ProblemSet3.Task11PS3.tiles.Tile;
import ProblemSet3.Task11PS3.tiles.WaterTile;
import ProblemSet3.Task11PS3.tiles.DirtTile;

public class Main {

	public static void main(String[] args) {
		
		int n = 10;
		Tile[] tiles = new Tile[n];

		for (int i = 0; i < n; i++) {
			if (i > 4) {
				tiles[i] = new WaterTile("FLOW", "VISCOUS_SQUARE", false);
			} else {
				tiles[i] = new DirtTile("", "SOLID_SQUARE", true);
			}
		}

		for (int i = 0; i < n; i++) {
			tiles[i].onCollision();
			tiles[i].onGameStep();
			tiles[i].onDestruction();
		}

	}

}