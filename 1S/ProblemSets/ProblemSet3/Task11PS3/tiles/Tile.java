/**
* @author Edward Bolshakov
* 11-701
* Problem Set 3 Task 11
**/

package ProblemSet3.Task11PS3.tiles;

abstract public class Tile {

	protected boolean destructable;

	//protected Animation animation;
	//protected CollisionMask colMask;

	abstract public void onDestruction();
	abstract public void onGameStep();
	abstract public void onCollision();

}


