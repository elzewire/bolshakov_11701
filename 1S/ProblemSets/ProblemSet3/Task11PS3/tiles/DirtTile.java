/**
* @author Edward Bolshakov
* 11-701
* Problem Set 3 Task 11
**/

package ProblemSet3.Task11PS3.tiles;

import ProblemSet3.Task11PS3.tiles.Tile;

public class DirtTile extends Tile {

	private String tilename;
	private final int ID = 2;

	public DirtTile(String animation, String colMask, boolean destructable) {
		this.destructable = destructable;
		tilename = "Dirt";

		/*if (!animation.equals("")) {
			this.animation = new Animation(animation);
		} else {
			this.animation = new Animation();
		}

		if (!colMask.equals("")) {
			this.colMask = new CollisionMask(colMask);
		} else {
			this.colMask = new CollisionMask();
		}*/
	}

	public void /*Entity*/ onDestruction() {
		if (destructable) {
			//return new Entity(ID);
			System.out.println(tilename + " destroyed");
		}
	}

	public void onGameStep() {
		//animation.updateFrame();
		System.out.println(tilename + " frame updated");
	}

	public void onCollision(/*Entity entity*/) {
		//CollisionMask other = entity.getColMask();
		//colMask.solidPush(other);
		System.out.println("Something collided with " + tilename);
	}

}
