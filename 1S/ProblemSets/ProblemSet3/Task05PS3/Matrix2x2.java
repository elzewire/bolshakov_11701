/**
* @author Edward Bolshakov
* 11-701
* Problem Set 3 Task 05
**/

package ProblemSet3.Task05PS3;

import ProblemSet3.Task03PS3.Vector2D;
import ProblemSet3.Task05PS3.MatrixException;

public class Matrix2x2 {

	public static double pow(double x, int deg) {
		int r = 1;
		for (int i = 0; i < deg; i++) {
			r *= x;
		}
		return r;
	}

	private final int N = 2;
	private double[][] mat = new double[N][N];

	public Matrix2x2(double x1, double x2, double x3, double x4) {
		mat = new double[N][N];
		mat[0][0] = x1;
		mat[0][1] = x2;
		mat[1][0] = x3;
		mat[1][1] = x4;
	}

	public Matrix2x2(double[][] mat) {
		this.mat = mat;
	}

	public Matrix2x2(double x) {
		for (int i = 0; i < N; i++) {
			for (int j = 0; j < N; j++) {
				mat[i][j] = x;
			}
		}
	}

	public Matrix2x2() {
		this(0);
	}

	public Matrix2x2 add(Matrix2x2 matrix) {
		double[][] newMat = new double[N][N];
		for (int i = 0; i < N; i++) {
			for (int j = 0; j < N; j++) {
				newMat[i][j] = mat[i][j] + matrix.getMat()[i][j];
			}
		}

		return new Matrix2x2(newMat);
	}

	public Matrix2x2 sub(Matrix2x2 matrix) {
		double[][] newMat = new double[N][N];
		for (int i = 0; i < N; i++) {
			for (int j = 0; j < N; j++) {
				newMat[i][j] = mat[i][j] - matrix.getMat()[i][j];
			}
		}

		return new Matrix2x2(newMat);
	}

	public Matrix2x2 mult(double mult) {
		double[][] newMat = new double[N][N];
		for (int i = 0; i < N; i++) {
			for (int j = 0; j < N; j++) {
				newMat[i][j] = mat[i][j] * mult;
			}
		}

		return new Matrix2x2(newMat);
	}

	public Matrix2x2 mult(Matrix2x2 matrix) {
		double[][] newMat = new double[N][N];
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                for (int k = 0; k < N; k++) {
                    newMat[i][j] += mat[i][k] * matrix.getMat()[k][j]; 
                }
            }
        }
		return new Matrix2x2(newMat);
	}

	public double det() {
		return (mat[0][0] * mat[1][1]) - (mat[0][1] * mat[1][0]);
	}

	public void transpose() {
		double temp = mat[0][1];
		mat[0][1] = mat[1][0];
		mat[1][0] = temp;
	}

	public Matrix2x2 inverseMatrix() throws MatrixException {
		double det = this.det();
		double[][] newMat = new double[N][N];

		if (det != 0) {
        	for (int i = 0; i < N; i++) {
        		for (int j = 0; j < N; j++) {
        			newMat[j][i] = (double)mat[1 - i][1 - j] * pow(-1, i + j) / det;
        		}
        	}
		} else {
			throw new MatrixException("Determinant equals 0");
		}

		return new Matrix2x2(newMat);
	}

	public Vector2D multVector(Vector2D v) {
		double x = mat[0][0] * v.getX() + mat[0][1] * v.getY();
		double y = mat[1][0] * v.getX() + mat[1][1] * v.getY();
		return new Vector2D(x, y);
	}

	public double[][] getMat() {
		return mat;
	}

	public String toString() {
		String arr = "";
		for (int i = 0; i < N; i++) {
			for (int j = 0; j < N; j++) {
				arr += mat[i][j] + " ";
			}
			arr += "\n";
		}

		return arr;
	}

	public static void main(String[] args) throws MatrixException {
		
		Matrix2x2 mat1 = new Matrix2x2(1, 2, 3, 4);
		Matrix2x2 mat2 = new Matrix2x2(1, 0, 0, 1);
		Matrix2x2 mat3 = new Matrix2x2(3);
		Matrix2x2 mat4 = new Matrix2x2();

		Vector2D vec = new Vector2D(1, 2);

		System.out.println(mat3);
		System.out.println(mat4);
		System.out.println(mat1.add(mat3));
		System.out.println(mat1.sub(mat3));
		System.out.println(mat1.mult(2));
		System.out.println(mat1.mult(mat2));
		System.out.println(mat1.inverseMatrix());
		System.out.println(mat1.multVector(vec));
		mat1.transpose();
		System.out.println(mat1);

	}

}	
