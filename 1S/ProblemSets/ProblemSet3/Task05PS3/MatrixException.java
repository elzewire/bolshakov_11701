/**
* @author Edward Bolshakov
* 11-701
* for Problem Set 3 Task 05
**/

package ProblemSet3.Task05PS3;

public class MatrixException extends Exception {

	public MatrixException(String message) {
		super(message);
	}

}