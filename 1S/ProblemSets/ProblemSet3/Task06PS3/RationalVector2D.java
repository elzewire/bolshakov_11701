/**
* @author Edward Bolshakov
* 11-701
* Problem Set 3 Task 06
**/
package ProblemSet3.Task06PS3;

import ProblemSet3.Task04PS3.RationalFraction;

public class RationalVector2D {

	private RationalFraction x;
	private RationalFraction y;

	public RationalVector2D(RationalFraction x, RationalFraction y) {
		this.x = x;
		this.y = y;
	}

	public RationalVector2D() {
		this(new RationalFraction(), new RationalFraction());
	}

	public RationalVector2D add(RationalVector2D rv) {
		return new RationalVector2D(x.add(rv.getX()), y.add(rv.getY()));
	}

	public RationalVector2D sub(RationalVector2D rv) {
		return new RationalVector2D(x.sub(rv.getX()), y.sub(rv.getY()));
	}

	public double length() {
		return Math.sqrt((x.mult(x)).value() + (y.mult(y)).value());
	}

	public double scalarProduct(RationalVector2D rv) {
		return (x.mult(rv.getX())).add(y.mult(rv.getY())).value();
	}

	public String toString() {
		return "{"+ x + "; " + y + "}";
	}

	public RationalFraction getX() {
		return x;
	}

	public RationalFraction getY() {
		return y;
	}

	public static void main(String[] args) {

		RationalFraction rf1 = new RationalFraction(1, 2);
		RationalFraction rf2 = new RationalFraction(-1, 4);
		RationalFraction rf3 = new RationalFraction(3, 4);

		RationalVector2D vec1 = new RationalVector2D();
		RationalVector2D vec2 = new RationalVector2D(rf1, rf2);
		RationalVector2D vec3 = new RationalVector2D(rf2, rf3);
 
		System.out.println(vec1);
		System.out.println(vec2.add(vec3));
		System.out.println(vec2.sub(vec3));
		System.out.println(vec2.scalarProduct(vec3));
		System.out.println(vec3.length());

	}

}