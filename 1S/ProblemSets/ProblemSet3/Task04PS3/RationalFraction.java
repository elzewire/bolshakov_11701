/**
* @author Edward Bolshakov
* 11-701
* Problem Set 3 Task 04
**/

package ProblemSet3.Task04PS3;

public class RationalFraction {

	public static int nod(int a, int b) {
		if (a == 0 || b == 0) {
			return 1;
		}
		while (a != b) {
			if (a > b) {
				a = a - b;
			} else {
				b = b - a;
			}
		}
		return a;
	}

	private int num;
	private int denom;

	public RationalFraction(int num, int denom) {
		this.num = num;
		if (denom == 0) {
			throw new java.lang.ArithmeticException();
		} else {
			this.denom = denom;
		}
	}

	public RationalFraction() {
		this(0, 1);
	}

	public void reduce() {
		int nod = nod(Math.abs(num), Math.abs(denom));

		if (nod > 1) {
			this.num /= nod;
			this.denom /= nod;
		}
	}

	public RationalFraction add(RationalFraction rf) {
		int n = num * rf.getDenom() + rf.getNum() * denom;
		int d = denom * rf.getDenom();
		RationalFraction frac = new RationalFraction(n, d);
		frac.reduce();
		return frac;
	}

	public RationalFraction sub(RationalFraction rf) {
		int n = num * rf.getDenom() - rf.getNum() * denom;
		int d = denom * rf.getDenom();
		RationalFraction frac = new RationalFraction(n, d);
		frac.reduce();
		return frac;
	}

	public RationalFraction mult(RationalFraction rf) {
		int n = num * rf.getNum();
		int d = denom * rf.getDenom();
		RationalFraction frac = new RationalFraction(n, d);
		frac.reduce();
		return frac;
	}

	public RationalFraction div(RationalFraction rf) {
		int n = num * rf.getDenom();
		int d = denom * rf.getNum();
		RationalFraction frac = new RationalFraction(n, d);
		frac.reduce();
		return frac;
	}

	public String toString() {
		return num + "/" + denom;
	}

	public double value() {
		return (double)num / denom;
	}

	public int getNum() {
		return num;
	}

	public int getDenom() {
		return denom;
	}

	public static void main(String[] args) {
		
		RationalFraction rf1 = new RationalFraction();
		RationalFraction rf2 = new RationalFraction(2, 4);
		RationalFraction rf3 = new RationalFraction(-1, 4);

		System.out.println(rf1);
		System.out.println(rf2);
		rf2.reduce();
		System.out.println(rf2);
		System.out.println(rf2.add(rf3));
		System.out.println(rf2.sub(rf3));
		System.out.println(rf2.mult(rf3));
		System.out.println(rf2.div(rf3));
		System.out.println(rf3.value());
		

	}

}