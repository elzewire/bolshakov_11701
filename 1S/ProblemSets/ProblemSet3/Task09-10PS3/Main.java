import java.util.Arrays;
import java.io.FileNotFoundException;
import java.text.ParseException;	

public class Main {

	public static void main(String[] args) 
			throws FileNotFoundException, ParseException {
		
		Database.loadUsers();
		Database.loadMessages();
		Database.loadSubs();
		
		System.out.println(Arrays.toString(Database.users));

		showAllFriends();
		showUsersChat("Daler", "Edward");
		mostSociableGender();
		readMsgStats();

	}

	public static void showAllFriends() {

		for (Sub sub1 : Database.subs) {
			for (Sub sub2 : Database.subs) {
				if (sub1.getTarget() == sub2.getFollower() 
					&& sub1.getFollower() == sub2.getTarget()) {

					System.out.println(sub1.getFollower().getUsername() + 
						" and " + sub1.getTarget().getUsername() + " are friends");
				}
			}
		}

	}

	public static void showUsersChat(String u1, String u2) {

		System.out.println("\nChat between " + u1 + " and " + u2 + ":");

		for (Message message : Database.messages) {
			if (message.getReceiver().getUsername().equals(u2) && message.getSender().getUsername().equals(u1)) {
				System.out.println(u2 + ": " + message.getText());
			} else if (message.getReceiver().getUsername().equals(u1) && message.getSender().getUsername().equals(u2)) {
				System.out.println(u1 + ": " + message.getText());
			}
		}

		System.out.println("End of chat\n");
		
	}

	public static void mostSociableGender() {

		int cMale = 0;
		int cFemale = 0;
		for (Message msg : Database.messages) {
			if (msg.getSender().getGender().equals("Female")) {
				cFemale++;
			} else {
				cMale++;
			}
		}

		if (cMale > cFemale) {
			System.out.println("Men sent more messages");
		} else if (cMale < cFemale) {
			System.out.println("Women sent more messages");
		} else {
			System.out.println("Amount of sent messages from men and women is equal");
		}

	}

	public static void readMsgStats() {
		int readF = 0;
		int readM = 0;
		int countF = 0;
		int countM = 0;
		String sGender;
		String rGender;

		for (Message msg : Database.messages) {
			sGender = msg.getSender().getGender();
			rGender = msg.getReceiver().getGender();
			if (sGender.equals("Male") && rGender.equals("Female")) {
				if (msg.getReadStatus()) {
					readM++;
					countM++;
				} else {
					countM++;
				}
			} else if (sGender.equals("Female") && rGender.equals("Male")) {
				if (msg.getReadStatus()) {
					readF++;
					countF++;
				} else {
					countF++;
				}
			}
		}

		if (countM != 0) {
			System.out.println("Men to women read messages stat: " + ((double)readM/countM)*100 + "%");
		} else {
			System.out.println("No men to women messages");
		}

		if (countF != 0) {
			System.out.println("Women to men read messages stat: " + ((double)readF/countF)*100 + "%");
		} else {
			System.out.println("No women to men messages");
		}
	}

}