public class User {

	private long id;
	private String username;
	private String password;
	private String gender;
	private String email;

	public User(long id, String username, String password, String gender, String email) {
		this.id = id;
		this.username = username;
		this.gender = gender;
		this.password = password;
		this.email = email;
	}

	//Getters
	public long getId() {
		return id;
	}

	public String getUsername() {
		return username;
	}

	public String getEmail() {
		return email;
	}

	public String getGender() {
		return gender;
	}

	//To String
	public String toString() {
		return username;
	}

}