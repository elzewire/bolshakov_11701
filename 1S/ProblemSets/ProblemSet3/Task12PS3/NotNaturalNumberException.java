/**
* @author Edward Bolshakov
* 11-701
* Problem Set 3 Task 12
**/

package ProblemSet3.Task12PS3;

public class NotNaturalNumberException extends Exception {

	public NotNaturalNumberException(String message) {
		super(message);
	}

}