/**
* @author Edward Bolshakov
* 11-701
* Problem Set 3 Task 12
**/

package ProblemSet3.Task12PS3;

import ProblemSet3.Task12PS3.NotNaturalNumberException;

public class VeryLongNumber implements Number {

	private int[] value;

	public VeryLongNumber(int[] value) {
		this.value = value;
	}
	
	public Number add(Number number) {

		int min = Math.min(value.length, number.getValue().length);
		int max = Math.max(value.length, number.getValue().length);
		int[] newVal = new int[max];
		int rem = 0;
		int val = 0;

		for (int i = max - 1; i >= 0; i--) {
			if (min - 1 < i) {
				newVal[i] = number.getValue()[i]; 
			} else {
				val = (value[i + (max - min)] + number.getValue()[i] + rem);
				newVal[i] = val % 10; 
				rem = val / 10;
			}
		}

		return new VeryLongNumber(newVal);

	}

	public Number sub(Number number) {
		if (number.getValue().length > value.length) {
			//throw new NotNaturalNumberException();
		}

		int[] newVal = new int[value.length];
		int rem = 0;
		int val = 0;

		for (int i = 0; i < newVal.length; i++) {
			if (i > number.getValue().length - 1) {
				newVal[i] -= rem;
				break;
			}
			val = value[i] - number.getValue()[i] - rem;
			if (val < 0) {
				newVal[i] = 0;
				rem = 1;
			} else {
				rem = 0;
			}
		}

		return new VeryLongNumber(newVal);

	}

	public String toString() {
		String str = "";
		for (int i = 0; i < value.length; i++) {
			str += value[i];
		}

		return str;
	}

	public int compareTo(Number number) {
		System.out.println();
		return 0;	
	}		

	public int[] getValue() {
		return value;
	}

	public static void main(String[] args) {

		int[] v1 = {1, 2, 3, 4, 5, 6, 7, 8, 9};
		int[] v2 =          {6, 5, 4, 3, 2, 1};

		VeryLongNumber n1 = new VeryLongNumber(v1);
		VeryLongNumber n2 = new VeryLongNumber(v2);

		System.out.println(n1.add(n2));
		//System.out.println(n1.sub(n2));
	}

}