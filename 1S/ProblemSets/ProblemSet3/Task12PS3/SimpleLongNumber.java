/**
* @author Edward Bolshakov
* 11-701
* Problem Set 3 Task 12
**/

public class SimpleLongNumber implements Number {

	private long value;

	public SimpleLongNumber(long value) {
		this.value = value;
	}

	public Number add(SimpleLongNumber number) {
		return new SimpleLongNumber(this.value + number.getValue());
	}

}