/**
* @author Edward Bolshakov
* 11-701
* Problem Set 3 Task 12
**/

package ProblemSet3.Task12PS3;

public interface Number {
	Number add(Number n);
	Number sub(Number n);
	int compareTo(Number n);
	int[] getValue();
}