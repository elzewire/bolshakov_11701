/**
* @author Edward Bolshakov
* 11-701
* Problem Set 3 Task 08
**/

package ProblemSet3.Task08PS3;

import ProblemSet3.Task08PS3.CSVReader;
import java.io.IOException;
import java.util.Arrays;

public class Main {

    public static void main(String[] args) throws IOException {
        
        CSVReader reader = new CSVReader("ProblemSets/ProblemSet3/Task08PS3/Browser_Speed.csv");
        String[][] data = reader.readFile();

        for(int i = 0; i < data.length; i++) {
            System.out.println(Arrays.toString(data[i]));
        }

        System.out.println(fastestSite(data));
        System.out.println(fastestBrowser(data));
        System.out.println(avgTimeOfOpening(data));
    
    }

    public static String fastestSite(String[][] data) {
        double max = 0;
        double s = 0;
        int k = 0;
        String site = "";        

        for (int i = 3; i < data[0].length; i++) {
            for (int j = 2; j < data.length; j++) {
                s += Double.parseDouble(data[j][i]);
                k++;
            }

            if (s/k >= max) {
                max = s;
                site = data[0][i];
            }

            s = 0;
            k = 0;
        }

        return site;
    }

    public static String fastestBrowser(String[][] data) {
        double max = 0;
        double s = 0;
        int k = 0;
        String browser = "";        

        for (int i = 2; i < data.length; i++) {
            for (int j = 3; j < data[0].length; j++) {
                s += Double.parseDouble(data[i][j]);
                k++;
            }

            if (s/k >= max) {
                max = s;
                browser = data[i][0];
            }

            s = 0;
            k = 0;
        }

        return browser;
    }

    public static double avgTimeOfOpening(String[][] data) {
        double avg = 0;
        int k = 0;

        for (int i = 2; i < data.length; i++) {
            for (int j = 3; j < data[0].length; j++) {
                avg += Double.parseDouble(data[i][j]);
                k++;
            }
        }

        return avg/k;

    }

}