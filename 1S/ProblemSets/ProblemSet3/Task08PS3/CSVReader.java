/**
* @author Edward Bolshakov
* 11-701
* Problem Set 3 Task 08
**/

package ProblemSet3.Task08PS3;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class CSVReader {

	private String csvFile;
	private String splitter;

	public CSVReader(String csvFile) {
		this.csvFile = csvFile;
		splitter = ",";
	}

    public String[][] readFile() throws IOException{
    	int k = 0;

    	String[][] ret = new String[6][12];

    	Scanner sc = new Scanner(new FileReader(csvFile));

    	while (sc.hasNextLine()) {
    		String line = sc.nextLine();
    		ret[k] = line.split(splitter);
    		k++;
    	}

    	return ret;
    }

}

