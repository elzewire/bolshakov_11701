/**
* @author Edward Bolshakov
* 11-701
* Problem Set 3 Task 03
**/

package ProblemSet3.Task03PS3;

public class Vector2D {

	private double x;
	private double y;

	public Vector2D(double x, double y) {
		this.x = x;
		this.y = y;
	}

	public Vector2D() {
		this(0, 0);
	}

	public Vector2D add(Vector2D vector) {
		return new Vector2D(x + vector.getX(), y + vector.getY());
	}

	public Vector2D sub(Vector2D vector) {
		return new Vector2D(x - vector.getX(), y - vector.getY());
	}

	public Vector2D mult(double mult) {
		return new Vector2D(x * mult, y * mult);
	}

	public String toString() {
		return "{" + x + "; " + y + "}";
	}

	public double length() {
		return Math.sqrt(x*x + y*y);
	}

	public double scalarProduct(Vector2D vector) {
		return x * vector.getX() + y * vector.getY();
	}

	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}

	public static void main(String[] args) {
		
		Vector2D vec1 = new Vector2D();
		Vector2D vec2 = new Vector2D(1, 1);
		Vector2D vec3 = new Vector2D(-4, 3);
 
		System.out.println(vec1);
		System.out.println(vec2.add(vec3));
		System.out.println(vec2.sub(vec3));
		System.out.println(vec2.scalarProduct(vec3));
		System.out.println(vec3.length());
		System.out.println(vec3.mult(1.5));
		
	}

}