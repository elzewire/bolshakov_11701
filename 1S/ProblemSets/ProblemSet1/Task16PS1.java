/**
* @author Edward Bolshakov
* 11-701
* Problem Set 1 Task 16
**/

import java.util.Scanner;

public class Task16PS1 {

	public static int abs(int x) {
		return x > 0 ? x : -1;
	}

	public static void main(String[] args) {
			
		Scanner sc = new Scanner(System.in);

		int n = sc.nextInt();
		int[] arr = new int[n];

		for (int i = 0; i < n; i++) {
			arr[i] = sc.nextInt();
		}

		System.out.println(hasExactlyThree(arr));

	}

	public static boolean hasExactlyThree(int[] arr) {

		int k = 0;

		for (int x : arr) {
			if (hasDecreasingDigits(x)) {
				if (k > 3) {
					return false;
				} else {
					k++;
				}
			}
		}

		return k == 3;

	}

	public static boolean hasDecreasingDigits(int x) {

		int d1 = x % 10;
		x /= 10; 
		int d2;

		while (abs(x) > 0) {
			d2 = x % 10;
			x /= 10;
			if (d1 >= d2) {
				return false;
			}
			d1 = d2;
		}

		return true;

	}

}