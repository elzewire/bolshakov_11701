/**
* @author Edward Bolshakov
* 11-701
* Problem Set 1 Task 03
**/

import java.util.Scanner;

public class Task03PS1 {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);

		final double EPS = 1e-6;

		double x = sc.nextDouble();
		double n = x - EPS;

		while (n * n - x > EPS) {
			n -= EPS;
		}

		System.out.println(n);
	}

}