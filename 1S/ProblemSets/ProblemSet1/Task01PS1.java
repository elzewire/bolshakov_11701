/**
* @author Edward Bolshakov
* 11-701
* Problem Set 1 Task 01
**/

public class Task01PS1 {

	public static void main(String[] args) {
		
		int n = Integer.parseInt(args[0]);
		String str = "";
		for (int i = 0; i < n; i++) {
			for (int j = i; j < 2 * (n - 1); j++) {
				str += " ";
			}
			for (int j = 0; j < 2 * i - 1; j++) {
				str += "*";
			}
			System.out.println(str);
			str = "";
		}

		for (int i = 0; i < n; i++) {
			for (int j = i; j < n - 1; j++) {
				str += " ";
			}
			for (int j = 0; j < 2 * i - 1; j++) {
				str += "*";
			}
			for (int j = 0; j < 2 * (n - i) - 1; j++) {
				str += " ";
			}
			for (int j = 0; j < 2 * i - 1; j++) {
				str += "*";
			}
			System.out.println(str);
			str = "";
		}

	}

}