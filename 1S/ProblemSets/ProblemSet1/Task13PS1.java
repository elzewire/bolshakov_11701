/**
* @author Edward Bolshakov
* 11-701
* Problem Set 1 Task 13
**/

import java.util.Scanner;

public class Task13PS1 {

	public static void main(String[] args) {
			
		Scanner sc = new Scanner(System.in);

		int n = sc.nextInt();
		int x = 1;
		int s = 0;
		for (int i = 0; i < n; i++) {
			s += x;
			x = x * 10 + 1;
		} 

		System.out.println(s);
	}

}