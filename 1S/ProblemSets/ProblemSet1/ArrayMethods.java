/**
* @author Edward Bolshakov
* 11-701
* for Problem Set 1 Tasks 10,11,17,19,20,23,24
**/

import java.util.Random;

public class ArrayMethods {

	public static int[] createRandom1DArray(int n) {
		
		Random rnd = new Random();

		int[] arr = new int[n];

		for (int i = 0; i < n; i++) {
			arr[i] = rnd.nextInt(10);
		}

		return arr;

	}

	public static int[][] createRandom2DArray(int n, int m) {
		
		Random rnd = new Random();	

		int[][] arr = new int[n][m];

		for (int i = 0; i < n; i++) {
			for (int j = 0; j < m; j++) {
				arr[i][j] = rnd.nextInt(10);
			}
		}

		return arr;

	}

	public static int[][][] createRandom3DArray(int n, int m, int k) {
		
		Random rnd = new Random();

		int[][][] arr = new int[n][m][k];

		for (int i = 0; i < n; i++) {
			for (int j = 0; j < m; j++) {
				for (int l = 0; l < k; l++) {
					arr[i][j][l] = rnd.nextInt(10);
				}
			}
		}

		return arr;

	}

	public static void printArray(int[] arr) {
		
		for (int i = 0; i < arr.length; i++) {
			System.out.print(arr[i] + " ");
		}

		System.out.println();

	}
	
	public static void printArray(int[][] arr) {
		
		for (int i = 0; i < arr.length; i++) {
			for (int j = 0; j < arr[0].length; j++) {
				System.out.print(arr[i][j] + " ");
			}
			System.out.println();
		}

		System.out.println();

	}

	public static void printArray(int[][][] arr) {
		
		for (int i = 0; i < arr.length; i++) {
			for (int j = 0; j < arr[0].length; j++) {
				for(int l = 0; l < arr[0][0].length; l++) {
					System.out.print(arr[i][j][l] + " ");
				}
				System.out.println();
			}
			System.out.println();
		}

		System.out.println();

	}

}