/**
* @author Edward Bolshakov
* 11-701
* Problem Set 1 Task 05
**/

import java.util.Scanner;

public class Task05PS1 {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);

		int n = sc.nextInt();
		double fact1 = 1;
		double fact2 = 2;
		double s = (fact1 * fact1) / (fact2);

		for (int i = 2; i <= n; i++) {
			fact1 *= i - 1;
			fact2 *= 2 * i * (2 * i - 1);
			s += (fact1 * fact1) / (fact2);
		}

		System.out.println(s);

	}

}