/**
* @author Edward Bolshakov
* 11-701
* Problem Set 1 Task 10
**/

import java.util.Scanner;

public class Task10PS1 {

	public static boolean hasEvenLocalMax(int[] arr) {

		for (int i = 1; i < arr.length - 1; i++) {
			if (arr[i - 1] < arr[i] && arr[i] > arr[i + 1]) {
				return true;
			}
		}

		return false;

	}


	public static void main(String[] args) {
			
		Scanner sc = new Scanner(System.in);

		int n = sc.nextInt();

		int[] arr = ArrayMethods.createRandom1DArray(n);

		ArrayMethods.printArray(arr);

		System.out.println();
		System.out.println(hasEvenLocalMax(arr));

	}

}