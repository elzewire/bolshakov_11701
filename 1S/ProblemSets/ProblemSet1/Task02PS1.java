/**
* @author Edward Bolshakov
* 11-701
* Problem Set 1 Task 02
**/

public class Task02PS1 {

	public static void main(String[] args) {
		
		int k = Integer.parseInt(args[0]);
		int m = Integer.parseInt(args[1]);

		k = k % 3 != 0 ? (k / 3 + 1) * 3 : k;
		m = (m / 3) * 3;

		if (k > m) {
			int temp = k;
			k = m;
			m = temp;
		}

		for (int i = k; i <= m; i += 3) {
			System.out.println(i);
		}
	}

}