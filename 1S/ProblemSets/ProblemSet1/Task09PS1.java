/**
* @author Edward Bolshakov
* 11-701
* Problem Set 1 Task 09
**/

import java.util.Scanner;

public class Task09PS1 {

	public static double abs(double x) {
		return x > 0 ? x : -x;
	}

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		final double EPS = 1e-9;

		double x = sc.nextDouble();

		int k = 1;
		double fact = 1;
		double deg = x;
		double coef1 = 1;
		double coef2 = 2 * k + 1;
		double a = (coef1 * deg) / (fact * coef2);
		double s = a;

		while (abs(a) > EPS) {

			fact *= k;
			deg *= x * x;
			coef1 *= -1;
			coef2 = 2 * k + 1;
			a = (coef1 * deg) / (fact * coef2);
			s += a;
			k++;

		}

		System.out.println(s);

	}

}