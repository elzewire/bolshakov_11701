/**
* @author Edward Bolshakov
* 11-701
* Problem Set 1 Task 19
**/

import java .util.Scanner;

public class Task19PS1 {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);

		int n = sc.nextInt();

		int [][] array = ArrayMethods.createRandom2DArray(n, n);
		
		ArrayMethods.printArray(array);
		cutTriangles(array);
		ArrayMethods.printArray(array);

	}

	public static void cutTriangles(int[][] arr) {
		
		for (int i = 0; i < arr.length; i++) {
			for (int j = i + 1; j < arr.length - i - 1; j++) {
				arr[i][j] = 0;
				arr[arr.length - i - 1][j] = 0;
			}
		}

	}

}