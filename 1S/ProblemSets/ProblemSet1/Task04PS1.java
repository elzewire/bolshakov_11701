/**
* @author Edward Bolshakov
* 11-701
* Problem Set 1 Task 04
**/

import java.util.Scanner;

public class Task04PS1 {
	
	public static int abs(int x) {
		return x > 0 ? x : -x;
	}

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);

		int n = sc.nextInt();
		separateByMultipliers(n);

	}

	public static void separateByMultipliers(int n) {

		int mp = 2;

		while (abs(n) > 1) {
			if (n % mp == 0) {
				System.out.print(mp + " ");
				n = n / mp;
			} else {
				mp++;
			}
		}

	}

}