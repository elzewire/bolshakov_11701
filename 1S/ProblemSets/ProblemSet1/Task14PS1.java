/**
* @author Edward Bolshakov
* 11-701
* Problem Set 1 Task 14
**/

import java.util.Scanner;

public class Task14PS1 {

	public static int abs(int x) {
		return x > 0 ? x : -x;
	}

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);

		int k = sc.nextInt();
		int n = sc.nextInt();

		System.out.println(toDecimal(n, k));

	}

	public static int toDecimal(int n, int scale) {

		int x = 0;
		int pow = 1;

		do {
			
			x += n % 10 * pow;
			pow *= scale;
			n /= 10;

		} while (abs(n) > 0);

		return x;

	}

}