/**
* @author Edward Bolshakov
* 11-701
* Problem Set 1 Task 17
**/

import java.util.Scanner;

public class Task17PS1 {
	
	public static void main(String[] args) {
			
		Scanner sc = new Scanner(System.in);

		int n = sc.nextInt();
		int[] arr = ArrayMethods.createRandom1DArray(n);

		ArrayMethods.printArray(arr);	
		reverseArray(arr);
		ArrayMethods.printArray(arr);
	}

	public static void reverseArray(int[] arr) {

		int middle = arr.length / 2;
		int temp;
		int j = arr.length - 1;
		for (int i = 0; i < middle; i++) {
			temp = arr[i];
			arr[i] = arr[j - i];
			arr[j - i] = temp;
		}

	}

}
