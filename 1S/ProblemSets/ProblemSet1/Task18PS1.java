/**
* @author Edward Bolshakov
* 11-701
* Problem Set 1 Task 18
**/

import java.util.Scanner;

public class Task18PS1 {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);

		int n = sc.nextInt();
		int k = sc.nextInt();
		int[] arr = ArrayMethods.createRandom1DArray(n);

		ArrayMethods.printArray(arr);
		reverseArrayBlocks(arr, k);
		ArrayMethods.printArray(arr);

	}

	public static void reverseArrayBlocks(int[] arr, int k) {
		reverseArrayPart(arr, 0, k - 1);
		reverseArrayPart(arr, k, arr.length - 1);
		reverseArrayPart(arr, 0, arr.length - 1);
	}

	public static void reverseArrayPart(int[] arr, int start, int last) {
		int temp;
		int m = (last + start) / 2;
		for (int i = start; i <= m; i++) {
			temp = arr[i];
			arr[i] = arr[last + start - i];
			arr[last + start - i] = temp;
		}

	}

}