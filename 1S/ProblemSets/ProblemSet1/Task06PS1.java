/**
* @author Edward Bolshakov
* 11-701
* Problem Set 1 Task 06
**/

import java.util.Scanner;

public class Task06PS1 {
	
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		int r = sc.nextInt();

		printCircle(r);

	}

	public static void printCircle(int r) {

		char[][] circle = createCircle(r);

		for (int i = 0; i < circle.length; i++) {
			for (int j = 0; j < circle.length; j++) {
				System.out.print(circle[i][j] + " ");
			}
			System.out.println();
		}

	} 

	public static char[][] createCircle(int r) {

		int size = r * 2 + 1;

		char[][] circle = new char[size][size];

		//Get circle center
		int cx = r;
		int cy = r;
	
		//Fill the circle
		for (int i = 0; i < size; i++) {
			for (int j = 0; j < size; j++) {
				if ((i - cy)*(i - cy) + (j - cx)*(j - cx) <= r * r) {
					circle[i][j] = '0';
				} else {
					circle[i][j] = '*';
				}
			}
		}

		return circle;

	}

}