/**
* @author Edward Bolshakov
* 11-701
* Problem Set 1 Task 07
**/

import java.util.Scanner;

public class Task07PS1 {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		int r = sc.nextInt();

		printYinYang(r);

	}

	public static void printYinYang(int r) {

		char[][] yinYang = createYinYang(r);

		for (int i = 0; i < yinYang.length; i++) {
			for (int j = 0; j < yinYang.length; j++) {
				System.out.print(yinYang[i][j] + " ");
			}
			System.out.println();
		} 

	}

	public static char[][] createYinYang(int r) {

		int size = 2 * r + 1;

		char[][] yinYang = new char[size][size];

		//Get circle center
		int cx = r;
		int cy = r;
	
		//Fill the circle
		for (int i = 0; i < size; i++) {
			for (int j = 0; j < size; j++) {
				if ((i - cy)*(i - cy) + (j - cx)*(j - cx) <= r * r) {
					if (i <= r) {
						yinYang[i][j] = '0';
					} else {
						yinYang[i][j] = '*';
					}
				} else {
					yinYang[i][j] = ' ';
				}
			}
		}

		//Small white circle and black dot
		createCircleColour(yinYang, r / 2, r / 2, r, '0');
		createCircleColour(yinYang, r / 6, r / 2, r, '*');
		
		//Small black circle and white dot
		createCircleColour(yinYang, r / 2, r + r / 2, r, '*');
		createCircleColour(yinYang, r / 6, r + r / 2, r, '0');

		return yinYang;
		
	}

	public static void createCircleColour(char[][] yinYang, int r, int cx, int cy, char col) {
	
		int size = r * 2 + 1;

		int xOffset = cx - r;
		int yOffset = cy - r;

		//Fill the circle
		for (int i = yOffset; i < size + yOffset; i++) {
			for (int j = xOffset; j < size + xOffset; j++) {
				if ((i - cy)*(i - cy) + (j - cx)*(j - cx) <= r * r) {
					yinYang[i][j] = col;
				}
			}
		}

	}



}