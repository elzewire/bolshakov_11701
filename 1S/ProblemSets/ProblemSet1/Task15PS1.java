/**
* @author Edward Bolshakov
* 11-701
* Problem Set 1 Task 15
**/

import java.util.Scanner;

public class Task15PS1 {

	public static int abs(int x) {
		return x > 0 ? x : -x;
	}
	
	public static void main(String[] args) {
			
		Scanner sc = new Scanner(System.in);

		int n = sc.nextInt();
		int[] arr = new int[n];

		for (int i = 0; i < n; i++) {
			arr[i] = sc.nextInt();
		}

		System.out.println(hasExactlyTwo(arr));

	}

	public static boolean hasExactlyTwo(int[] arr) {

		int k = 0;

		for (int x : arr) {
			if (is3Or5Long(x) && isOnlyEvenOrOdd(x)) {
				if (k > 2) {
					return false;
				} else {
					k++;
				}
			}
		}

		if (k == 2) {
			return true;
		} else {
			return false;
		}

	}

	public static boolean is3Or5Long(int x) {

		int k = 0;

		while (abs(x) > 0) {
			if (k > 5) {
				return false;
			} else {
				k++;
			}

			x /= 10;
		}

		if (k == 3 || k == 5) {
			return true;
		} else {
			return false;
		}

	}

	public static boolean isOnlyEvenOrOdd(int x) {

		int flag = 0;

		while (x > 0) {
			switch (flag) {
				case 0:
					if (x % 2 == 0) {
						flag = 1;
					} else {
						flag = -1;
					}
					break;
				case 1:
					if (x % 2 == 1) {
						return false;
					}
					break;
				case -1:
					if (x % 2 == 0) {
						return false;
					}
					break;
			}

			x /= 10;
		}

		return true;

	}

}