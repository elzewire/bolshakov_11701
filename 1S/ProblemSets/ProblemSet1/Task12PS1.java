/**
* @author Edward Bolshakov
* 11-701
* Problem Set 1 Task 12
**/

import java.util.Scanner;

public class Task12PS1 {

	public static void main(String[] args) {
			
		Scanner sc = new Scanner(System.in);

		int n = sc.nextInt();
		int k = 1;
		int x = 0;

		boolean flag = false;

		while (n > 0) {
			if (n % 2 == 1) {
				x = x + k * (n % 10);
				k *= 10;
			}
			n = n / 10;
		} 

		System.out.println(x);
	}

}