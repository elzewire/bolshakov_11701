/**
* @author Edward Bolshakov
* 11-701
* Problem Set 1 Task 20
**/

import java.util.Scanner;

public class Task20PS1 {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		int n = sc.nextInt();
		int arr[][] = ArrayMethods.createRandom2DArray(n, n);

		ArrayMethods.printArray(arr);

		int coef1;
		int coef2;
		for (int i = 0; i < n; i++) {
			if (arr[i][i] == 0) {
				int col = getNonZeroElementLine(arr, i);
				if (col != 0) {
					arraySwapLines(arr, i, col);
					coef1 = arr[i][i];
					for (int j = i + 1; j < n; j++) {
						if (arr[j][i] != 0) {
							coef2 = arr[j][i];
							for (int l = 0; l < n; l++) {
								arr[j][l] *= coef1;
								arr[j][l] -= arr[i][l] * coef2;  
							}
						}
					}
				}
			} else {
				coef1 = arr[i][i];
				for (int j = i + 1; j < n; j++) {
					if (arr[j][i] != 0) {
						coef2 = arr[j][i];
						for (int l = 0; l < n; l++) {
							arr[j][l] *= coef1;
							arr[j][l] -= arr[i][l] * coef2;  
						}
					}
				}
			}

		}	

		ArrayMethods.printArray(arr);	

	}

	public static int getNonZeroElementLine(int[][] arr, int col) {

		for (int i = 0; i < arr.length; i++) {
			if (arr[i][col] != 0) {
				return i;
			} 
		}

		return 0;

	}

	public static void arraySwapLines(int[][] arr, int l1, int l2) {

		int[] temp = new int[arr[0].length];

		for (int i = 0; i < arr[0].length; i++) {
			temp[i] = arr[l1][i];
			arr[l1][i] = arr[l2][i];
			arr[l2][i] = temp[i];
		}

	}


}