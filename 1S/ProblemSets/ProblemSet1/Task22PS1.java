/**
* @author Edward Bolshakov
* 11-701
* Problem Set 1 Task 22
**/

import java.util.Scanner;

public class Task22PS1 {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);

		int n = sc.nextInt();
		int[][] arr = ArrayMethods.createRandom2DArray(n, n);

		ArrayMethods.printArray(arr);

		System.out.println(arrayDiagMaxSum(arr));

	}

	public static int arrayDiagMaxSum(int[][] arr) {

		int max = 0;
		int sum;

		for (int n = 0; n < arr.length; n++) {
			sum = diagSum(arr);
			if (sum > max) {
				max = sum;
			}

			arrayShiftColumns(arr);
		}

		return max;

	}

	public static void arrayShiftColumns(int[][] arr) {

		int[] temp = new int[arr.length];

		for (int i = 0; i < arr.length; i++) {
			temp[i] = arr[i][0];
		}

		for (int i = 0; i < arr.length; i++) {
			for (int j = 0; j < arr[0].length - 1; j++) {
				arr[i][j] = arr[i][j+1];
			}
		}

		for (int i = 0; i < arr.length; i++) {
			arr[i][arr[0].length - 1] = temp[i];
		}

	}

	public static int diagSum(int[][] arr) {

		int s = 0;

		for (int i = 0; i < arr.length; i++) {
			s += arr[i][i];
		}

		return s;

	}
	
}