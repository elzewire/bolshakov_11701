/**
* @author Edward Bolshakov
* 11-701
* Problem Set 1 Task 11
**/

import java.util.Scanner;

public class Task11PS1 {

	public static boolean hasTwoEvenLocalMax(int[] arr) {

		int k = 0;

		for (int i = 1; i < arr.length - 1; i++) {
			if (arr[i - 1] < arr[i] && arr[i] > arr[i + 1]) {
				if (k > 2) {
					return false;
				} else {
					k++;
				}
			}
		}

		return k == 2;
		
	}

	public static void main(String[] args) {
			
		Scanner sc = new Scanner(System.in);

		int n = sc.nextInt();

		int[] arr = ArrayMethods.createRandom1DArray(n);

		ArrayMethods.printArray(arr);

		System.out.println();
		System.out.println(hasTwoEvenLocalMax(arr));

	}

}