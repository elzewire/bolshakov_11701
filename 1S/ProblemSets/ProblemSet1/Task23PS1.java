/**
* @author Edward Bolshakov
* 11-701
* Problem Set 1 Task 23
**/

import java.util.Scanner;

public class Task23PS1 {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);

		int n = sc.nextInt();
		int m = sc.nextInt();
		int k = sc.nextInt();
		int[][] arr1 = ArrayMethods.createRandom2DArray(n, m);
		int[][] arr2 = ArrayMethods.createRandom2DArray(m, k);

		ArrayMethods.printArray(arr1);
		ArrayMethods.printArray(arr2);

		ArrayMethods.printArray(multiplyMatrices(arr1, arr2));

	}

	public static int[][] multiplyMatrices(int[][] arr1, int[][] arr2) {

		int[][] arr = new int[arr1.length][arr2[0].length];
		int s = 0;

		for (int i = 0; i < arr1.length; i++) {
			for (int j = 0; j < arr2[0].length; j++) {
				for (int k = 0; k < arr1[0].length; k++) {
					s += arr1[i][k] * arr2[k][j];
				}
				arr[i][j] = s;
				s = 0;
			}
		}

		return arr;

	}

}