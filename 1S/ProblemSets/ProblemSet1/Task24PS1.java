/**
* @author Edward Bolshakov
* 11-701
* Problem Set 1 Task 24
**/

import java.util.Scanner;

public class Task24PS1 {

	public static void main(String[] args) {
			
		Scanner sc = new Scanner(System.in);

		int n = sc.nextInt();
		int m = sc.nextInt();
		int k = sc.nextInt();

		int[][][] arr = ArrayMethods.createRandom3DArray(n, m, k);
		ArrayMethods.printArray(arr);
		System.out.println(hasLineInEach2DArr(arr));

	}

	public static boolean hasLineInEach2DArr(int[][][] arr) {

		for (int[][] x : arr) {
			if (!hasLine(x)) {
				return false;
			}
		}

		return true;
	
	}

	public static boolean hasLine(int[][] arr) {

		for (int[] x : arr) {
			if (hasOnlyDivisibleBy3El(x)) {
				return true;
			}
		}

		return false;
	
	}

	public static boolean hasOnlyDivisibleBy3El(int[] arr) {

		for (int x : arr) {
			if (x % 3 != 0) {
				return false;
			}
		}

		return true;

	}

}