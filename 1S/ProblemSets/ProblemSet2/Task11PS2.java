/**
* @author Edward Bolshakov
* 11-701
* Problem Set 2 Task 11
**/

import java.util.Arrays;

public class Task11PS2 {

	public static void sortStrings(String[] arr) {
		String tmp = "";

		for (int i = 0; i < arr.length; i++) {
			for (int j = 1; j < arr.length; j++) {
				if (Task10PS2.compareStrings(arr[j - 1], arr[j]) > 0) {
					tmp = arr[j - 1];
					arr[j - 1] = arr[j];
					arr[j] = tmp;
				}
			}
		}
	}

	public static void main(String[] args) {
		
		String[] arr = {"aba","abs","ace","act","add","ado","aft","age",
			"arm","art","ash","ask","asp","ass","ate","ave","awe","axe",
			"beg","bel","ben","bet","bid","big","bin","bio","bis","bit",
			"biz","bob","bog","boo","bow","box","boy","bra","bud","Bug",
			"ana","and","ant","any","ape","app","apt","arc","are","ark",
			"ago","aha","aid","aim","air","ala","ale","all","alt","amp",
			"bum","bun","bus","but","buy","bye","cab","cad","cam","can",
			"cap","car","cat","chi","cob","cod","col","con","coo","cop",
			"aye","BAA","bad","bag","ban","bar","bat","bay","bed","bee",
			"cor","cos","cot","cow","cox","coy","cry","cub","cue","cup",
			"cut","dab","dad","dal","dam","dan"};
		sortStrings(arr);
		System.out.println(Arrays.toString(arr));
	
	}

}
