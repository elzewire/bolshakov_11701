/**
* @author Edward Bolshakov
* 11-701
* Problem Set 2 Task 10
**/

public class Task10PS2 {

    public int compare(String s1, String s2) {
        int n1 = s1.length();
        int n2 = s2.length();
        int min = Math.min(n1, n2);
        for (int i = 0; i < min; i++) {
            char c1 = s1.charAt(i);
            char c2 = s2.charAt(i);
            if (c1 != c2) {
                c1 = Character.toUpperCase(c1);
                c2 = Character.toUpperCase(c2);
                if (c1 != c2) {
                    c1 = Character.toLowerCase(c1);
                    c2 = Character.toLowerCase(c2);
                    if (c1 != c2) {
                        return c1 - c2;
                    }
                }
            }
        }
        return n1 - n2;
    }

	public static int compareStrings(String str1, String str2) {
		int k = 0;

		int min = Math.min(str1.length(), str2.length());
		for (int i = 0; i < min; i++) {
			char c1 = str1.charAt(i);
			char c2 = str2.charAt(i);
			if (c1 != c2) {
				return c1 - c2;
			}
		}

		return str1.length() - str2.length();
	}

	public static void main(String[] args) {
		
		System.out.println(compareStrings("adsadasd", "adsad"));
		System.out.println(compareStrings("adsadasd", "dadsad"));

	}

}