/**
* @author Edward Bolshakov
* 11-701
* Problem Set 2 Task 03
**/

import java.util.regex.*;

public class Task03PS2 {

	public static void main(String[] args) {
		
		Pattern p = Pattern.compile(
			"(((0?(10)*|1?(01)*))|(0*|1*))"
		);

		for (int i = 0; i < args.length; i++) {
			Matcher m = p.matcher(args[i]);
			if (m.matches()) {
				System.out.print((i + 1) +"; ");
			}
		}

	}

}