/**
* @author Edward Bolshakov
* 11-701
* Problem Set 2 Task 09
**/

import java.util.Scanner;

public class Task09PS2 {

	public static void backTracking(String s, int n, int k) {
		if (s.length() == n) {
			System.out.println(s);
		} else if (s.length() == 0) {
			for (char c = 'a'; c <= 'z'; c += 1) {
				backTracking(s + c, n, k);
			}
		} else {
			char c2;

			for (char c = 'a'; c <= 'z'; c += 1) {
				c2 = s.charAt(s.length() - 1);
				if (Math.abs(c - c2) == k) {
					backTracking(s + c, n, k);
				}
			}
		}
	}

	public static void main(String[] args) {
		
		String s = "";
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		int k = sc.nextInt();
		backTracking(s, n, k);

	}

}