/**
* @author Edward Bolshakov
* 11-701
* Problem Set 2 Task 12-13
**/

import java.util.Random;
import java.util.Arrays;
import java.util.Scanner;

public class Task12PS2 {

	static Scanner sc = new Scanner(System.in);
	final static int N = sc.nextInt();
	final static int K = sc.nextInt();

	public static void main(String[] args) {
		
		int num = 100;
		String[] arr = createRandom1DArray(num);
		
		System.out.println(Arrays.toString(arr));

		geneticAlgorythm(arr, 0);

	}

	public static String[] createRandom1DArray(int num) {
		
		Random rnd = new Random();

		String[] arr = new String[num];

		for (int i = 0; i < num; i++) {
			arr[i] = "";
			for (int j = 0; j < N; j++) {
				arr[i] += (char)('a' + rnd.nextInt(26));
			}
		}

		return arr;

	}

	public static void geneticAlgorythm(String[] arr, int gen) {

		//Terminate if unsuccesful
		if (gen > 20) {
			return;
		}

		//Print info
		System.out.println("Generation: " + gen);
		System.out.println(arr[0]);
		System.out.println(Arrays.toString(arr));


		//Create new generation
		if (checkMistake(arr[0]) > 0) {
			geneticAlgorythm(createNewGeneration(arr), ++gen);
		} else {
			System.out.println(arr[0]);
		}

		//Run sorting program
		sortByMistakes(arr);

	}

	public static void sortByMistakes(String[] arr) {

		String temp;

		for (int i = 0; i < arr.length - 1; i++) {
			for (int j = 0; j < arr.length; j++) {
				if (checkMistake(arr[i]) > checkMistake(arr[i + 1])) {
					temp = arr[i];
					arr[i] = arr[i + 1];
					arr[i + 1] = temp;
				}
			}
		}

	}

	public static int checkMistake(String str) {
		
		int k = 0;
		char c;
		char c2;

		for (int i = 1; i < str.length(); i++) {
			c2 = str.charAt(i - 1);
			c = str.charAt(i);
			if (Math.abs(c - c2) != K) {
				k++;
			}
		}

		return k;

	}

	public static String[] createNewGeneration(String[] arr) {

		String p1 = arr[0];
		String p2 = arr[1];

		for (int i = 0; i < arr.length; i++) {
			arr[i] = crossover(p1, p2);
		}

		return arr;

	}

	public static String crossover(String p1, String p2) {

		Random rnd = new Random();

		String child = "";

		for (int i = 0; i < N; i++) {
			if (rnd.nextBoolean()) {
				//Mutation
				if (rnd.nextInt(100) + 1 <= 15) {
					child += (char)('a' + rnd.nextInt(26));
				} else {
					child += p1.charAt(rnd.nextInt(N));
				}
			} else {
				//Mutation
				if (rnd.nextInt(100) + 1 <= 15) {
					child += (char)('a' + rnd.nextInt(26));
				} else {
					child += p2.charAt(rnd.nextInt(N));
				}
			}
		}

		return child;

	}

}