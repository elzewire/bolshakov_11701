/**
* @author Edward Bolshakov
* 11-701
* Problem Set 2 Task 02
**/

import java.util.regex.*;

public class Task02PS2 {

	public static void main(String[] args) {
		
		Pattern p = Pattern.compile(
			"0|-?[1-9][0-9]*|-?(0|[1-9][0-9]*)\\.([0-9]*[1-9]+|[0-9]*\\([0-9]+\\))"
		);

		for (String str : args) {
			Matcher m = p.matcher(str);
			System.out.println(m.matches());
		}
		
	}

}