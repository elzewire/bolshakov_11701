/**
* @author Edward Bolshakov
* 11-701
* Problem Set 2 Task 08
**/

import java.util.Scanner;

public class Task08PS2 {

	public static void backTracking(String s, int n) {
		if (s.length() == n) {
			System.out.println(s);
		} else if (s.length() == 0) {
			for (char c = '1'; c <= '9'; c += 1) {
				backTracking(s + c, n);
			}
		} else {
			char c2;

			for (char c = '0'; c <= '9'; c += 1) {
				c2 = s.charAt(s.length() - 1);
				if (areSimple(c, c2) != 1) {
					backTracking(s + c, n);
				}
			}
		}
	}

	public static int areSimple(char x1, char x2) {
		int b = (int)x1 > (int)x2 ? (int)x1 : (int)x2;
		int a = (int)x1 > (int)x2 ? (int)x2 : (int)x1;
		int tmp = 0;
		while (b > 0) {
			tmp = a % b;
			a = b;
			b = tmp;
		}
		return(Math.abs(a));
	}

	public static void main(String[] args) {
		
		String s = "";
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		backTracking(s, n);

	}

}