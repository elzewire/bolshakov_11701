import java.util.Scanner;

public class ArrayMax {

	public static int maxOfArray(int [] array) {

		int max = array[0];

		for (int n : array) {
			max = n > max ? n : max;
		}

		return max;

	}

	public static void main(String [] args) {

		Scanner sc = new Scanner(System.in);
		
		int n = sc.nextInt();
		int arr [] = new int[n];

		for (int i = 0; i < n; i++) {
			arr[i] = sc.nextInt();
		}

		System.out.println();
		System.out.println(maxOfArray(arr));

	}

}