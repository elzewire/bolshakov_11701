import java.util.Scanner;

public class ArrayShiftOne {

	public static void arrayShift(int [] array) {

		int first = array[0];
		int temp;

		for (int j = 0; j < array.length - 1; j++) {
			array[j] = array[j+1];
		}

		array[array.length - 1] = first;

	}

	public static void main(String [] args) {

		Scanner sc = new Scanner(System.in);
		
		int n = sc.nextInt();
		int arr [] = new int[n];

		for (int i = 0; i < n; i++) {
			arr[i] = sc.nextInt();
		}

		System.out.println();
		arrayShiftOne(arr);

		for (int i = 0; i < arr.length; i++) {
			System.out.print(arr[i] + ' ');
		}

	}

}