import java.util.Scanner;

public class MatrixToTriangleShape {

	public static void main(String [] args) {

		Scanner sc = new Scanner(System.in);

		int n = sc.nextInt();
		int m = sc.nextInt();

		int arr[][] = new int[n][m];

		for (int i = 0; i < n; i++) {
			for (int j = 0; j < m; j++) {
				arr[i][j] = sc.nextInt();
			}
		}

		int k = 0;
		int s = 0;
		int coef1;
		int coef2;
		while (k < n && s < m) {
			coef1 = arr[k][s];
			for (int i = k + 1; i < n; i++) {
				coef2 = arr[i][s];
				for (int j = 0; j < m; j++) {
					arr[i][j] *= coef1;
					arr[i][j] -= arr[k][j] * coef2;  
				}
			}
			k++;
			s++;
		}	

		System.out.println();

		for (int i = 0; i < n; i++) {
			for (int j = 0; j < m; j++) {
				System.out.print(arr[i][j] + " ");
			}
			System.out.println();
		}

	}

}