import java.util.regex.*;

public class From1000To1999 {

	public static void main(String [] args) {

		Pattern p = Pattern.compile("1[0-9]{3}");
		for (String str : args) {
			Matcher m = p.matcher(str);
			System.out.println(m.matches());
		}
	}
}