import java.util.regex.*;

public class TaskRE1 {

	public static void main(String [] args) {

		Pattern p = Pattern.compile("M[a-z]+");
		for (String str : args) {
			Matcher m = p.matcher(str);
			System.out.println(m.matches());
		}
	}
}