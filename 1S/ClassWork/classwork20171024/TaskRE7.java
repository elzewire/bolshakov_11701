import java.util.regex.*;

public class TaskRE7 {

	public static void main(String [] args) {

		Pattern p = Pattern.compile(
			"+7-[0-9]{3}-[0-9]{3}-[0-9]{2}-[0-9]{2}"
		);
		for (String str : args) {
			Matcher m = p.matcher(str);
			System.out.println(m.matches());
		}
	}
}