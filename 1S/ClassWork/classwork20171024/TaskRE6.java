import java.util.regex.*;

public class TaskRE6 {

	public static void main(String [] args) {

		Pattern p = Pattern.compile(
			"[ABCEHKMOPTXY][0-9]{3}[ABCEHKMOPTXY]{2}"
		);
		for (String str : args) {
			Matcher m = p.matcher(str);
			System.out.println(m.matches());
		}
	}
}