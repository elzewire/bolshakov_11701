import java.util.regex.*;

public class TaskRE4 {

	public static void main(String [] args) {

		Pattern p = Pattern.compile(
			"1[5-9][0-9]{2}|14[4-9][0-9]|143[2-9]|2[0-2][0-5][0-9]|23[0-6][0-5]|237[0-5]"
		);
		for (String str : args) {
			Matcher m = p.matcher(str);
			System.out.println(m.matches());
		}
	}
}