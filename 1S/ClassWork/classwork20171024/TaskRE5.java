import java.util.regex.*;

public class TaskRE5 {

	public static void main(String [] args) {

		Pattern p = Pattern.compile(
			"([0-1][0-9]|2[0-4]):[0-5][0-9]:[0-5][0-9]"
		);
		for (String str : args) {
			Matcher m = p.matcher(str);
			System.out.println(m.matches());
		}
	}
}