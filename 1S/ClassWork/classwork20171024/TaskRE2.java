import java.util.regex.*;

public class TaskRE2 {

	public static void main(String [] args) {

		Pattern p = Pattern.compile("(-?[1-9][0-9]*)|0");
		for (String str : args) {
			Matcher m = p.matcher(str);
			System.out.println(m.matches());
		}
	}
}