public class Task4 {
	
	public static boolean isLetter(char x) {
		if (x >= 'A' && x <= 'Z' || x >= 'a' && x <= 'z') {
			return true;
		} else {
			return false;
		}
	}

	public static boolean isDigit(char x) {
		if (x >= 48 &&  x <= 57) {
			return true;
		} else {
			return false;
		}
	}

	public static boolean correctAssignment(String str) {
		
		int s = 0;
		for (int i = 0; i < str.length(); i++) {
			switch (s) {
				case 0:
					if (isLetter(str.charAt(i))) {
						s = 1;
					} else {	
						return false;
					}
					break;

				case 1:
					if (str.charAt(i) == '=') {
						s = 2;
					} else if (isLetter(str.charAt(i)) || isDigit(str.charAt(i))) {
						s = 1;
					} else {
						return false;
					}
					break;

				case 2:
					if (isLetter(str.charAt(i))) {
						s = 3;
					} else if (isDigit(str.charAt(i))) {
						s = 4;
					} else {
						return false;
					}
					break;

				case 3:
					if (str.charAt(i) == ';') {
						s = 0;
					} else if (isLetter(str.charAt(i)) || isDigit(str.charAt(i))) {
						s = 3;
					} else {
						return false;
					}
					break;

				case 4:
					if (str.charAt(i) == ';') {
						s = 0;
					} else if (isDigit(str.charAt(i))) {
						s = 4;
					} else {
						return false;
					}
					break;
			}
		}

		if (s == 0 && str.length() > 0) {
			return true;
		} else {
			return false;
		}

	}

	public static void main(String [] args) {

		for (String str : args) {
			System.out.println(correctAssignment(str));
		}

	}

}