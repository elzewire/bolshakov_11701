public class Task5 {

	public static boolean correctBrackets(String str) {
		
		int k = 0;
		for (int i = 0; i < str.length(); i++) {
			if (str.charAt(i) == '(') {
				k++;
			} else if (str.charAt(i) == ')') {
				k--;
			}

			if (k < 0) {
				return false;
			}
		}

		if (k > 0) {
			return false;
		} else {
			return true;
		}

	}

	public static void main(String[] args) {
		
		for (String str : args) {
			System.out.println(correctBrackets(str));
		}

	}

}