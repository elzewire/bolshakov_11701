import java.util.regex.*;

public class TaskRE03 {
	
	public static void main(String[] args) {
		
		Pattern p = Pattern.compile(
			"C:\\\\([a-zA-Z0-9]+\\\\)*[a-zA-Z0-9]+\\.pdf"
		);

		for (String str : args) {
			Matcher m = p.matcher(str);
			System.out.println(m.matches());
		}

	}

}