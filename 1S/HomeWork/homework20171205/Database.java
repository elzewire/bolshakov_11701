import java.util.Scanner;
import java.util.Date;
import java.io.*;
import java.text.*;

public class Database {

	public static User[] users;
	public static Message[] messages;
	public static Post[] posts;
	public static Like[] likes;
	public static Sub[] subs;
	
	private static final String DATA_PATH = "Data/";
	private static final String USERS_PATH = DATA_PATH + "users.txt";
	private static final String MESSAGES_PATH = DATA_PATH + "messages.txt";
	private static final String LIKES_PATH = DATA_PATH + "likes.txt";
	private static final String POSTS_PATH = DATA_PATH + "posts.txt";
	private static final String SUBS_PATH = DATA_PATH + "subscriptions.txt";

	private static final String DATE_FORMAT = "dd-mm-yyyy, hh:mm:ss";

	public static User[] loadUsers() throws FileNotFoundException {

		Scanner sc = new Scanner(new File(USERS_PATH));	

		int n = Integer.parseInt(sc.nextLine());
		users = new User[n];

		for (int i = 0; i < n; i++) {
			sc.nextLine();

			long id = Long.parseLong(sc.nextLine());
			String username = sc.nextLine();
			String password = sc.nextLine();
			String email = sc.nextLine();

			users[i] = new User(id, username, password, email);
		}

		return users;
	} 

	public static Message[] loadMessages() throws FileNotFoundException, ParseException {
		
		DateFormat format = new SimpleDateFormat(DATE_FORMAT);

		Scanner sc = new Scanner(new File(MESSAGES_PATH));	

		int n = Integer.parseInt(sc.nextLine());
		messages = new Message[n];

		for (int i = 0; i < n; i++) {
			sc.nextLine();

			User sender = getUserById(Long.parseLong(sc.nextLine()));
			User receiver = getUserById(Long.parseLong(sc.nextLine()));
			String text = sc.nextLine();
			Date date = format.parse(sc.nextLine());

			messages[i] = new Message(sender, receiver, text, date);
		}

		return messages;

	}

	public static Post[] loadPosts() throws FileNotFoundException, ParseException {
		
		DateFormat format = new SimpleDateFormat(DATE_FORMAT);

		Scanner sc = new Scanner(new File(POSTS_PATH));	

		int n = Integer.parseInt(sc.nextLine());
		posts = new Post[n];

		for (int i = 0; i < n; i++) {
			sc.nextLine();

			long id = Long.parseLong(sc.nextLine());
			User user = getUserById(Long.parseLong(sc.nextLine()));
			String text = sc.nextLine();
			Date date = format.parse(sc.nextLine());

			posts[i] = new Post(id, user, text, date);
		}

		return posts;

	}

	public static Like[] loadLikes() throws FileNotFoundException {
		
		Scanner sc = new Scanner(new File(LIKES_PATH));	

		int n = Integer.parseInt(sc.nextLine());
		likes = new Like[n];

		for (int i = 0; i < n; i++) {
			sc.nextLine();

			User user = getUserById(Long.parseLong(sc.nextLine()));
			Post post = getPostById(Long.parseLong(sc.nextLine()));

			likes[i] = new Like(user, post);
		}

		return likes;

	}

	public static Sub[] loadSubs() throws FileNotFoundException {
		
		Scanner sc = new Scanner(new File(SUBS_PATH));	

		int n = Integer.parseInt(sc.nextLine());
		subs = new Sub[n];

		for (int i = 0; i < n; i++) {
			sc.nextLine();

			User follower = getUserById(Long.parseLong(sc.nextLine()));
			User target = getUserById(Long.parseLong(sc.nextLine()));

			subs[i] = new Sub(follower, target);
		}

		return subs;

	}

	public static User getUserById(long id) {

		for (User user : users) {
			if (user.getId() == id) {
				return user;
			}
		}

		return null;

	}

	public static Post getPostById(long id) {

		for (Post post : posts) {
			if (post.getId() == id) {
				return post;
			}
		}

		return null;

	}

}