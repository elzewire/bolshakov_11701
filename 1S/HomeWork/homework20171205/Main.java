import java.util.Arrays;
import java.io.FileNotFoundException;
import java.text.ParseException;	

public class Main {

	public static void main(String[] args) 
			throws FileNotFoundException, ParseException {
		

		//Task 2
		Database.loadUsers();
		Database.loadMessages();
		Database.loadPosts();
		Database.loadLikes();
		Database.loadSubs();
		
		System.out.println(Arrays.toString(Database.users));
		System.out.println(Arrays.toString(Database.posts));

		showAllFriends();
		showUsersChat("Daler", "Edward");

	}

	//Task 3
	public static void showAllFriends() {

		for (Sub sub1 : Database.subs) {
			for (Sub sub2 : Database.subs) {
				if (sub1.getTarget() == sub2.getFollower() 
					&& sub1.getFollower() == sub2.getTarget()) {

					System.out.println(sub1.getFollower().getUsername() + 
						" and " + sub1.getTarget().getUsername() + " are friends");
				}
			}
		}

	}

	//Task 4
	public static void showUsersChat(String u1, String u2) {

		System.out.println("\nChat between " + u1 + " and " + u2 + ":");

		for (Message message : Database.messages) {
			if (message.getReceiver().getUsername().equals(u2) && message.getSender().getUsername().equals(u1)) {
				System.out.println(u2 + ": " + message.getText());
			} else if (message.getReceiver().getUsername().equals(u1) && message.getSender().getUsername().equals(u2)) {
				System.out.println(u1 + ": " + message.getText());
			}
		}

		System.out.println("End of chat\n");
		
	}

}