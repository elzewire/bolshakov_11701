public class Sub {

	private User follower;
	private User target;

	public Sub(User follower, User target) {
		this.follower = follower;
		this.target = target;
	}

	//Getters
	public User getFollower() {
		return follower;
	}

	public User getTarget() {
		return target;
	}

}