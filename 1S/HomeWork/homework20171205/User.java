public class User {

	private long id;
	private String username;
	private String password;
	private String email;

	public User(long id, String username, String password, String email) {
		this.id = id;
		this.username = username;
		this.password = password;
		this.email = email;
	}

	//Getters
	public long getId() {
		return id;
	}

	public String getUsername() {
		return username;
	}

	public String getPassword() {
		return password;
	}

	public String getEmail() {
		return email;
	}

	//To String
	public String toString() {
		return username;
	}

}