import java.util.Date;

public class Post {

	private long id;
	private User user;
	private String text;
	private Date date;

	public Post(long id, User user, String text, Date date) {
		this.id = id;
		this.user = user;
		this.text = text;
		this.date = date;
	}

	//Getters
	public long getId() {
		return id;
	}

	public User getUser() {
		return user;
	}

	public String getText() {
		return text;
	}

	public Date getDate() {
		return date;
	}

	//ToString
	public String toString() {
		return "\n\nPost №" + id + "\nDate: " + date + "\nPosted by: " + user.getUsername() + "\n\n" + text;
	}

}