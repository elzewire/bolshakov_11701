import java.util.Scanner;
public class Task77c {

	public static void main(String [] args) {

		Scanner sc = new Scanner(System.in);
		double x = 1;
		int n = sc.nextInt();
		for (var i = 1; i <= n; i++) {
			x *= (1 + 1/(i*i));
		}
		System.out.print(x);

	}

}