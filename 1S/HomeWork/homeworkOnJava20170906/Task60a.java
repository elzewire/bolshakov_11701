import java.util.Scanner;
public class Task60a {

	public static void main(String [] args) {

		Scanner sc = new Scanner(System.in);
		double x = sc.nextDouble();
		double y = sc.nextDouble();
		double u;
		if (y >= 0) && (x * x + y * y <= 4) && (x * x + y * y >= 1) {
			u = 0;
		} else {
			u = x;
		}
		System.out.print(u);

	}

}