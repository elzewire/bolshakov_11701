import java.util.Scanner;
public class Task59a {

	public static void main(String [] args) {

		Scanner sc = new Scanner(System.in);
		double x = sc.nextDouble();
		double y = sc.nextDouble();
		if (x * x + y * y <= 1) {
			System.out.print('True');
		} else {
			System.out.print('False');
		}
		
	}

}