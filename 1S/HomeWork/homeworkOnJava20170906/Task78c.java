import java.util.Scanner;
public class Task78c {

	public static void main(String [] args) {

		Scanner sc = new Scanner(System.in);
		double x = 0;
		int k = 1;
		int n = sc.nextInt();
		double a = sc.nextDouble();
		for (var i = 0; i <= n; i++) {
			k *= a + i;
			x += 1/k;
		}
		System.out.print(x);

	}

}