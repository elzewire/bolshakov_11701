import java.util.Scanner;
import java.lang.Math;
public class Task59f {

	public static void main(String [] args) {

		Scanner sc = new Scanner(System.in);
		double x = sc.nextDouble();
		double y = sc.nextDouble();
		boolean case1 = (Math.abs(x) + 2 * Math.abs(y) <= 2) && (x <= 0);
		boolean case2 = (x * x + y * y <= 1) && (x > 0);
		if (case1) || (case2) {
			System.out.print('True');
		} else {
			System.out.print('False');
		}
		
	}

}