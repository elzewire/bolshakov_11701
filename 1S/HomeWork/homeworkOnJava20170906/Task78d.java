import java.util.Scanner;
public class Task78d {

	public static void main(String [] args) {

		Scanner sc = new Scanner(System.in);
		double x = 0;
		int n = sc.nextInt();
		double a = sc.nextDouble();
		for (var i = 0; i <= n; i++) {
			x += 1/a;
			a *= a;
		}
		System.out.print(x);

	}

}