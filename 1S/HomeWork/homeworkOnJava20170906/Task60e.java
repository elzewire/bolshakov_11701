import java.util.Scanner;
import java.lang.Math;
public class Task60e {

	public static void main(String [] args) {

		Scanner sc = new Scanner(System.in);
		double x = sc.nextDouble();
		double y = sc.nextDouble();
		double u;
		if (y >= Math.abs(x)) && (x * x + y * y <= 1) {
			u = Math.sqrt(Math.abs(x * x - 1));
		} else {
			u = x + y;
		}
		System.out.print(u);

	}

}