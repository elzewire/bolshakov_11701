import java.util.Scanner;
import java.lang.Math;
public class Task77g {

	public static void main(String [] args) {

		Scanner sc = new Scanner(System.in);
		double x = 0;
		int n = sc.nextInt();
		for (var i = 1; i <= n; i++) {
			x = Math.sqrt(3 * (n - i + 1) + x);
		}
		System.out.print(x);

	}

}