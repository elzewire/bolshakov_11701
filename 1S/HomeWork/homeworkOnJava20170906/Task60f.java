import java.util.Scanner;
import java.lang.Math;
public class Task60f {

	public static void main(String [] args) {

		Scanner sc = new Scanner(System.in);
		double x = sc.nextDouble();
		double y = sc.nextDouble();
		double u;
		if (y >= x * x) && (y <= Math.exp(x)) && (y <= Math.exp(-x)) {
			u = x + y;
		} else {
			u = x - y;
		}
		System.out.print(u);

	}

}