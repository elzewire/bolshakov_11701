import java.util.Scanner;
import java.lang.Math;
public class Task59e {

	public static void main(String [] args) {

		Scanner sc = new Scanner(System.in);
		double x = sc.nextDouble();
		double y = sc.nextDouble();
		if (Math.abs(x) + 0.5 * Math.abs(y) <= 0.5) {
			System.out.print('True');
		} else {
			System.out.print('False');
		}
		
	}

}