import java.util.Scanner;
public class Task88d {

	public static void main(String [] args) {

		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		int x = n;
		int k = 1;
		while (n > 0) {
			n = n / 10;
			k = k * 10;
		}
		x = (k + x) * 10 + 1;

		System.out.print(x);

	}

}