import java.util.Scanner;
public class Task59i {

	public static void main(String [] args) {

		Scanner sc = new Scanner(System.in);
		double x = sc.nextDouble();
		double y = sc.nextDouble();
		boolean case1 = (((y <= 2 * (x + 1.5)) && (y >= (x + 0.5)/3-0.5) && (y <= -x)) && (x <= 0));
		boolean case2 = ((y <= 0) && (y >= (x + 0.5)/3 - 0.5) && (x > 0)) ;
		if (case1) || (case2) {
			System.out.print('True');
		} else {
			System.out.print('False');
		}
		
	}

}