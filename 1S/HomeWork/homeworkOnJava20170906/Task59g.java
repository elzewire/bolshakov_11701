import java.util.Scanner;
import java.lang.Math;
public class Task59g {

	public static void main(String [] args) {

		Scanner sc = new Scanner(System.in);
		double x = sc.nextDouble();
		double y = sc.nextDouble();
		if (y >= -1) && (3 * Math.abs(x) + Math.abs(y + 1) <= 3) {
			System.out.print('True');
		} else {
			System.out.print('False');
		}
		
	}

}