import java.util.Scanner;
public class Task60c {

	public static void main(String [] args) {

		Scanner sc = new Scanner(System.in);
		double x = sc.nextDouble();
		double y = sc.nextDouble();
		double u;
		if (y <= 1 - x * x) && (x * x + (y - 1) * (y - 1) <= 1) {
			u = x - y;
		} else {
			u = x * y + 7;
		}
		System.out.print(u);

	}

}