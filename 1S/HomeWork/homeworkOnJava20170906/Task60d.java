import java.util.Scanner;
import java.lang.Math;
public class Task60d {

	public static void main(String [] args) {

		Scanner sc = new Scanner(System.in);
		double x = sc.nextDouble();
		double y = sc.nextDouble();
		boolean case1 = ((x <= 0) && (x * x + y * y <= 1));
		boolean case2 = ((x > 0) && (x * x + y * y >= 0.09) && (x * x + y * y <= 1));
		double u;
		if (case1 || case2) && (y >= 0) {
			u = x * x - 1;
		} else {
			u = Math.sqrt(Math.abs(x - 1));
		}
		System.out.print(u);

	}

}