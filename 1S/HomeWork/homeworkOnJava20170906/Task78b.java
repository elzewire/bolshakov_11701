import java.util.Scanner;
public class Task78b {

	public static void main(String [] args) {

		Scanner sc = new Scanner(System.in);
		double x = 1;
		int n = sc.nextInt();
		double a = sc.nextDouble();
		for (var i = 1; i <= n; i++) {
			x *= a+i-1;
		}
		System.out.print(x);

	}

}