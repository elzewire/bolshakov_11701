import java.util.Scanner;
import java.lang.Math;
public class Task59c {

	public static void main(String [] args) {

		Scanner sc = new Scanner(System.in);
		double x = sc.nextDouble();
		double y = sc.nextDouble();
		if (Math.abs(x) <= 1) && (Math.abs(y) <= 1) {
			System.out.print('True');
		} else {
			System.out.print('False');
		}
		
	}

}