import java.util.Scanner;
import java.lang.Math;
public class Task77d {

	public static void main(String [] args) {

		Scanner sc = new Scanner(System.in);
		double x = 0;
		double k = 0;
		int n = sc.nextInt();
		for (var i = 1; i <= n; i++) {
			k += sin(i);
			x += 1/k;
		}
		System.out.print(x);

	}

}