import java.util.Scanner;
public class Task60b {

	public static void main(String [] args) {

		Scanner sc = new Scanner(System.in);
		double x = sc.nextDouble();
		double y = sc.nextDouble();
		double u;
		if (y <= x/2) && (x * x + y * y <= 1) {
			u = -3;
		} else {
			u = y * y;
		}
		System.out.print(u);

	}

}