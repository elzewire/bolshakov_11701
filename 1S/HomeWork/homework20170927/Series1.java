import java.lang.Math;

public class MyClass41 {
	
	public static void main(String [] args) {

		final double EPS = 1e-9;//0.000000001; 

		double x = Double.parseDouble(args[0]);

		int k = 1;
		double s = 0;
		double deg = 1;
		double a = deg / k;
		s += a;

		while (Math.abs(a) > EPS) {

			System.out.println(s);
			deg *= x;
			a = deg / (k + 1);
			s += a;
			k++;

		}

		System.out.print(s);

	}

}