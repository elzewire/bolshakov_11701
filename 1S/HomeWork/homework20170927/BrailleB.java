public class BrailleB {

	public static void main(String [] args) {

		int x;
		int arr[][]  = new int[3][100];
		int k = 0;
		//byte bytes;
		//string br;

		for (String s: args) {

			x = Integer.parseInt(s);
			for (int i = 0; i < 3; i++) {
				for (int j = 0; j < 2; j++) {
					arr[i][j + k] = x % 2;
					x = x >> 1;
				}
			}
			k += 2;

		}

		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < k; j++) {
				if (arr[i][j] == 1) {
					System.out.print('*');
				} else {
					System.out.print(' ');
				}
			}
			System.out.println();
		}

	}

}