import java.lang.Math;

public class MyClass41 {
	
	public static void main(String [] args) {

		final double EPS = 1e-9;//0.000000001; 

		double x = Double.parseDouble(args[0]);

		int k = 1;
		double s = 0;
		double deg = 1;
		double fact = 1;
		double a = deg / k;
		s += a;

		while (Math.abs(a) > EPS) {

			deg *= x*x;
			fact *= 2 * k * (2 * k - 1);
			a = deg / fact;
			s += a;
			k++;
			System.out.println(s);

		}

		System.out.print(s);

	}

}