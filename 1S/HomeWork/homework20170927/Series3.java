import java.lang.Math;

public class MyClass41 {
	
	public static void main(String [] args) {

		final double EPS = 1e-9;//0.000000001; 

		double x = Double.parseDouble(args[0]);

		int k = 1;
		double s = 0;
		double degX = 1;
		double deg2 = 1;
		double fact1 = 1;
		double fact2 = 1;
		double fact = 1;
		double a = (degX * deg2) / fact;
		s += a;

		while (Math.abs(a) > EPS) {

			System.out.println(s);
			degX *= x * x;
			deg2 *= 2;
			if (k % 2 == 1) {
				fact1 *= k;
				fact = fact1;
			} else {
				fact2 *= k;
				fact = fact2;
			}
			a = degX * deg2 / fact;
			s += a;
			k++;

		}

		System.out.print(s);

	}

}