﻿//Генетический алгоритм
//Построить строки длины 5 у которых расстояниe < 10

/*
1.Создать популяцию 100 строк из 5 символов
2.Целевая функция f(s) = 0 - все ок
	Кол-во нарушений
	Сортировка
3.Скрещивание:
	s1 s2
 12121 34343
 '''      ''
   \     /
	12143

*/
import java.util.Random;
import java.util.Arrays;

public class GeneticAlgorythm {

	public static final int L = 5;
	public static final int D = 10;

	public static void main(String[] args) {
		
		int n = 100;
		String[] arr = createRandom1DArray(n);
		
		System.out.println(Arrays.toString(arr));

		geneticAlgorythm(arr, 0);

	}

	public static String[] createRandom1DArray(int n) {
		
		Random rnd = new Random();

		String[] arr = new String[n];

		for (int i = 0; i < n; i++) {
			arr[i] = "";
			for (int j = 0; j < L; j++) {
				arr[i] += (char)('a' + rnd.nextInt(26));
			}
		}

		return arr;

	}

	public static void geneticAlgorythm(String[] arr, int gen) {

		//Print info
		System.out.println("Generation: " + gen);
		System.out.println(arr[0]);
		System.out.println(Arrays.toString(arr));


		//Create new generation
		if (checkMistake(arr[0]) > 0) {
			geneticAlgorythm(createNewGeneration(arr), ++gen);
		} else {
			System.out.println(arr[0]);
		}

		//Run sorting program
		sortByMistakes(arr);

	}

	public static void sortByMistakes(String[] arr) {

		String temp;

		for (int i = 0; i < arr.length - 1; i++) {
			for (int j = 0; j < arr.length; j++) {
				if (checkMistake(arr[i]) > checkMistake(arr[i + 1])) {
					temp = arr[i];
					arr[i] = arr[i + 1];
					arr[i + 1] = temp;
				}
			}
		}

	}

	public static int checkMistake(String str) {
		
		int k = 0;
		char c;
		char c2;

		for (int i = 1; i < str.length(); i++) {
			c2 = str.charAt(i - 1);
			c = str.charAt(i);
			if (Math.abs(c - c2) > D) {
				k++;
			}
		}

		return k;

	}

	public static String[] createNewGeneration(String[] arr) {

		String p1 = arr[0];
		String p2 = arr[1];

		for (int i = 0; i < arr.length; i++) {
			arr[i] = crossover(p1, p2);
		}

		return arr;

	}

	public static String crossover(String p1, String p2) {

		Random rnd = new Random();

		String child = "";

		for (int i = 0; i < L; i++) {
			if (rnd.nextBoolean()) {
				child += p1.charAt(rnd.nextInt(L));
			} else {
				child += p2.charAt(rnd.nextInt(L));	
			}
		}

		return child;

	}

}