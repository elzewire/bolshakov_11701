public class Player {

	private int hp;
	private int maxhp; 
	private String name;

	public Player(String name) {
		hp = 100;
		maxhp = 100;
		this.name = name;
	}

	public void decreaseHp(int power) {
		hp -= power;
	}

	public void punch(Player player, int power) {
		player.decreaseHp(power);
		System.out.println(player.getName() + " loses " + power + " hp!\n");
	}

	public int getHp() {
		return hp;
	}

	public String getName() {
		return name;
	}

}