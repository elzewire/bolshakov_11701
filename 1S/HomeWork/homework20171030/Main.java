import java.util.Random;
import java.util.Scanner;

public class Main {
	
	public static void main(String [] args) {

		Scanner sc = new Scanner(System.in);
		Random random = new Random();

		System.out.println("(Enter -1 instead of power to exit)\n");

		System.out.print("Player 1, enter your name: ");
		String name1 = sc.nextLine();
		System.out.print("Choose player class: ");
		int class1 = sc.nextInt();
		System.out.print("PLayer 2, enter your name: ");
		String name2 = sc.nextLine();
		System.out.print("Choose player class: ");
		int class2 = sc.nextInt();

		switch (class1) {
			case 1:
				Player p1 = new Player(name1);
				break;
			
			case 2:
				PlayerVampire p1 = new PlayerVampire(name1);
				break;
			
			case 3:
				PlayerArmored p1 = new PlayerArmored(name1);
				break;

			case 4:
				System.out.println("PLayer 1, enter your battlecry: ");
				String bc1 = sc.nextLine();
				PlayerBerserk p1 = new PlayerBerserk(name1, bc1);
		}

		switch (class2) {
			case 1:
				Player p2 = new Player(name2);
				break;
			
			case 2:
				PlayerVampire p2 = new PlayerVampire(name2);
				break;
			
			case 3:
				PlayerArmored p2 = new PlayerArmored(name2);
				break;

			case 4:
				System.out.println("PLayer 2, enter your battlecry: ");
				String bc2 = sc.nextLine();
				PlayerBerserk p2 = new PlayerBerserk(name2, bc2);
		}

		int turn = 1;
		int maxPower = 9;

		while (p1.getHp() > 0 && p2.getHp() > 0) {

			System.out.print(p1.getName() + "'s hp: " + p1.getHp() + "  ");
			System.out.println(p2.getName() + "'sc hp: " + p2.getHp());
			System.out.println();
			if (turn > 0) {
				System.out.print(p1.getName() + ", enter the power of hit(1-9): ");				
			} else {
				System.out.print(p2.getName() + ", enter the power of hit(1-9): ");
			}
			int power = sc.nextInt();

			if (power < 1 && power != -1 || power > 9) {
				System.out.println("Wrong input!\n");
				continue;
			} else if (power == -1) {
				break;
			}

			double amount = (double)((maxPower + 1.0) - ((maxPower + 1.0) / power));
			double chance = random.nextDouble() * 10;

			if (amount <= chance) {
				if (turn > 0) {
					p1.punch(p2, power);
				} else {
					p2.punch(p1, power);
				}
			} else {
				System.out.println("You've missed!\n");
			}

			turn *= -1;

		}

		if (p1.getHp() <= 0) {
			System.out.println("PLayer " + p2.getName() + " wins!");
		} else if (p2.getHp() <= 0) {
			System.out.println("PLayer " + p1.getName() + " wins!");
		}

	}

}