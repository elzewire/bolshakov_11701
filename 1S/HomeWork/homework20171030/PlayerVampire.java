public class PlayerVampire extends Player {

	public void punch(Player player, int power) {
		super.punch(player, power);
		
		if (hp + power < maxhp) {
			hp += power;
			System.out.println(this.name + " restored " + power	 + " hp!\n");
		} else {
			System.out.println(this.name + " restored " + (maxhp - hp) + " hp!\n");
			hp = maxhp;
		}

	}

}