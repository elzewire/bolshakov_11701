public class PlayerArmored extends Player {
	
	private int armor;

	public PlayerArmored(String name) {
		super.Player(name);
		armor = 2;
	}

	public void decreaseHp(Player player, int power) {
		
		super.decreaseHp(player, power);
		if (power > armor) {
			hp += armor;
		} else {
			hp += power;
		}

	}

}