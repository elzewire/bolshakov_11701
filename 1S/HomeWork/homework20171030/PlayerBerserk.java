public class PlayerBerserk extends Player {

	private String battleCry;

	public PlayerBerserk(String name, String battleCry) {
		super.Player(name);
		this.battleCry = battleCry;
	}

	public void punch(Player player, int power) {
		player.decreaseHp(power*2);
		System.out.println(name + " shouts: " + battleCry);
		System.out.println(player.getName() + " loses " + power + " hp!\n");
	}

}