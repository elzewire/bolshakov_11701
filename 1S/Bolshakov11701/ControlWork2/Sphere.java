/**
* @author Edward Bolshakov
* 11-701
* Control Work 2
**/

public class Sphere implements Volumeable {

	private int rad;
	private double pi;

	public Sphere(int rad) {
		this.rad = rad;
		pi = 3.14;
	}	

	//Getters
	public double getV() {
		//formula 4/3(piR^3)
		return (double)(4 * pi * rad * rad * rad / 3);
	}

	public double getS() {
		//formula 4piR^2
		return (double)(4 * pi * rad * rad);
	}

/*	public String getFigure() {
		return "Sphere: " + rad;
	}*/
	
}