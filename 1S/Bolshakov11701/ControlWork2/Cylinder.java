/**
* @author Edward Bolshakov
* 11-701
* Control Work 2
**/

public class Cylinder implements Volumeable {

	private int rad;
	private int height;
	private double pi;

	public Cylinder(int rad, int height) {
		this.rad = rad;
		this.height = height;
		pi = 3.14;
	}	

	//Getters
	public double getV() {
		//formula piR^2h
		return (double)(pi * rad * rad * height);
	}

	public double getS() {
		//formula 2piR(R + h)
		return (double)(2 * pi * rad * (rad + height));
	}

/*	public String getFigure() {
		return "Cylinder: rad: " + rad + " height: " + height;
	}*/

}