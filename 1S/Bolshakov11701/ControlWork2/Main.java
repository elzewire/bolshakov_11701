/**
* @author Edward Bolshakov
* 11-701
* Control Work 2
**/

import java.util.Random;

public class Main {
	
	public static void main(String[] args) {
	
		final int N = 10;
		Volumeable[] vols = new Volumeable[N];
		Random rnd = new Random();
		
		//Fill
		for (int i = 1; i < N; i += 2) {
			vols[i - 1] = new Cylinder(rnd.nextInt(9) + 1, rnd.nextInt(9) + 1);
			vols[i] = new Sphere(rnd.nextInt(9) + 1);
		}

		//Show
		for (Volumeable vol : vols) {
			//System.out.println(vol.getFigure());
			System.out.println(vol.getV());
			System.out.println(vol.getS());
		}
	
	}

}