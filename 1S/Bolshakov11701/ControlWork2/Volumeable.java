/**
* @author Edward Bolshakov
* 11-701
* Control Work 2
**/

public interface Volumeable {

	double getV ();
  	double getS ();
  	//String getFigure();

}