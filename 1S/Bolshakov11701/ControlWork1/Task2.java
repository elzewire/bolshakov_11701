import java.util.Scanner;

public class Task1 {

	public static int digitsSum(int x) {
		int s = 0;

		while (x > 0) {
			s += x % 10;
			x = x / 10;
		}

		return s;
	}

	public static boolean hasSalvation(int a, int b, int c) {
		//x^2 + (2^i + i!) * x - a[i]! = 0
		int d;
		d = (b)*(b) - 4 * a * c; 

		return D >= 0 ? true : false;
	}

	public static int getAmountOf(int [] arr) {
		int k = 0;
		int a = 1;
		int b1 = 1;
		int b2 = 1;
		int b = b1 + b2;
		int c = 1;

		for (int i = 0; i < arr.length; i++) {
			if (i > 0) {
				b1 *= 2;
				b2 *= i;
				b = b1 + b2;
				c *= a[i];
			}

			if (hasSalvation(a, b, c)) {
				k++;
			}
		}

		return k;
	}

	public static boolean check(int x) {


		while (x > 0) {
			if (x % 2 == 1) {
				return true;
			}

			x = x / 10;
		}

		return false;
	}


	public static void main(String [] args) {

		Scanner sc = new Scanner(System.in);

		int n = sc.nextInt();
		int[] a = new int[n];

		for (int i = 0; i < n; i++) {
			a[i] = sc.nextInt();
		}

		System.out.println(check(digitsSum(getAmountOf(a))));

	}

}