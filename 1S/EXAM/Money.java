/**
* @author Edward Bolshakov
* 11-701
* Exam Task 5 
**/

public class Money {

	long rub;
	byte cop;

	Money(long rub, byte cop) {
		this.rub = rub;
		this.cop = cop;
	}

	public int compareTo(Money other) {

		if (getRub() != other.getRub()) {
			return Money.sign(getRub() - other.getRub());
		} else {
			return Money.sign((long)(getCop() - other.getCop()));		
		}

		/*if (getRub() > other.getRub()) {
			return 1;
		} else if (getRub() == other.getRub()) {
			if (getCop() > other.getCop()) {
				return 1;
			} else if (getCop() == other.getCop()) {
				return 0;
			} else {
				return -1;
			}
		} else {
			return -1;
		}*/

	}

	public Money add(Money other) {
		long nRub = other.getRub() + rub;
		int nCop = other.getCop() + cop;

		if (nCop >= 100) {
			nRub += nCop / 100;
			nCop = nCop % 100;
		}

		return(new Money(nRub, (byte)nCop));
	}

	public Money mult(int mult) {
		long nRub = getRub() * mult;
		int nCop = getCop() * mult;

		if (nCop >= 100) {
			nRub += nCop / 100;
			nCop = nCop % 100;
		}

		return(new Money(nRub, (byte)nCop));		
	}

	public Money sub(Money other) {
		if (compareTo(other) > 0) {

			long nRub = rub - other.getRub();
			int nCop = cop + 100 - other.getCop();

			if (nCop < 100) {
				nRub -= 1;
				nCop = Math.abs(nCop) % 100;
			}

			return new Money(nRub, (byte)nCop);
		} else {
			return other.sub(this);
		}
	}

	public String toString() {
		return rub + "," + cop;
	}

	//Getters
	public long getRub() {
		return rub;
	}

	public byte getCop() {
		return cop;
	}

	public static int sign(long x) {
		return x > 0 ? x == 0 ? 0 : 1 : -1;
	}

	//Testing
	public static void main(String[] args) {
		
		Money mon1 = new Money(123, (byte)45);
		Money mon2 = new Money(76, (byte)71);

		System.out.println(mon1.add(mon2));
		System.out.println(mon1.compareTo(mon2));
		System.out.println(mon2.compareTo(mon1));
		System.out.println(mon1.sub(mon2));
		System.out.println(mon2.sub(mon1));
		System.out.println(mon1.mult(5));
		System.out.println(mon2.mult(19));
	}

}