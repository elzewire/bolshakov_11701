/**
* @author Edward Bolshakov
* 11-701
* Exam Task 18 
**/

public class MyDouble {

	public static double myParseDouble(String num) {
		
		long x = 0;
		long div = 1;
		boolean split = false;

		for (int i = 0; i < num.length(); i++) {
			if (split) {
				div *= 10;
			}

			if (num.charAt(i) != ',') {
				x = x * 10 + (long)(num.charAt(i) - 48);
			} else {
				split = true;
			}
		}

		return (double)x / div;
	}

	public static void main(String[] args) {
		
		System.out.println(myParseDouble("13231,00931"));
		System.out.println(myParseDouble("0,00931"));
		System.out.println(myParseDouble("000,00931"));

	}

}