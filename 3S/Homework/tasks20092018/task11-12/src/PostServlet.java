import dao.entities.Post;
import helpers.Helper;
import services.PostService;
import services.UserService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;

public class PostServlet extends HttpServlet {

    private UserService userService = new UserService();
    private PostService postService = new PostService();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String rel_path = req.getPathInfo();
        if (postService.hasId(rel_path)) {
            int id = postService.getId(rel_path);
            Post post = postService.getPostById(id);
            if (post != null) {
                HashMap<String, Object> root = new HashMap<>();
                root.put("post", post);
                Helper.render(getServletContext(), resp, "post.ftl", root);
            }
        } else {
            List<Post> posts = postService.getPosts();
            if (posts != null) {
                HashMap<String, Object> root = new HashMap<>();
                root.put("posts", posts);
                Helper.render(getServletContext(), resp, "posts.ftl", root);
            }
        }
    }
}
