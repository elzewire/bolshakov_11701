package services;

import dao.PostDAO;
import dao.UserDAO;
import dao.postgres_impl.PostgresPostDAO;
import dao.postgres_impl.PostgresUserDAO;
import dao.entities.Post;
import dao.entities.User;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

public class UserService {

    private UserDAO userDAO = new PostgresUserDAO();
    private PostDAO postDAO = new PostgresPostDAO(userDAO);

    public User getCurrentUser(HttpServletRequest req) {
        HttpSession session = req.getSession();
        return (User) session.getAttribute("current_user");
    }

    public User authenticate(HttpServletRequest req) {
        String username = req.getParameter("username");
        if (username != null) {
            User user = userDAO.getByUsername(username);
            if (user == null) {
                return null;
            }
            String password = req.getParameter("password");
            if (password.equals(user.getPassword())) {
                return user;
            }
        }
        return null;
    }

    public void authorize(User current_user, HttpServletRequest req) {
        HttpSession session = req.getSession();
        session.setAttribute("current_user", current_user);
    }

    public List<Post> getUserPosts(User user) {
        return (postDAO.getByAuthor(user));
    }

    public Post getPost(int id) {
        return (postDAO.getById(id));
    }

    public boolean validatePost(String text) {
        if (text != null) {
            return true;
        }
        return false;
    }

    public void createPost(User user, String text) { postDAO.createPost(user, text); }

    public User getViewedUser(int id) {
        return userDAO.getById(id);
    }
}