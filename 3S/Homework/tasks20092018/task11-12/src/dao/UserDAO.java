package dao;

import dao.entities.User;

import java.util.List;

public interface UserDAO {

    public User getById(int id);
    public User getByUsername(String username);
    public List<User> getByName(String name);
    public void createUser(String username, String password, String name);
    public void updateUser(int id);
    public void deleteUser(int id);

}
