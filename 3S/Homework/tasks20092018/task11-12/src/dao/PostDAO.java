package dao;

import dao.entities.Post;
import dao.entities.User;

import java.util.List;

public interface PostDAO {

    public Post getById(int id);
    public List<Post> getByAuthor(User author);
    public void createPost(User author, String text);
    public void updatePost(Post post);
    public void deletePost(int id);
    public List<Post> getAll();
}
