import dao.entities.User;
import helpers.Helper;
import services.UserService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class LoginServlet extends HttpServlet {

    private UserService userService = new UserService();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (userService.getCurrentUser(req) != null) {
            resp.sendRedirect("/profile");
        } else {
            resp.setContentType("text/html");
            Helper.render(getServletContext(), resp, "login.ftl");
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (userService.getCurrentUser(req) != null) {
            resp.sendRedirect("/profile");
        } else {
            User current_user = userService.authenticate(req);
            if (current_user != null) {
                userService.authorize(current_user, req);
                resp.sendRedirect("/profile");
            } else {
                resp.sendRedirect("/login");
            }
        }
    }
}
