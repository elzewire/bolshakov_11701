import dao.entities.Post;
import dao.entities.User;
import services.UserService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class ProfileServlet extends HttpServlet{

    private UserService userService = new UserService();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        if (userService.getCurrentUser(req) == null) {
            resp.sendRedirect("/login");
        } else {
            String rawId = req.getParameter("id");
            User user = (User) req.getSession().getAttribute("current_user");
            if (rawId != null && user.getId() != Integer.parseInt(rawId)) {
                //Render other profile
                User viewed_user = userService.getViewedUser(Integer.parseInt(rawId));
                resp.setContentType("text/html");
                resp.getWriter().write("<h1><i>" + viewed_user.getName() + "'s</i> profile</h1>");
                List<Post> posts = userService.getUserPosts(viewed_user);
                if (posts == null) {
                    resp.getWriter().write( viewed_user.getName() + " hasn't posted anything yet");
                } else {
                    for (Post post : posts) {
                        resp.getWriter().write("<div><h1><a href='/post/" + post.getId() + "'>" + post.getId()
                                + "</a></h1>" + post.getText() + "</div>");
                    }
                }
                resp.getWriter().close();
            } else {
                //Render own profile
                resp.setContentType("text/html");
                resp.getWriter().write("<h1>Hello, <i>" + user.getName() + "</i></h1>" +
                        "<a href='/logout'>Log out</a>");
                resp.getWriter().write("<a href='/post'>Post something</a>");
                List<Post> posts = userService.getUserPosts(user);
                if (posts == null) {
                    resp.getWriter().write("You haven't posted anything yet");
                } else {
                    for (Post post : posts) {
                        resp.getWriter().write("<div>" + post.getText() + "</div>");
                    }
                }
                resp.getWriter().close();
            }
        }
    }
}
