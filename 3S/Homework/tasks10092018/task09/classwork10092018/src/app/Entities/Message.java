package app.Entities;

public class Message {

    private int id;
    private User sender;
    private User receiver;
    private String text;

    public Message(int id, User sender, User receiver, String text) {
        this.id = id;
        this.sender = sender;
        this.receiver = receiver;
        this.text = text;
    }

    public int getId() {
        return id;
    }

    public User getSender() {
        return sender;
    }

    public User getReceiver() {
        return receiver;
    }

    public String getText() {
        return text;
    }
}
