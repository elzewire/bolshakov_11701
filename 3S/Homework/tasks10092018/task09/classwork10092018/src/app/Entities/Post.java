package app.Entities;

public class Post {

    private int id;
    private User publisher;
    private String text;

    public Post(int id, User publisher, String text) {
        this.id = id;
        this.publisher = publisher;
        this.text = text;
    }

    public int getId() {
        return id;
    }

    public User getPublisher() {
        return publisher;
    }

    public String getText() {
        return text;
    }
}
