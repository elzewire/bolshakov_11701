package app;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (true) {
            String next = sc.next();
            if (next.equals("exit")) {
                break;
            } else {
                new Dispatcher(next);
            }
        }
    }
}
