package app.CollectionsSingleton;

import app.Entities.User;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class UsersCollectionSingleton {

    private static List<User> usersCollection;

    public static List<User> getUsersCollection() {
        if (usersCollection == null) {
            usersCollection = new ArrayList<>();
            try {
                BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream("res/db/users.txt")));
                String line = br.readLine();
                while (line != null) {
                    String[] attrs = line.split(",");
                    User user = new User(Integer.parseInt(attrs[0]), attrs[1]);
                    usersCollection.add(user);
                    line = br.readLine();
                }
            } catch (java.io.IOException e) {
                e.printStackTrace();
            }
        }
        return usersCollection;
    }
}
