package app.CollectionsSingleton;

import app.Entities.Post;
import app.Entities.User;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class PostsCollectionSingleton {

    private static List<Post> postsCollection;

    public static List<Post> getPostsCollection() {
        if (postsCollection == null) {
            postsCollection = new ArrayList<>();
            try {
                BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream("res/db/posts.txt")));
                String line = br.readLine();
                while (line != null) {
                    String[] attrs = line.split(",");
                    Post post = new Post(Integer.parseInt(attrs[0]), getUser(Integer.parseInt(attrs[1])), attrs[2]);
                    postsCollection.add(post);
                    line = br.readLine();
                }
            } catch (java.io.IOException e) {
                e.printStackTrace();
            }
        }
        return postsCollection;
    }

    private static User getUser(int id) {
        List<User> users = UsersCollectionSingleton.getUsersCollection();
        for (User user : users) {
            if (user.getId() == id) {
                return user;
            }
        }
        return null;
    }
}
