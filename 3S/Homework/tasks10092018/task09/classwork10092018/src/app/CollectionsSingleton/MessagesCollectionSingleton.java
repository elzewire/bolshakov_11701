package app.CollectionsSingleton;

import app.Entities.Message;
import app.Entities.User;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class MessagesCollectionSingleton {

    private static List<Message> messagesCollection;

    public static List<Message> getMessagesCollection() {
        if (messagesCollection == null) {
            messagesCollection = new ArrayList<>();
            try {
                BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream("res/db/messages.txt")));
                String line = br.readLine();
                while (line != null) {
                    String[] attrs = line.split(",");
                    Message msg = new Message(Integer.parseInt(attrs[0]), getUser(Integer.parseInt(attrs[1])),
                            getUser(Integer.parseInt(attrs[2])), attrs[3]);
                    messagesCollection.add(msg);
                    line = br.readLine();
                }
            } catch (java.io.IOException e) {
                e.printStackTrace();
            }
        }
        return messagesCollection;
    }

    private static User getUser(int id) {
        List<User> users = UsersCollectionSingleton.getUsersCollection();
        for (User user : users) {
            if (user.getId() == id) {
                return user;
            }
        }
        return null;
    }
}
