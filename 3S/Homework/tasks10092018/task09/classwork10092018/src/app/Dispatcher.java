package app;

import app.Entities.Message;
import app.Entities.Post;
import app.Entities.User;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Dispatcher implements Runnable {

    private String rawUri;
    private Thread thread;

    public Dispatcher(String rawUri) {
        this.rawUri = rawUri;
        thread = new Thread(this);
        thread.start();
    }

    public void get() {
        String regex = "(?<path>/[a-zA-Z]+)(?<id>[0-9]*)";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(rawUri);
        String uri = "";
        String id = "";
        if (matcher.matches()) {
            uri = matcher.group("path");
            id = matcher.group("id");
        }

        switch (uri) {
            case "/feed":
                feed();
                break;

            case "/messages":
                messages();
                break;

            case "/id":
                if (!id.equals("")) {
                 id(Integer.parseInt(id));
                }
                break;

            default:
                pageNotFound();
        }
    }

    private void pageNotFound() {
        try {
            FileWriter fw = new FileWriter("res/html/404.html");
            fw.write("<h1>404</h1>");
            fw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void id(int id) {
        try {
            FileWriter fw = new FileWriter("res/html/id" + id + ".html");
            fw.write("<h1>Profile</h1>");
            List<User> users = Database.getUsersCollection();
            for (User user : users) {
                if (user.getId() == id) {
                    fw.write("<h2>" + user.getName() + "</h2>");
                    fw.close();
                    break;
                }
            }
            fw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void messages() {
        try {
            FileWriter fw = new FileWriter("res/html/messages.html");
            fw.write("<h1>Feed</h1>");
            List<Message> messages = Database.getMessagesCollection();
            for (Message message : messages) {
                fw.write("<div>\n" +
                        "\t<h2>Message</h2>\n" +
                        "\t<h4>from: " + message.getSender() + " to: " + message.getReceiver() + "</h4>\n" +
                        "\t<p>" + message.getText() + "</p>\n" +
                        "</div>");
            }
            fw.close();
        } catch (java.io.IOException e) {
            e.printStackTrace();
        }
    }

    private void feed() {
        try {
            FileWriter fw = new FileWriter("res/html/feed.html");
            fw.write("<h1>Feed</h1>");
            List<Post> posts = Database.getPostsCollection();
            for (Post post : posts) {
                fw.write("<div>\n" +
                        "\t<h2>Post</h2>\n" +
                        "\t<h4>author: " + post.getPublisher() + "</h4>\n" +
                        "\t<p>" + post.getText() + "</p>\n" +
                        "</div>");
            }
            fw.close();
        } catch (java.io.IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        get();
    }
}
