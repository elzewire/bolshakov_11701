package app;

import app.CollectionsSingleton.MessagesCollectionSingleton;
import app.CollectionsSingleton.PostsCollectionSingleton;
import app.CollectionsSingleton.UsersCollectionSingleton;
import app.Entities.Message;
import app.Entities.Post;
import app.Entities.User;

import java.util.List;

public class Database {

    public static List<User> getUsersCollection() {
        return UsersCollectionSingleton.getUsersCollection();
    }

    public static List<Message> getMessagesCollection() {
        return MessagesCollectionSingleton.getMessagesCollection();
    }

    public static List<Post> getPostsCollection() {
        return PostsCollectionSingleton.getPostsCollection();
    }
}
