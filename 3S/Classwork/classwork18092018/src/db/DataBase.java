package db;

import db.collections.UsersCollectionSingleton;
import entities.User;

import java.util.List;

public class DataBase {

    public static List<User> getUsersCollection() {
        return UsersCollectionSingleton.getUsersCollection();
    }

}
