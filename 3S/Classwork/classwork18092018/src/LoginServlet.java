import entities.User;
import services.UserService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class LoginServlet extends HttpServlet {

    private UserService userService = new UserService();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (userService.getCurrentUser(req) != null) {
            resp.sendRedirect("/profile");
        } else {
            resp.setContentType("text/html");
            resp.getWriter().write("<form>\n" +
                    "\t<input type='text' name='username'>\n" +
                    "\t<input type='password' name='password'>\n" +
                    "\t<button type='submit'>Sign in</button>\n" +
                    "</form>");
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (userService.getCurrentUser(req) != null) {
            resp.sendRedirect("/profile");
        } else {
            User current_user = userService.authenticate(req);
            if (current_user != null) {
                userService.authorize(current_user, req);
            } else {
                resp.sendRedirect("'/login");
            }
        }
    }
}
