package dao;

import entities.User;

import java.util.List;

public interface UserDAO {

    public User getById(int id);
    public List<User> getByName(String name);
    public User getByUsername(String username);
}
