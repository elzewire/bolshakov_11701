package services;

import dao.UserDAO;
import entities.User;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class UserService {

    private UserDAO userDAO;

    public User getCurrentUser(HttpServletRequest req) {
        HttpSession session = req.getSession();
        return (User) session.getAttribute("current_user");
    }

    public User authenticate(HttpServletRequest req) {
        String username = req.getParameter("username");
        if (username != null) {
            User user = userDAO.getByUsername(username);
            if (user == null) {
                return null;
            }
            String password = req.getParameter("password");
            if (password.equals(user.getPassword())) {
                return user;
            } else {
                return null;
            }
        }
        return null;
    }

    public void authorize(User current_user, HttpServletRequest req) {
        HttpSession session = req.getSession();
        session.setAttribute("current_user", current_user);
    }
}