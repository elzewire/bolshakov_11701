CREATE VIEW ticket_view AS 
	SELECT t.id, t.user_id,
	t.event_id, e.name, e."datetime", e.description, e.image AS event_image, e.file, e.author_id
	FROM ticket AS t 
	INNER JOIN "event" AS e ON t.event_id = e.id ;

CREATE VIEW event_view AS
	SELECT 	e.id, e.name, e."datetime", e.description, e.image, e.file, e.author_id,
	u.username, u."password", u.token,
	p.first_name, p.last_name, p.email, p.is_moderator, p.image AS profile_image
	FROM "event" AS e
	INNER JOIN "user" AS u ON e.author_id = u.id
	INNER JOIN profile AS p ON e.author_id = p.user_id;

CREATE VIEW comment_view AS
	SELECT c.id, c.text, c."datetime", c.event_id,
	c.author_id AS commentator_id, u.username, u."password", u.token,
	p.first_name, p.last_name, p.email, p.is_moderator, p.image AS profile_image
	FROM "comment" AS c 
	INNER JOIN "user" AS u ON c.author_id = u.id
	INNER JOIN profile AS p ON c.author_id = p.user_id;

CREATE VIEW user_view AS
	SELECT u.id, u.username, u."password", u.token,
	p.first_name, p.last_name, p.email, p.is_moderator, p.image
	FROM "user" AS u
	INNER JOIN profile AS p ON p.user_id = u.id;