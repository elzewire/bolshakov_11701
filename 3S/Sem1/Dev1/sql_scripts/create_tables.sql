CREATE TABLE "user" (
	id SERIAL PRIMARY KEY,
	username varchar(20) UNIQUE NOT NULL,
	"password" varchar(20) NOT NULL,
	token text
);

CREATE TABLE profile (
	user_id integer PRIMARY KEY REFERENCES "user" (id) ON DELETE CASCADE,
	first_name varchar(20) NOT NULL,
	last_name varchar(20) NOT NULL,
	email varchar(20) NOT NULL,
	is_moderator boolean,
	image text
);

CREATE TABLE "event" (
	id SERIAL PRIMARY KEY,
	name text NOT NULL,
	"datetime" timestamp NOT NULL,
	description text NOT NULL,
	image text,
	file text,
	author_id integer REFERENCES "user" (id) ON DELETE CASCADE
);

CREATE TABLE comment (
	id SERIAL PRIMARY KEY,
	author_id integer REFERENCES "user" (id) ON DELETE CASCADE,
	event_id integer REFERENCES event (id) ON DELETE CASCADE,
	"datetime" timestamp NOT NULL,
	"text" text NOT NULL
);

CREATE TABLE ticket (
	id SERIAL PRIMARY KEY,
	user_id integer REFERENCES "user" (id) ON DELETE CASCADE,
	event_id integer REFERENCES event (id) ON DELETE CASCADE,
	expire_time timestamp NOT NULL
);
