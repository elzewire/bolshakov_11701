<#include "navbar.ftl">
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <link href="/static/css/style.css" rel="stylesheet">
        <title><@title></@title></title>
    </head>
    <body style="background: url(/static/img/background.jpg)">
        <@navbar></@navbar>
        <@main></@main>
    </body>
</html>