<#include "base.ftl">
<#macro title>
    Profile
</#macro>
<#macro main>
    <#if current_user??>
        <a href="/profile/edit"><button type="button">edit</button></a>
        <img src="${current_user.profile.image}">
        <h1>${current_user.profile.firstName} ${current_user.profile.lastName}</h1>
        <p>${current_user.profile.email}</p>
        <h1>Signed up to: </h1>
        <#if tickets??>
            <#list tickets as ticket>
            <h2><a href="/event/${ticket.eventId}">${ticket.event.name}</a></h2>
            <p>${ticket.event.datetime}</p>
            <p><a href="/ticket/${ticket.id}">view ticket</a></p>
            </#list>
        <#else>
            No events;
        </#if>
    </#if>

    <#if user??>
        <h1>${user.profile.firstName} ${user.profile.lastName}</h1>
        <p>${user.profile.email}</p>
    </#if>

    <h1>Events: </h1>
    <#if events??>
        <#list events as event>
        <h2><a href="/event/${event.id}">${event.name}</a></h2>
        </#list>
    <#else>
        No events yet;
    </#if>
</#macro>