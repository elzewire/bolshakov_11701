<#include "base.ftl">
<#macro title>
    Edit Profile
</#macro>
<#macro main>
    <form method="post" enctype="multipart/form-data">
        <p><input type="text" name="email" placeholder="email" value="${current_user.profile.email}"></p>
        <p><input type="text" name="first_name" placeholder="first_name" value="${current_user.profile.firstName}"></p>
        <p><input type="text" name="last_name" placeholder="last_name" value="${current_user.profile.lastName}"></p>
        <p><input type="file" name="image" placeholder="image"></p>
        <p><button type="submit">Save</button></p>
    </form>
</#macro>