<#include "base.ftl">
<#macro title>
    Log in
</#macro>
<#macro main>
    <div class="wrapper">
        <div class="form">
            <h1>Log in</h1>
            <form method="post">
                <p><input type="text" class="login-input" name="username" placeholder="Username"></p>
                <p><input type="password" class="login-input" name="password" placeholder="Password"></p>
                <p><label>
                    Remember me:
                    <input type="checkbox" name="remember">
                </label></p>
                <p>
                    <div class="form-bottom">
                        Don't have an account? <a href="/register">Sign up!</a>
                        <button type="submit" class="button-submit" name="submit">Log in</button>
                    </div>
                </p>
            </form>
        </div>
    </div>
</#macro>