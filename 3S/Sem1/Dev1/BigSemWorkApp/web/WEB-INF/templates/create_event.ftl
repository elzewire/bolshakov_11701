<#include "base.ftl">
<#macro title>
    Create Event
</#macro>
<#macro main>
    <div class="wrapper">
        <div class="post">
            <form method="post" enctype="multipart/form-data">
                <p><input type="text" class="login-input" name="name" placeholder="Name"></p>
                <p><input type="datetime-local" class="login-input" name="datetime" placeholder="Time"></p>
                <p><label>Image: </label><input type="file" name="image" placeholder="Image"></p>
                <p><label>Presentation: </label><input type="file" name="file" placeholder="File"></p>
                <p>
                    <textarea placeholder="Description" name="description"></textarea>
                    <button type="submit">Create</button>
                </p>
            </form>
        </div>
    </div>
</#macro>