<#include "base.ftl">
<#macro title>
    Register
</#macro>
<#macro main>
    <div class="wrapper">
        <div class="form">
            <h1>Sign up</h1>
            <form method="post" enctype="multipart/form-data">
                <p><input type="text" class="login-input" name="username" placeholder="Username"></p>
                <p><input type="password" class="login-input" name="password1" placeholder="Password"></p>
                <p><input type="password" class="login-input" name="password2" placeholder="Confirm password"></p>
                <p><input type="email" class="login-input" name="email" placeholder="E-mail"></p>
                <p><input type="text" class="login-input" name="firs_tname" placeholder="First name"></p>
                <p><input type="text" class="login-input" name="last_name" placeholder="Last name"></p>
                <p><label>Avatar: </label><input type="file" name="image" placeholder="image"></p>
                <p><label>
                    Remember me:
                    <input type="checkbox" name="remember">
                </label></p>
                <p>
                    <div class="form-bottom">
                        Already have an account? <a href="/login">Log in!</a>
                        <button type="submit" class="button-submit" name="submit">Sign up</button>
                    </div>
                </p>
            </form>
        </div>
    </div>
</#macro>