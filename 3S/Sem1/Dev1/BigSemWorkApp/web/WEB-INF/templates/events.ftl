<#include "base.ftl">
<#macro title>
    Events
</#macro>
<#macro main>
    <div class="wrapper">
        <#if user??>
            <#if user.profile.isModerator() == true>
                <a href="/event"><button type="button">CreateEvent</button></a>
            </#if>
        </#if>
        <#if isQuery??>
            Search Results:
        </#if>
        <#if events??>
            <#list events as event>
                <a href="/event/${event.id}">
                    <div class="post">
                        <#if event.image??>
                            <img src="${event.image}" height="80">
                        <#else>
                            <img src="/static/img/logo.png" height="80">
                        </#if>
                        <h3>${event.name}</h3>
                        <div class="timeplace">
                            ${event.datetime}<br>
                        </div>
                        <a href="/profile/${event.authorId}">${event.author.profile.firstName} ${event.author.profile.lastName}</a>
                    </div>
                </a>
            </#list>
        <#else>
            No events yet;
        </#if>
    </div>
</#macro>