<#include "base.ftl">
<#macro title>
    Edit Event
</#macro>
<#macro main>
    <div class="wrapper">
        <div class="post">
            <form method="post" enctype="multipart/form-data">
                <p><input type="text" class="login-input" name="name" placeholder="Name" value="${event.name}"></p>
                <p><input type="datetime-local" class="login-input" name="datetime" placeholder="Time" value="${event.datetime}"></p>
                <p><label>Image: </label><input type="file" name="image" placeholder="Image"></p>
                <p><label>Presentation: </label><input type="file" name="file" placeholder="File"></p>
                <p>
                    <textarea placeholder="Description" name="description">${event.description}</textarea>
                    <button type="submit">Save</button>
                </p>
            </form>
        </div>
    </div>
</#macro>