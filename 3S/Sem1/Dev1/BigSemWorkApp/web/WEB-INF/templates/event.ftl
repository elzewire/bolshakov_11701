<#include "base.ftl">
<#macro title>
    <#if event??>
        ${event.name}
    <#else>
        Event
    </#if>
</#macro>
<#macro main>
    <div class="wrapper">
        <#if user??>
            <#if user.profile.isModerator() == true>
                <a href="/event/${event.id}/edit"><button type="button">edit</button></a>
                <form method="post" action="/event/${event.id}/delete"><button type="submit">delete</button></form>
            </#if>
        </#if>
        <div class="post">
            <#if event.image??>
                <img src="${event.image}" height="80">
            <#else>
                <img src="/static/img/logo.png" height="80">
            </#if>
            <h2>${event.name}</h2>
            <div class="timeplace">
                ${event.datetime}
            </div>
            <p><a href="/profile/${event.authorId}">${event.author.profile.firstName} ${event.author.profile.lastName}</a></p>
            <p>${event.description}</p>
            <#if ticket??>
                <form method="post" action="/ticket/${ticket.id}"><button>
                    <input type="submit" value="Unsign">
                </button></form>
            <#else>
                <form method="post" action="/event/${event.id}/signup"><button>
                    <input type="submit" value="Sign up">
                </button></form>
            </#if>
            <form method="post" action="/event/${event.id}/comment">
                <p>Comment:<Br>
                <label>Write comment:
                        <textarea name="text" cols="40" rows="3"></textarea>
                </label></p>
                <p><input type="submit" value="Send">
            </form>
            <#if comments??>
                <#list comments as comment>
                    <div class="comment">
                        <a href="/profile/${comment.authorId}">${comment.author.profile.firstName} ${comment.author.profile.lastName}  :
                        <br>
                        ${comment.text}
                        <small>${comment.datetime}</small>
                    </div>
                </#list>
            </#if>
        </div>
    </div>
</#macro>