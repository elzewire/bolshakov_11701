<#include "base.ftl">
<#macro title>
    Ticket
</#macro>
<#macro main>
    <div class="ticket">
        <#if ticket.user.profile.image??>
            <img id="userPhoto" src="${ticket.user.profile.image}" height="80">
        <#else>
            <img id="userPhoto" src="/static/img/logo.png" height="80">
        </#if>
        <#if ticket.event.image??>
            <img id="eventPhoto" src="${ticket.event.image}" height="80">
        <#else>
            <img id="eventPhoto" src="/static/img/logo.png" height="80">
        </#if>
        <#if ticket??>
            <h2><a href="/event/${ticket.eventId}">${ticket.event.name}</a></h2>
            <h3>${ticket.user.profile.firstName} ${ticket.user.profile.lastName}</h3>
            <p>${ticket.expTime}</p>
        </#if>
    </div>
</#macro>