<html>
    <#include "navbar.ftl">
    <head>
        <title>Create Event</title>
    </head>
    <body>
    <form method="post">
        <p><input type="text" name="name" placeholder="name"></p>
        <p><input type="datetime-local" name="datetime" placeholder="event date"></p>
        <p><textarea name="description" placeholder="description"></textarea></p>
        <p><label>Image: </label><input type="file" name="image" placeholder="Image"></p>
        <p><label>Presentation: </label><input type="file" name="file" placeholder="File"></p>
        <button type="submit">Create</button>
    </form>
    </body>
</html>