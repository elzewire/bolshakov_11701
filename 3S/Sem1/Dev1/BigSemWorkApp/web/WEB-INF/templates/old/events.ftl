<html>
    <#include "navbar.ftl">
    <head>
        <title>Events</title>
    </head>
    <body>
        <#if user??>
            <#if user.profile.isModerator() == true>
                <a href="/event"><button type="button">CreateEvent</button></a>
            </#if>
        </#if>
        <#if isQuery??>
            Search Results:
        </#if>
        <#if events??>
            <#list events as event>
                <h2><a href="/event/${event.id}">${event.name}</a></h2>
                <p><a href="/profile/${event.authorId}">${event.author.profile.firstName} ${event.author.profile.lastName}</a></p>
            </#list>
        <#else>
            No events yet;
        </#if>
    </body>
</html>