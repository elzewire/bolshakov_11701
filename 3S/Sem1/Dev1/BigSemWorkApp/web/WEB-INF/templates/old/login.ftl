<html>
    <#include "navbar.ftl">
    <head>
        <title>Log In</title>
    </head>
    <body>
        <form method="post">
            <p><input type="text" name="username" placeholder="username"></p>
            <p><input type="password" name="password" placeholder="password"></p>
            <p><label>
                Remember me:
                <input type="checkbox" name="remember">
            </label></p>
            <button type="submit">Log In</button>
        </form>
    </body>
</html>