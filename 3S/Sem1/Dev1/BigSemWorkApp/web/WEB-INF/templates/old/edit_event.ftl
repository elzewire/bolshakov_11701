<html>
    <#include "navbar.ftl">
    <head>
        <title>Edit Event</title>
    </head>
    <body>
    <form method="post" enctype="multipart/form-data">
        <p><input type="text" name="name" placeholder="name" value="${event.name}"></p>
        <p><input type="datetime-local" name="datetime" placeholder="event date" value="${event.datetime}"></p>
        <p><textarea name="description" placeholder="description">${event.description}</textarea></p>
        <button type="submit">Save</button>
    </form>
    </body>
</html>