<html>
    <#include "navbar.ftl">
    <head>
        <title>Register</title>
    </head>
    <body>
        <form method="post" enctype="multipart/form-data">
            <p><input type="text" name="username" placeholder="username"></p>
            <p><input type="password" name="password1" placeholder="password"></p>
            <p><input type="password" name="password2" placeholder="confirm password"></p>
            <p><input type="text" name="email" placeholder="email"></p>
            <p><input type="text" name="first_name" placeholder="first_name"></p>
            <p><input type="text" name="last_name" placeholder="last_name"></p>
            <p><input type="file" name="image" placeholder="image"></p>
            <p><label>
                Remember me:
                <input type="checkbox" name="remember">
            </label></p>
            <p><button type="submit">Sign up</button></p>
        </form>
    </body>
</html>