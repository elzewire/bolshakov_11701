<#macro navbar>
    <form method="get" action="/search">
        <input type="text" name="q" placeholder="search">
        <button type="submit">Search</button>
    </form>
    <ul>
        <li><a href="/events">Events</a></li>
        <#if user??>
            <li><a href="/profile">My profile</a></li>
            <li><a href="/logout">Log Out</a></li>
        <#else>
            <li><a href="/login">Log In</a></li>
            <li><a href="/register">Register</a></li>
        </#if>
    </ul>
</#macro>
