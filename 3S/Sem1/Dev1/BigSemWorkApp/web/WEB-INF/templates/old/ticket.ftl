<html>
    <#include "navbar.ftl">
    <head>
        <title>Ticket</title>
    </head>
    <body>
        <#if ticket??>
            <h2><a href="/event/${ticket.eventId}">${ticket.event.name}</a></h2>
            <p>${ticket.event.datetime}</p>
            <p>${ticket.user.profile.firstName} ${ticket.profile.user.lastName}</p>
        </#if>
    </body>
</html>