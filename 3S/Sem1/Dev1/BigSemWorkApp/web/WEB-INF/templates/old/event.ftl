<html>
    <#include "navbar.ftl">
    <head>
        <title>Event</title>
    </head>
    <body>
        <#if user??>
            <#if user.profile.isModerator() == true>
                <a href="/event/${event.id}/edit"><button type="button">edit</button></a>
                <form method="post" action="/event/${event.id}/delete"><button type="submit">delete</button></form>
            </#if>
        </#if>
        <#if ticket??>
            <form method="post" action="/ticket/${ticket.id}"><button type="submit">unsign</button></form>
        <#else>
            <form method="post" action="/event/${event.id}/signup"><button type="submit">sign up</button></form>
        </#if>

        <h1>${event.name}</h1>
        <p><a href="/profile/${event.authorId}">${event.author.profile.firstName} ${event.author.profile.lastName}</a></p>
        <p>${event.description}</p>
        <p>${event.datetime}</p>
        <#if comments??>
            <#list comments as comment>
                <h3><a href="/profile/${comment.authorId}">${comment.author.profile.firstName} ${comment.author.profile.lastName}</a></h3>
                <p>${comment.text}</p>
            </#list>
        </#if>

        <form method="post" action="/event/${event.id}/comment">
            <p><textarea name="text" placeholder="comment"></textarea></p>
            <button type="submit">Comment</button>
        </form>
    </body>
</html>