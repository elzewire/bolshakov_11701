package helpers;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import freemarker.template.TemplateExceptionHandler;

import javax.servlet.Servlet;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

public class MainHelper {

    private static final String URL = "jdbc:postgresql://localhost:5433/semwork";
    private static final String USERNAME = "postgres";
    private static final String PASSWORD = "Postgres";
    private static final String STATIC_PATH = "/static/";
    private static final String UPLOAD_PATH = "/static/uploads/";
    private static final String SALT = "someKindOf_$417";
    private static final int REMEMBER_TIME = 432000;
    private static Connection conn;
    private static Configuration cfg;

    public static Connection getConn() {
        try {
            Class.forName("org.postgresql.Driver");
            conn = DriverManager.getConnection(
                    URL,
                    USERNAME,
                    PASSWORD
            );
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }

        return conn;
    }

    private static Configuration getConfig(ServletContext sc) {
        if (cfg == null) {
            cfg = new Configuration();
            cfg.setServletContextForTemplateLoading(sc, "WEB-INF/templates");
            cfg.setTemplateExceptionHandler(
                    TemplateExceptionHandler.HTML_DEBUG_HANDLER
            );
        }
        return cfg;
    }

    public static void render(ServletContext sc, HttpServletResponse resp, String path, Map<String, Object> root) {
        Configuration cfg = MainHelper.getConfig(sc);
        Template tmpl = null;
        try {
            tmpl = cfg.getTemplate(path);
            tmpl.process(root, resp.getWriter());
        } catch (TemplateException | IOException e) {
            e.printStackTrace();
        }
    }

    public static void render(ServletContext sc, HttpServletResponse resp, String path) {
        Configuration cfg = MainHelper.getConfig(sc);
        Template tmpl = null;
        try {
            tmpl = cfg.getTemplate(path);
            tmpl.process(new HashMap<String, Object>(), resp.getWriter());
        } catch (TemplateException | IOException e) {
            e.printStackTrace();
        }
    }

    public static String timestampToString(Timestamp datetime) {
        return new SimpleDateFormat("dd MM yyyy HH:mm").format(datetime);
    }

    public static String generateFName(String username, String fname) {
        return username + "/" + System.currentTimeMillis() + fname;
    }

    private static String getUploadPath() {
        return UPLOAD_PATH;
    }

    private static String getStaticPath() {
        return STATIC_PATH;
    }

    public static String saveFile(ServletContext sc, Part image, String folder) {
        if (image.getSize() != 0) {
            try {
                String filename = MainHelper.generateFName(folder, image.getSubmittedFileName());
                File dir = new File(sc.getRealPath(getUploadPath() + folder + "/"));
                if (!dir.exists()) {
                    dir.mkdirs();
                }
                InputStream is = image.getInputStream();
                FileOutputStream out = new FileOutputStream(new File(sc.getRealPath( getUploadPath() + filename)));
                byte[] buffer = new byte[4096];
                int data = is.read(buffer);
                while (data != -1) {
                    out.write(buffer, 0, data);
                    data = is.read(buffer);
                }
                is.close();
                out.close();
                return "/static/uploads/" + filename;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public static String salt(String string) {
        return string + SALT;
    }

    public static int getRememberTime() {
        return REMEMBER_TIME;
    }
}
