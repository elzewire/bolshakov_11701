package servlets;

import dao.entities.Event;
import dao.entities.User;
import helpers.MainHelper;
import services.EventService;
import services.UserService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EventsServlet extends HttpServlet {

    private EventService eventService = new EventService();
    private UserService userService = new UserService();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) {
        List<Event> events = eventService.getEvents();
        Map<String, Object> map = new HashMap<>();
        if (events != null) {
            map.put("events", events);
        }
        User user = userService.getCurrentUser(req);
        if (user != null) {
            map.put("user", user);
        }
        MainHelper.render(getServletContext(), resp, "events.ftl", map);
    }
}
