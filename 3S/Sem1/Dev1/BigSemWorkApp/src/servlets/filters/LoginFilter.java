package servlets.filters;

import dao.entities.User;
import services.UserService;

import javax.servlet.*;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

public class LoginFilter implements Filter {

    private UserService userService = new UserService();

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws IOException, ServletException {
        String path = ((HttpServletRequest)req).getServletPath();
        if (!path.equals("/register") && !path.equals("/login") && !path.equals("/logout")) {
            Cookie remember = userService.getRememberCookie(req);
            if (userService.getCurrentUser(req) != null) {
                chain.doFilter(req, resp);
            } else {
                if (remember != null) {
                    User current_user = userService.authenticate(remember.getValue());
                    if (current_user != null) {
                        userService.authorize(current_user, req);
                        chain.doFilter(req, resp);
                    } else {
                        RequestDispatcher rd = req.getRequestDispatcher("/login");
                        rd.forward(req, resp);
                    }
                } else {
                    RequestDispatcher rd = req.getRequestDispatcher("/login");
                    rd.forward(req, resp);
                }
            }
        } else {
            chain.doFilter(req, resp);
        }
    }

    @Override
    public void destroy() {

    }
}
