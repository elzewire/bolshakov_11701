package servlets;

import dao.entities.Comment;
import dao.entities.Event;
import dao.entities.Ticket;
import dao.entities.User;
import helpers.MainHelper;
import services.EventService;
import services.UserService;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@MultipartConfig
public class EventServlet extends HttpServlet{
    private EventService eventService = new EventService();
    private UserService userService = new UserService();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        if (eventService.isCreate(req.getPathInfo())) {
            if (userService.getCurrentUser(req) != null) {
                if (userService.isModerator(req)) {
                    MainHelper.render(getServletContext(), resp, "create_event.ftl");
                } else {
                    resp.sendError(403);
                }
            } else {
                resp.sendRedirect("/login");
            }
        } else {
            int id = eventService.getId(req.getPathInfo());
            Event event = eventService.getById(id);
            if (event != null) {
                if (eventService.isEdit(req.getPathInfo())) {
                    if (userService.getCurrentUser(req) != null) {
                        if (userService.isModerator(req) && userService.hasPermission(req, event.getAuthorId())) {
                            Map<String, Object> map = new HashMap<>();
                            map.put("event", event);
                            MainHelper.render(getServletContext(), resp, "edit_event.ftl", map);
                        } else {
                            resp.sendError(403);
                        }
                    } else {
                        resp.sendRedirect("/login");
                    }
                } else {
                    List<Comment> comments = eventService.getComments(event);
                    Map<String, Object> map = new HashMap<>();
                    if (req.getSession().getAttribute("current_user") != null) {
                        Ticket ticket = eventService.getUserTicket(event, (User)req.getSession().getAttribute("current_user"));
                        map.put("user", req.getSession().getAttribute("current_user"));
                        if (ticket != null) {
                            map.put("ticket", ticket);
                        }
                    }
                    map.put("event", event);
                    map.put("comments", comments);
                    MainHelper.render(getServletContext(), resp, "event.ftl", map);
                }
            } else {
                resp.sendError(404);
            }
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (userService.getCurrentUser(req) != null) {
            if (eventService.isCreate(req.getPathInfo())) {
                if (userService.isModerator(req)) {
                    eventService.create(getServletContext(), req, resp);
                    resp.sendRedirect("/events");
                } else {
                    resp.sendError(403);
                }
            } else {
                if (eventService.isEdit(req.getPathInfo())) {
                    if (userService.isModerator(req) &&
                            userService.hasPermission(req,
                                    eventService.getById(eventService.getId(req.getPathInfo())).getAuthorId())) {
                        eventService.update(getServletContext(), req, resp);
                        resp.sendRedirect("/event/" + eventService.getId(req.getPathInfo()));
                    } else {
                        resp.sendError(403);
                    }
                } else if (eventService.isDelete(req.getPathInfo())) {
                    if (userService.isModerator(req) &&
                            userService.hasPermission(req,
                                    eventService.getById(eventService.getId(req.getPathInfo())).getAuthorId())) {
                        eventService.delete(req, resp);
                        resp.sendRedirect("/events");
                    } else {
                        resp.sendError(403);
                    }
                } else if (eventService.isComment(req.getPathInfo())) {
                    eventService.comment(req, resp);
                    resp.sendRedirect("/event/" + eventService.getId(req.getPathInfo()));
                } else if (eventService.isSignup(req.getPathInfo())) {
                    eventService.signup(req, resp);
                    resp.sendRedirect("/event/" + eventService.getId(req.getPathInfo()));
                }
            }
        } else {
            resp.sendRedirect("/login");
        }
    }
}
