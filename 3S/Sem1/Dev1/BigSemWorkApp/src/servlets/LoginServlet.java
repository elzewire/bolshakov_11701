package servlets;

import dao.entities.User;
import helpers.MainHelper;
import services.UserService;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class LoginServlet extends HttpServlet {

    private UserService userService = new UserService();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Cookie remember = userService.getRememberCookie(req);
        if (userService.getCurrentUser(req) != null) {
            resp.sendRedirect("/events");
        } else {
            if (remember != null) {
                User current_user = userService.authenticate(remember.getValue());
                if (current_user != null) {
                    userService.authorize(current_user, req);
                    resp.sendRedirect("/events");
                } else {
                    MainHelper.render(getServletContext(), resp, "login.ftl");
                }
            } else {
                MainHelper.render(getServletContext(), resp, "login.ftl");
            }
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (userService.getCurrentUser(req) != null) {
            resp.sendRedirect("/events");
        } else {
            Cookie remember = userService.getRememberCookie(req);
            User current_user = null;
            current_user = userService.authenticate(req);
            if (remember != null && current_user == null) {
                current_user = userService.authenticate(remember.getValue());
            }
            if (current_user != null) {
                userService.remember(req, resp, current_user);
                userService.authorize(current_user, req);
                resp.sendRedirect("/events");
            } else {
                resp.sendRedirect("/login");
            }
        }
    }
}
