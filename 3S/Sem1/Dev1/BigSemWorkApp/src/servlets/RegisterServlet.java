package servlets;

import dao.entities.User;
import helpers.MainHelper;
import services.UserService;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@MultipartConfig
public class RegisterServlet extends HttpServlet {

    private UserService userService = new UserService();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Cookie remember = userService.getRememberCookie(req);
        if (userService.getCurrentUser(req) != null) {
            resp.sendRedirect("/events");
        } else {
            if (remember != null) {
                resp.sendRedirect("/login");
            } else {
                MainHelper.render(getServletContext(), resp, "register.ftl");
            }
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Cookie remember = userService.getRememberCookie(req);
        if (userService.getCurrentUser(req) != null) {
            resp.sendRedirect("/events");
        } else {
            if (remember != null) {
                resp.sendRedirect("/login");
            } else {
                if (userService.validate(req)) {
                    User current_user = userService.createUser(getServletContext(), req);
                    if (current_user != null) {
                        userService.remember(req, resp, current_user);
                        userService.authorize(current_user, req);
                        resp.sendRedirect("/events");
                    } else {
                        resp.sendRedirect("/register");
                    }
                } else {
                    resp.sendRedirect("/register");
                }
            }
        }
    }
}
