package servlets;

import dao.entities.Ticket;
import helpers.MainHelper;
import services.TicketService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class TicketServlet extends HttpServlet {
    private TicketService ticketService = new TicketService();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (ticketService.hasId(req.getPathInfo())) {
            int id = ticketService.getId(req.getPathInfo());
            Ticket ticket = ticketService.getById(id);
            Map<String, Object> map = new HashMap<>();
            if (ticket != null) {
                map.put("ticket", ticket);
            }
            MainHelper.render(getServletContext(), resp, "ticket.ftl", map);
        } else {
            resp.sendError(404);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (ticketService.hasId(req.getPathInfo())) {
            int id = ticketService.getId(req.getPathInfo());
            int eventId = ticketService.delete(id);
            resp.sendRedirect("/event/" + eventId);
        } else {
            resp.sendError(404);
        }
    }
}
