package servlets;

import dao.entities.Event;
import dao.entities.Ticket;
import dao.entities.User;
import helpers.MainHelper;
import services.ProfileService;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@MultipartConfig
public class ProfileServlet extends HttpServlet{

    private ProfileService profileService = new ProfileService();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Map<String, Object> map = new HashMap<>();
        if (profileService.hasId(req.getPathInfo())) {
            int id = profileService.getId(req.getPathInfo());
            User user = profileService.getById(id);
            if (user != null) {
                if (user.getId() == ((User)req.getSession().getAttribute("current_user")).getId()) {
                    resp.sendRedirect("/profile");
                } else {
                    List<Event> events = profileService.getUserEvents(user);
                    List<Ticket> tickets = profileService.getSignedUpTickets(user);
                    if (events != null) {
                        map.put("tickets", tickets);
                    }
                    if (tickets != null) {
                        map.put("events", events);
                    }
                    map.put("user", user);
                    MainHelper.render(getServletContext(), resp, "profile.ftl", map);
                }
            } else {
                resp.sendError(404);
            }
        } else {
            if (profileService.isEdit(req.getPathInfo())) {
                map.put("current_user", req.getSession().getAttribute("current_user"));
                MainHelper.render(getServletContext(), resp, "edit_profile.ftl", map);
            } else {
                List<Event> events = profileService.getUserEvents((User)req.getSession().getAttribute("current_user"));
                List<Ticket> tickets = profileService.getSignedUpTickets((User)req.getSession().getAttribute("current_user"));
                if (events != null) {
                    map.put("tickets", tickets);
                }
                if (tickets != null) {
                    map.put("events", events);
                }
                map.put("current_user", req.getSession().getAttribute("current_user"));
                MainHelper.render(getServletContext(), resp, "profile.ftl", map);
            }
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (profileService.isEdit(req.getPathInfo())) {
            if (profileService.validate(req)) {
                profileService.save(getServletContext(), req);
                resp.sendRedirect("/profile");
            } else {
                resp.sendRedirect("/profile/edit");
            }
        } else {
            resp.sendError(404);
        }
    }
}
