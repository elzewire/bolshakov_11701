package servlets;

import services.UserService;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class LogoutServlet extends HttpServlet {

    private UserService userService = new UserService();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (userService.getCurrentUser(req) != null) {
            req.getSession().invalidate();
            resp.addCookie(new Cookie("remember", ""));
            resp.sendRedirect("/login");
        } else {
            resp.sendRedirect("/login");
        }
    }
}
