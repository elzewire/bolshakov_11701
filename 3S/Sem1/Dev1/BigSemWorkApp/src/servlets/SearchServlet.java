package servlets;

import dao.entities.Event;
import helpers.MainHelper;
import services.SearchService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SearchServlet extends HttpServlet {
    private SearchService searchService = new SearchService();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Map<String, Object> map = new HashMap<>();
        List<Event> events = searchService.get(req.getParameter("q"));
        if (events != null) {
            map.put("events", events);
        }
        map.put("isQuery", true);
        MainHelper.render(getServletContext(), resp, "events.ftl", map);
    }
}
