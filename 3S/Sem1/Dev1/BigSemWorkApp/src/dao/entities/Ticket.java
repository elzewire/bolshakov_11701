package dao.entities;

import dao.impl.EventDAOjdbc;
import dao.impl.UserDAOjdbc;
import dao.interfaces.EventDAO;
import dao.interfaces.UserDAO;

import java.sql.Time;
import java.sql.Timestamp;

public class Ticket {

    private static String table = "\"ticket\"";

    private int id;

    private int userId;
    private User user;

    private int eventId;
    private Event event;

    private Timestamp expTime;
    private UserDAO userDAO = new UserDAOjdbc();
    private EventDAO eventDAO = new EventDAOjdbc();

    public Ticket(int id, int userId, int eventId, Timestamp expTime) {
        this.id = id;
        this.userId = userId;
        this.eventId = eventId;
        this.expTime = expTime;
    }

    public Ticket(int id, int userId, Event event, Timestamp expTime) {
        this.id = id;
        this.userId = userId;
        this.event = event;
        this.eventId = event.getId();
        this.expTime = expTime;
    }

    public Ticket(int id, User user, Event event, Timestamp expTime) {
        this.id = id;
        this.user = user;
        this.userId = user.getId();
        this.event = event;
        this.eventId = event.getId();
        this.expTime = expTime;
    }

    public int getId() {
        return id;
    }

    public User getUser() {
        if (user == null) {
            this.user = userDAO.get(userId);
        }
        return user;
    }

    public int getUserId() {
        return userId;
    }

    public Event getEvent() {
        if (event == null) {
            this.event = eventDAO.get(eventId);
        }
        return event;
    }

    public int getEventId() {
        return eventId;
    }

    public Timestamp getExpTime() {
        return expTime;
    }
}
