package dao.entities;

public class Profile {
    private int id;
    private String firstName;
    private String lastName;
    private String email;
    private boolean isModerator;
    private String image;

    public Profile(int id, String firstName, String lastName, String email, boolean isModerator, String image) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.isModerator = isModerator;
        this.image = image;
    }

    public Profile(int id, String firstName, String lastName, String email, boolean isModerator) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.isModerator = isModerator;
    }

    public int getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public boolean isModerator() {
        return isModerator;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public void setId(int id) {
        this.id = id;
    }
}
