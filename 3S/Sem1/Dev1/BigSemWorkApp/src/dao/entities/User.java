package dao.entities;

public class User {

    private int id;
    private String username;
    private String password;
    private String token;
    private Profile profile;

    public User(int id, String username, String password, String token, Profile profile) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.token = token;
        this.profile = profile;
    }

    public int getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getToken() {
        return token;
    }

    public Profile getProfile() {
        return profile;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
