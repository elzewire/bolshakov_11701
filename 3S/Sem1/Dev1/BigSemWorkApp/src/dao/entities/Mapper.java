package dao.entities;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class Mapper {

    public User mapUser(ResultSet rs) {
        try {
            rs.next();
            return new User(
                    rs.getInt("id"),
                    rs.getString("username"),
                    rs.getString("password"),
                    rs.getString("token"),
                    new Profile(
                            rs.getInt("id"),
                            rs.getString("first_name"),
                            rs.getString("last_name"),
                            rs.getString("email"),
                            rs.getBoolean("is_moderator"),
                            rs.getString("image")
                    )
            );
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public User mapUserWithoutProfile(ResultSet rs) {
        try {
            rs.next();
            return new User(
                    rs.getInt("id"),
                    rs.getString("username"),
                    rs.getString("password"),
                    rs.getString("token"),
                    null
            );
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<User> mapUsers(ResultSet rs) {
        List<User> users = new ArrayList<>();
        User u = mapUser(rs);
        while (u != null) {
            users.add(u);
            u = mapUser(rs);
        }
        return users;
    }

    public Event mapEvent(ResultSet rs) {
        try {
            rs.next();
            return new Event(
                    rs.getInt("id"),
                    rs.getString("name"),
                    rs.getTimestamp("datetime"),
                    rs.getString("description"),
                    rs.getString("image"),
                    rs.getString("file"),
                    rs.getInt("author_id")
            );
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<Event> mapEvents(ResultSet rs) {
        List<Event> events = new ArrayList<>();
        Event e = mapEvent(rs);
        while (e != null) {
            events.add(e);
            e = mapEvent(rs);
        }
        return events;
    }

    public Comment mapComment(ResultSet rs) {
        try {
            rs.next();
            return new Comment(
                    rs.getInt("id"),
                    new User (
                            rs.getInt("commentator_id"),
                            rs.getString("username"),
                            rs.getString("password"),
                            rs.getString("token"),
                            new Profile(
                                    rs.getInt("commentator_id"),
                                    rs.getString("first_name"),
                                    rs.getString("last_name"),
                                    rs.getString("email"),
                                    rs.getBoolean("is_moderator"),
                                    rs.getString("profile_image")
                            )
                    ),
                    rs.getInt("event_id"),
                    rs.getTimestamp("datetime"),
                    rs.getString("text")
            );
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<Comment> mapComments(ResultSet rs) {
        List<Comment> comments = new ArrayList<>();
        Comment c = mapComment(rs);
        while (c != null) {
            comments.add(c);
            c = mapComment(rs);
        }
        return comments;
    }

    public Ticket mapTicket(ResultSet rs) {
        try {
            rs.next();
            return new Ticket(
                    rs.getInt("id"),
                    rs.getInt("user_id"),
                    new Event(
                            rs.getInt("event_id"),
                            rs.getString("name"),
                            rs.getTimestamp("datetime"),
                            rs.getString("description"),
                            rs.getString("event_image"),
                            rs.getString("file"),
                            rs.getInt("author_id")
                    ),
                    rs.getTimestamp("datetime")
            );
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<Ticket> mapTickets(ResultSet rs) {
        List<Ticket> tickets = new ArrayList<>();
        Ticket t = mapTicket(rs);
        while (t != null) {
            tickets.add(t);
            t = mapTicket(rs);
        }
        return tickets;
    }

    public List<Event> mapEventsWithAuthors(ResultSet rs) {
        List<Event> events = new ArrayList<>();
        Event e = mapEventWithAuthor(rs);
        while (e != null) {
            events.add(e);
            e = mapEventWithAuthor(rs);
        }
        return events;
    }

    private Event mapEventWithAuthor(ResultSet rs) {
        try {
            rs.next();
            return new Event(
                    rs.getInt("id"),
                    rs.getString("name"),
                    rs.getTimestamp("datetime"),
                    rs.getString("description"),
                    rs.getString("image"),
                    rs.getString("file"),
                    new User(
                            rs.getInt("author_id"),
                            rs.getString("username"),
                            rs.getString("password"),
                            rs.getString("token"),
                            new Profile(
                                    rs.getInt("author_id"),
                                    rs.getString("first_name"),
                                    rs.getString("last_name"),
                                    rs.getString("email"),
                                    rs.getBoolean("is_moderator"),
                                    rs.getString("profile_image")
                            )
                    )
            );
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
