package dao.entities;

import dao.impl.UserDAOjdbc;
import dao.interfaces.UserDAO;

import java.sql.Timestamp;

public class Event{

    private int id;
    private String name;
    private Timestamp datetime;
    private String description;
    private String image;
    private String file;

    private int authorId;

    private User author;
    private UserDAO userDAO = new UserDAOjdbc();

    public Event(int id, String name, Timestamp datetime, String description, String image, String file, int authorId) {
        this.id = id;
        this.name = name;
        this.datetime = datetime;
        this.description = description;
        this.image = image;
        this.file = file;
        this.authorId = authorId;
    }

    public Event(int id, String name, Timestamp datetime, String description, String image, String file, User author) {
        this.id = id;
        this.name = name;
        this.datetime = datetime;
        this.description = description;
        this.image = image;
        this.file = file;
        this.author = author;
        this.authorId = author.getId();
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Timestamp getDatetime() {
        return datetime;
    }

    public String getDescription() {
        return description;
    }

    public String getImage() {
        return image;
    }

    public String getFile() {
        return file;
    }

    public User getAuthor() {
        if (author == null) {
            this.author = userDAO.get(authorId);
        }
        return author;
    }

    public int getAuthorId() {
        return authorId;
    }
}
