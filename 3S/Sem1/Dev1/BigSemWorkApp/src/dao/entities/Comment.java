package dao.entities;

import dao.impl.EventDAOjdbc;
import dao.impl.UserDAOjdbc;
import dao.interfaces.EventDAO;
import dao.interfaces.UserDAO;

import java.sql.Timestamp;

public class Comment {

    private static String table = "\"comment\"";

    private int id;

    private int authorId;
    private User author;

    private int eventId;
    private Event event;

    private Timestamp datetime;
    private String text;
    private UserDAO userDAO = new UserDAOjdbc();
    private EventDAO eventDAO = new EventDAOjdbc();

    public Comment(int id, int authorId, int eventId, Timestamp datetime, String text) {
        this.id = id;
        this.authorId = authorId;
        this.eventId = eventId;
        this.datetime = datetime;
        this.text = text;
    }

    public Comment(int id, User author, int eventId, Timestamp datetime, String text) {
        this.id = id;
        this.author = author;
        this.authorId = author.getId();
        this.eventId = eventId;
        this.datetime = datetime;
        this.text = text;
    }

    public Comment(int id, User author, Event event, Timestamp datetime, String text) {
        this.id = id;
        this.author = author;
        this.authorId = author.getId();
        this.event = event;
        this.eventId = event.getId();
        this.datetime = datetime;
        this.text = text;
    }

    public int getId() {
        return id;
    }

    public User getAuthor() {
        if (author == null) {
            this.author = userDAO.get(authorId);
        }
        return author;
    }

    public int getAuthorId() {
        return authorId;
    }

    public Event getEvent() {
        if (event == null) {
            this.event = eventDAO.get(eventId);
        }
        return event;
    }

    public int getEventId() {
        return eventId;
    }

    public Timestamp getDatetime() {
        return datetime;
    }

    public String getText() {
        return text;
    }
}
