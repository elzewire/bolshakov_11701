package dao.impl;

import dao.entities.Profile;
import dao.interfaces.ProfileDAO;
import helpers.MainHelper;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

public class ProfileDAOjdbc implements ProfileDAO {
    @Override
    public Profile get(int id) {
        return null;
    }

    @Override
    public List<Profile> getAll() {
        return null;
    }

    @Override
    public void create(Profile entity) {
        Connection conn = MainHelper.getConn();
        try {
            PreparedStatement ps = conn.prepareStatement(
                    "INSERT INTO profile (user_id, first_name, last_name, email, is_moderator, image) VALUES (?, ?, ?, ?, ?, ?);"
            );
            ps.setInt(1, entity.getId());
            ps.setString(2, entity.getFirstName());
            ps.setString(3, entity.getLastName());
            ps.setString(4, entity.getEmail());
            ps.setBoolean(5, entity.isModerator());
            ps.setString(6, entity.getImage());
            ps.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void update(Profile entity) {
        Connection conn = MainHelper.getConn();
        try {
            PreparedStatement ps = conn.prepareStatement(
                    "UPDATE profile SET first_name = ?, last_name = ?, email = ?, is_moderator = ?, image = ? WHERE user_id = ?;"
            );

            ps.setString(1, entity.getFirstName());
            ps.setString(2, entity.getLastName());
            ps.setString(3, entity.getEmail());
            ps.setBoolean(4, entity.isModerator());
            ps.setString(5, entity.getImage());

            ps.setInt(6, entity.getId());
            ps.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void delete(Profile entity) {
        Connection conn = MainHelper.getConn();
        try {
            PreparedStatement ps = conn.prepareStatement(
                    "DELETE FROM profile WHERE user_id = ?;"
            );
            ps.setInt(1, entity.getId());
            ps.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
