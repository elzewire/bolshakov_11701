package dao.impl;

import dao.entities.Event;
import dao.entities.Mapper;
import dao.entities.Ticket;
import dao.entities.User;
import dao.interfaces.TicketDAO;
import helpers.MainHelper;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class TicketDAOjdbc implements TicketDAO {

    private Mapper mapper = new Mapper();

    @Override
    public Ticket getByEventAndUser(Event event, User user) {
        Connection conn = MainHelper.getConn();
        try {
            PreparedStatement ps = conn.prepareStatement(
                    "SELECT * FROM ticket_view WHERE user_id = ? AND event_id = ?;"
            );
            ps.setInt(1, user.getId());
            ps.setInt(2, event.getId());
            ResultSet rs = ps.executeQuery();
            return mapper.mapTicket(rs);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<Ticket> getByUser(User user) {
        Connection conn = MainHelper.getConn();
        try {
            PreparedStatement ps = conn.prepareStatement(
                    "SELECT * FROM ticket_view WHERE user_id = ?;"
            );
            ps.setInt(1, user.getId());
            ResultSet rs = ps.executeQuery();
            return mapper.mapTickets(rs);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Ticket get(int id) {
        Connection conn = MainHelper.getConn();
        try {
            PreparedStatement ps = conn.prepareStatement(
                    "SELECT * FROM ticket_view WHERE id = ?;"
            );
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            return mapper.mapTicket(rs);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<Ticket> getAll() {
        Connection conn = MainHelper.getConn();
        try {
            PreparedStatement ps = conn.prepareStatement(
                    "SELECT * FROM ticket_view;"
            );
            ResultSet rs = ps.executeQuery();
            return mapper.mapTickets(rs);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void create(Ticket entity) {
        Connection conn = MainHelper.getConn();
        try {
            PreparedStatement ps = conn.prepareStatement(
                    "INSERT INTO ticket (user_id, event_id, expire_time) VALUES (?, ?, ?);"
            );
            ps.setInt(1, entity.getUserId());
            ps.setInt(2, entity.getEventId());
            ps.setTimestamp(3, entity.getExpTime());
            ps.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void update(Ticket entity) {
        Connection conn = MainHelper.getConn();
        try {
            PreparedStatement ps = conn.prepareStatement(
                    "UPDATE ticket SET user_id = ?, event_id = ?, expire_time = ? WHERE id = ?;"
            );
            ps.setInt(1, entity.getUserId());
            ps.setInt(2, entity.getEventId());
            ps.setTimestamp(3, entity.getExpTime());
            ps.setInt(4, entity.getId());
            ps.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void delete(Ticket entity) {
        Connection conn = MainHelper.getConn();
        try {
            PreparedStatement ps = conn.prepareStatement(
                    "DELETE FROM ticket WHERE id = ?;"
            );
            ps.setInt(1, entity.getId());
            ps.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
