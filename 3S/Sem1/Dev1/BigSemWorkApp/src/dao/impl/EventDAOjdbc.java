package dao.impl;

import dao.entities.Event;
import dao.entities.Mapper;
import dao.entities.User;
import dao.interfaces.EventDAO;
import helpers.MainHelper;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class EventDAOjdbc implements EventDAO {

    private Mapper mapper = new Mapper();

    @Override
    public List<Event> getByAuthor(User author) {
        Connection conn = MainHelper.getConn();
        try {
            PreparedStatement ps = conn.prepareStatement(
                    "SELECT * FROM event_view WHERE author_id = ?;"
            );
            ps.setInt(1, author.getId());
            ResultSet rs = ps.executeQuery();
            return mapper.mapEventsWithAuthors(rs);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<Event> getByKey(String q) {
        Connection conn = MainHelper.getConn();
        try {
            PreparedStatement ps = conn.prepareStatement(
                    "SELECT \"event\".id, name, datetime, description, author_id, \"event\".image, file FROM \"event\"" +
                            " JOIN \"user\" ON \"event\".author_id = \"user\".id" +
                            " JOIN profile ON \"event\".author_id = profile.user_id" +
                            " WHERE name ILIKE ?" +
                            " OR description ILIKE ?" +
                            " OR datetime::TEXT ILIKE ?" +
                            " OR profile.first_name ILIKE ?" +
                            " OR profile.last_name ILIKE ?;"
            );
            ps.setString(1,  "%" + q + "%");
            ps.setString(2,  "%" + q + "%");
            ps.setString(3,  "%" + q + "%");
            ps.setString(4,  "%" + q + "%");
            ps.setString(5,  "%" + q + "%");
            ResultSet rs = ps.executeQuery();
            return mapper.mapEvents(rs);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Event get(int id) {
        Connection conn = MainHelper.getConn();
        try {
            PreparedStatement ps = conn.prepareStatement(
                    "SELECT * FROM \"event\" WHERE id = ?;"
            );
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            return mapper.mapEvent(rs);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<Event> getAll() {
        Connection conn = MainHelper.getConn();
        try {
            PreparedStatement ps = conn.prepareStatement(
                    "SELECT * FROM event_view;"
            );
            ResultSet rs = ps.executeQuery();
            return mapper.mapEventsWithAuthors(rs);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void create(Event entity) {
        Connection conn = MainHelper.getConn();
        try {
            PreparedStatement ps = conn.prepareStatement(
                    "INSERT INTO event (\"name\", \"datetime\", description, image, file, author_id) VALUES (?, ?, ?, ?, ?, ?);"
            );
            ps.setString(1, entity.getName());
            ps.setTimestamp(2, entity.getDatetime());
            ps.setString(3, entity.getDescription());
            ps.setString(4, entity.getImage());
            ps.setString(5, entity.getFile());
            ps.setInt(6, entity.getAuthorId());
            ps.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void update(Event entity) {
        Connection conn = MainHelper.getConn();
        try {
            PreparedStatement ps = conn.prepareStatement(
                    "UPDATE \"event\" SET \"name\" = ?, description = ?, \"datetime\" = ?, image = ?, file = ?, author_id = ? WHERE id = ?;"
            );
            ps.setString(1, entity.getName());
            ps.setString(2, entity.getDescription());
            ps.setTimestamp(3, entity.getDatetime());
            ps.setString(4, entity.getImage());
            ps.setString(5, entity.getFile());
            ps.setInt(6, entity.getAuthorId());
            ps.setInt(7, entity.getId());
            ps.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void delete(Event entity) {
        Connection conn = MainHelper.getConn();
        try {
            PreparedStatement ps = conn.prepareStatement(
                    "DELETE FROM \"event\" WHERE id = ?;"
            );
            ps.setInt(1, entity.getId());
            ps.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
