package dao.impl;

import dao.entities.Mapper;
import dao.entities.User;
import dao.interfaces.UserDAO;
import helpers.MainHelper;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class UserDAOjdbc implements UserDAO{

    private Mapper mapper = new Mapper();

    @Override
    public User getByUsername(String username) {
        Connection conn = MainHelper.getConn();
        try {
            PreparedStatement ps = conn.prepareStatement(
                    "SELECT * FROM user_view WHERE username = ?;"
            );
            ps.setString(1, username);
            ResultSet rs = ps.executeQuery();
            return mapper.mapUser(rs);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public User getByToken(String token) {
        Connection conn = MainHelper.getConn();
        try {
            PreparedStatement ps = conn.prepareStatement(
                    "SELECT * FROM user_view WHERE token = ?;"
            );
            ps.setString(1, token);
            ResultSet rs = ps.executeQuery();
            return mapper.mapUser(rs);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public User getByUsernameWithoutProfile(String username) {
        Connection conn = MainHelper.getConn();
        try {
            PreparedStatement ps = conn.prepareStatement(
                    "SELECT * FROM \"user\" WHERE username = ?;"
            );
            ps.setString(1, username);
            ResultSet rs = ps.executeQuery();
            return mapper.mapUserWithoutProfile(rs);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public User get(int id) {
        Connection conn = MainHelper.getConn();
        try {
            PreparedStatement ps = conn.prepareStatement(
                    "SELECT * FROM user_view WHERE id = ?;"
            );
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            return mapper.mapUser(rs);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<User> getAll() {
        Connection conn = MainHelper.getConn();
        try {
            PreparedStatement ps = conn.prepareStatement(
                    "SELECT * FROM user_view;"
            );
            ResultSet rs = ps.executeQuery();
            return mapper.mapUsers(rs);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void create(User entity) {
        Connection conn = MainHelper.getConn();
        try {
            PreparedStatement ps = conn.prepareStatement(
                    "INSERT INTO \"user\" (username, password,  token) VALUES (?, ?, ?);"
            );
            ps.setString(1, entity.getUsername());
            ps.setString(2, entity.getPassword());
            ps.setString(3, entity.getToken());
            ps.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void update(User entity) {
        Connection conn = MainHelper.getConn();
        try {
            PreparedStatement ps = conn.prepareStatement(
                    "UPDATE \"user\" SET username = ?, password = ?, token = ? WHERE id = ?;"
            );
            ps.setString(1, entity.getUsername());
            ps.setString(2, entity.getPassword());
            ps.setString(3, entity.getToken());
            ps.setInt(4, entity.getId());
            ps.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void delete(User entity) {
        Connection conn = MainHelper.getConn();
        try {
            PreparedStatement ps = conn.prepareStatement(
                    "DELETE FROM \"user\" WHERE id = ?"
            );
            ps.setInt(1, entity.getId());
            ps.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}