package dao.impl;

import dao.entities.Comment;
import dao.entities.Event;
import dao.entities.Mapper;
import dao.interfaces.CommentDAO;
import helpers.MainHelper;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class CommentDAOjdbc implements CommentDAO {

    private Mapper mapper = new Mapper();

    @Override
    public List<Comment> getByEvent(Event event) {
        Connection conn = MainHelper.getConn();
        try {
            PreparedStatement ps = conn.prepareStatement(
                    "SELECT * FROM comment_view WHERE event_id = ?;"
            );
            ps.setInt(1, event.getId());
            ResultSet rs = ps.executeQuery();
            return mapper.mapComments(rs);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Comment get(int id) {
        Connection conn = MainHelper.getConn();
        try {
            PreparedStatement ps = conn.prepareStatement(
                    "SELECT * FROM comment WHERE event_id = ?;"
            );
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            return mapper.mapComment(rs);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<Comment> getAll() {
        Connection conn = MainHelper.getConn();
        try {
            PreparedStatement ps = conn.prepareStatement(
                    "SELECT * FROM comment;"
            );
            ResultSet rs = ps.executeQuery();
            return mapper.mapComments(rs);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void create(Comment entity) {
        Connection conn = MainHelper.getConn();
        try {
            PreparedStatement ps = conn.prepareStatement(
                    "INSERT INTO comment (author_id, event_id, \"datetime\", \"text\") VALUES (?, ?, ?, ?);"
            );
            ps.setInt(1, entity.getAuthorId());
            ps.setInt(2, entity.getEventId());
            ps.setTimestamp(3, entity.getDatetime());
            ps.setString(4, entity.getText());
            ps.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void update(Comment entity) {
        Connection conn = MainHelper.getConn();
        try {
            PreparedStatement ps = conn.prepareStatement(
                    "UPDATE comment SET author_id = ?, event_id = ?, \"datetime\" = ?, \"text\" = ? WHERE id = ?;"
            );
            ps.setInt(1, entity.getAuthorId());
            ps.setInt(2, entity.getEventId());
            ps.setTimestamp(3, entity.getDatetime());
            ps.setString(4, entity.getText());
            ps.setInt(5, entity.getId());
            ps.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void delete(Comment entity) {
        Connection conn = MainHelper.getConn();
        try {
            PreparedStatement ps = conn.prepareStatement(
                    "DELETE FROM comment WHERE id = ?;"
            );
            ps.setInt(1, entity.getId());
            ps.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
