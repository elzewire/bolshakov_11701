package dao.interfaces;

import java.util.List;

public interface DAO<T> {
    T get(int id);
    List<T> getAll();
    void create(T entity);
    void update(T entity);
    void delete(T entity);
}
