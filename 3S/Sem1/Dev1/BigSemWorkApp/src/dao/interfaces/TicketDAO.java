package dao.interfaces;

import dao.entities.Event;
import dao.entities.Ticket;
import dao.entities.User;

import java.util.List;

public interface TicketDAO extends DAO<Ticket> {
    Ticket getByEventAndUser(Event event, User user);
    List<Ticket> getByUser(User user);
}
