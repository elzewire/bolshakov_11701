package dao.interfaces;

import dao.entities.User;

public interface UserDAO extends DAO<User> {
    User getByUsername(String username);
    User getByToken(String token);
    User getByUsernameWithoutProfile(String username);
}
