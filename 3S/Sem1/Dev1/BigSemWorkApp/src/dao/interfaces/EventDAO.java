package dao.interfaces;

import dao.entities.Event;
import dao.entities.User;

import java.util.List;

public interface EventDAO extends DAO<Event> {
    List<Event> getByAuthor(User author);
    List<Event> getByKey(String q);
}
