package dao.interfaces;

import dao.entities.Comment;
import dao.entities.Event;

import java.util.List;

public interface CommentDAO extends DAO<Comment> {
    List<Comment> getByEvent(Event event);
}
