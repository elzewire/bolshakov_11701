package services;

import dao.entities.Event;
import dao.entities.Profile;
import dao.entities.Ticket;
import dao.entities.User;
import dao.impl.EventDAOjdbc;
import dao.impl.ProfileDAOjdbc;
import dao.impl.TicketDAOjdbc;
import dao.impl.UserDAOjdbc;
import dao.interfaces.EventDAO;
import dao.interfaces.ProfileDAO;
import dao.interfaces.TicketDAO;
import dao.interfaces.UserDAO;
import helpers.MainHelper;
import services.forms.Field;
import services.forms.Form;
import sun.applet.Main;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

public class ProfileService {
    private UserDAO userDAO = new UserDAOjdbc();
    private EventDAO eventDAO = new EventDAOjdbc();
    private TicketDAO ticketDAO = new TicketDAOjdbc();
    private ProfileDAO profileDAO = new ProfileDAOjdbc();

    public int getId(String path) {
        // Pattern p = Pattern.compile("/(?<id>[0-9]+)(/[a-z]*)?");
        //return Integer.parseInt(p.matcher(path).group("id"));
        return Integer.parseInt(path.split("/")[1]);
    }

    public boolean hasId(String path) {
        Pattern p = Pattern.compile("/([0-9]*)");
        return path != null && p.matcher(path).matches();
    }

    public User getById(int id) {
        return userDAO.get(id);
    }

    public boolean isEdit(String path) {
        return path != null && path.equals("/edit");
    }

    public void save(ServletContext sc, HttpServletRequest req) {
        User current_user = (User)req.getSession().getAttribute("current_user");
        String image = null;
        try {
            image = MainHelper.saveFile(sc, req.getPart("image"), current_user.getUsername());
        } catch (IOException | ServletException e) {
            e.printStackTrace();
        }
        Profile profile = new Profile(
                current_user.getId(),
                req.getParameter("first_name"),
                req.getParameter("last_name"),
                req.getParameter("email"),
                current_user.getProfile().isModerator()
        );
        if (image != null) {
            profile.setImage(image);
        }
        User newUser = new User(
            current_user.getId(),
            current_user.getUsername(),
            current_user.getPassword(),
            current_user.getToken(),
            profile
        );
        userDAO.update(newUser);
        profileDAO.update(profile);
        req.getSession().setAttribute("current_user", newUser);
    }

    public boolean validate(HttpServletRequest req) {
        Map<String, String[]> params = req.getParameterMap();
        List<Field> fields = new ArrayList<>();
        for (String key : params.keySet()) {
            fields.add(new Field(req.getParameter(key), key));
        }
        return new Form(fields).validate();
    }

    public List<Event> getUserEvents(User user) {
        return eventDAO.getByAuthor(user);
    }

    public List<Ticket> getSignedUpTickets(User user) {
        return ticketDAO.getByUser(user);
    }
}
