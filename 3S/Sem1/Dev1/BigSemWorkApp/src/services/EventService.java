package services;

import dao.entities.Comment;
import dao.entities.Event;
import dao.entities.Ticket;
import dao.entities.User;
import dao.impl.CommentDAOjdbc;
import dao.impl.EventDAOjdbc;
import dao.impl.TicketDAOjdbc;
import dao.interfaces.CommentDAO;
import dao.interfaces.EventDAO;
import dao.interfaces.TicketDAO;
import helpers.MainHelper;
import services.forms.Field;
import services.forms.Form;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

public class EventService {

    private EventDAO eventDAO = new EventDAOjdbc();
    private CommentDAO commentDAO = new CommentDAOjdbc();
    private TicketDAO ticketDAO = new TicketDAOjdbc();

    public List<Event> getEvents() {
        return eventDAO.getAll();
    }

    public Event getById(int id) {
        return eventDAO.get(id);
    }

    public boolean isCreate(String path) {
        return path == null;
    }

    public boolean isEdit(String path) {
        Pattern p = Pattern.compile("/([0-9]+)/edit");
        return p.matcher(path).matches();
    }

    public boolean isDelete(String path) {
        Pattern p = Pattern.compile("/([0-9]+)/delete");
        return p.matcher(path).matches();
    }

    public int getId(String path) {
        // Pattern p = Pattern.compile("/(?<id>[0-9]+)(/[a-z]*)?");
        //return Integer.parseInt(p.matcher(path).group("id"));
        return Integer.parseInt(path.split("/")[1]);
    }

    public void update(ServletContext sc, HttpServletRequest req, HttpServletResponse resp) {
        int id = getId(req.getPathInfo());
        Event event = eventDAO.get(id);
        if (event != null) {
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
            Timestamp ts = new Timestamp(System.currentTimeMillis());
            try {
                Date parsedDate = df.parse(req.getParameter("datetime"));
                ts = new Timestamp(parsedDate.getTime());
            } catch (ParseException e) {
                e.printStackTrace();
            }

            //Image and file handling
            String image = null;
            String file = null;
            try {
                image = MainHelper.saveFile(sc, req.getPart("image"), "event" + event.getName());
                file = MainHelper.saveFile(sc, req.getPart("file"), "event" + event.getName());
            } catch (IOException | ServletException e) {
                e.printStackTrace();
            }
            Event upd = new Event(
                    event.getId(),
                    req.getParameter("name"),
                    ts,
                    req.getParameter("description"),
                    image,
                    file,
                    event.getAuthorId()
            );
            eventDAO.update(upd);
        } else {
            try {
                resp.sendError(404);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void delete(HttpServletRequest req, HttpServletResponse resp) {
        int id = getId(req.getPathInfo());
        Event event = eventDAO.get(id);
        if (event != null) {
            eventDAO.delete(event);
        } else {
            try {
                resp.sendError(404);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public boolean isComment(String path) {
        Pattern p = Pattern.compile("/(?<id>[0-9]+)/comment");
        return p.matcher(path).matches();
    }

    public boolean isSignup(String path) {
        Pattern p = Pattern.compile("/(?<id>[0-9]+)/signup");
        return p.matcher(path).matches();
    }

    public void comment(HttpServletRequest req, HttpServletResponse resp) {
        int id = getId(req.getPathInfo());
        Event event = eventDAO.get(id);
        if (event != null) {
            Comment comment = new Comment(
                    0,
                    ((User) req.getSession().getAttribute("current_user")).getId(),
                    event.getId(),
                    new Timestamp(System.currentTimeMillis()),
                    req.getParameter("text")
            );
            commentDAO.create(comment);
        } else {
            try {
                resp.sendError(404);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void signup(HttpServletRequest req, HttpServletResponse resp) {
        int id = getId(req.getPathInfo());
        Event event = eventDAO.get(id);
        if (event != null) {
            if (ticketDAO.getByEventAndUser(event, (User)req.getSession().getAttribute("current_user")) == null) {
                Ticket ticket = new Ticket(
                        0,
                        ((User) req.getSession().getAttribute("current_user")).getId(),
                        event.getId(),
                        event.getDatetime()
                );
                ticketDAO.create(ticket);
            } else {
                try {
                    resp.sendError(403);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } else {
            try {
                resp.sendError(404);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public boolean validate(HttpServletRequest req) {
        Map<String, String[]> params = req.getParameterMap();
        List<Field> fields = new ArrayList<>();
        for (String key : params.keySet()) {
            if (!key.equals("datetime") && !key.equals("anonymous") && !key.equals("text")) {
                fields.add(new Field(req.getParameter(key), key));
            }
        }
        return new Form(fields).validate();
    }

    public List<Comment> getComments(Event event) {
        return commentDAO.getByEvent(event);
    }

    public void create(ServletContext sc, HttpServletRequest req, HttpServletResponse resp) {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
        Timestamp ts = new Timestamp(System.currentTimeMillis());
        if (req.getParameter("datetime") != null) {
            try {
                Date parsedDate = df.parse(req.getParameter("datetime"));
                ts = new Timestamp(parsedDate.getTime());
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        //Image and file handling
        String image = null;
        String file = null;
        try {
            image = MainHelper.saveFile(sc, req.getPart("image"), "event/" + req.getParameter("name"));
            file = MainHelper.saveFile(sc, req.getPart("file"), "event/" + req.getParameter("name"));
        } catch (IOException | ServletException e) {
            e.printStackTrace();
        }
        System.out.println(req.getParameter("name"));
        System.out.println(req.getParameter("description"));

        Event event = new Event(
                0,
                req.getParameter("name"),
                ts,
                req.getParameter("description"),
                image,
                file,
                ((User) req.getSession().getAttribute("current_user")).getId()
        );
        eventDAO.create(event);
    }

    public Ticket getUserTicket(Event event, User user) {
        return ticketDAO.getByEventAndUser(event, user);
    }
}
