package services.regex;

import java.util.HashMap;
import java.util.Map;

public class Regex {

    private Map<String, String> regexMap;
    private String textRegex = "([a-zA-Z0-9_\\-]+)";
    private String emailRegex = "([a-zA-Z_\\-]+)@([a-zA-Z_\\-]+)\\.([a-z]+)";

    public Regex() {
        this.regexMap = new HashMap<>();
        regexMap.put("password1", textRegex);
        regexMap.put("password2", textRegex);
        regexMap.put("username", textRegex);
        regexMap.put("email", emailRegex);
        regexMap.put("first_name", textRegex);
        regexMap.put("last_name", textRegex);
        regexMap.put("name", textRegex);
    }

    public String getRegex(String type) {
        String re = regexMap.get(type);
        if (re != null)
            return re;
        return textRegex;
    }
}
