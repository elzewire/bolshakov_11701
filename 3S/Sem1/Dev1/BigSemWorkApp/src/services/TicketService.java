package services;

import dao.entities.Ticket;
import dao.impl.TicketDAOjdbc;
import dao.interfaces.TicketDAO;

import java.util.regex.Pattern;

public class TicketService {
    private TicketDAO ticketDAO = new TicketDAOjdbc();

    public int getId(String path) {
        // Pattern p = Pattern.compile("/(?<id>[0-9]+)(/[a-z]*)?");
        //return Integer.parseInt(p.matcher(path).group("id"));
        return Integer.parseInt(path.split("/")[1]);
    }

    public boolean hasId(String path) {
        Pattern p = Pattern.compile("/([0-9]*)");
        return path != null && p.matcher(path).matches();
    }

    public int delete(int id) {
        Ticket ticket = ticketDAO.get(id);
        ticketDAO.delete(ticket);
        return ticket.getEventId();
    }

    public Ticket getById(int id) {
        return ticketDAO.get(id);
    }
}
