package services.forms;

import services.regex.Regex;

import java.util.regex.Pattern;

public class Field {

    private String input;
    private String regex;

    public Field(String input, String type) {
        this.input = input;
        this.regex = new Regex().getRegex(type);
    }

    public boolean validate() {
        Pattern p = Pattern.compile(regex);
        return p.matcher(input).matches();
    }
}
