package services.forms;

import java.util.List;

public class Form {
    private List<Field> fields;

    public Form(List<Field> fields) {
        this.fields = fields;
    }

    public boolean validate() {
        for (Field field : fields) {
            if (!field.validate()) {
                return false;
            }
        }
        return true;
    }
}
