package services;

import dao.entities.Event;
import dao.impl.EventDAOjdbc;
import dao.interfaces.EventDAO;

import java.util.List;

public class SearchService {
    private EventDAO eventDAO = new EventDAOjdbc();

    public List<Event> get(String q) {
        /*String[] query = q.split(" ");
        for (String keyword : query) {
            list.addAll(eventDAO.getByKey(keyword));
        }*/
        return eventDAO.getByKey(q);
    }
}
