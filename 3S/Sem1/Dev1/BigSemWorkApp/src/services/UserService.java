package services;

import dao.entities.Profile;
import dao.entities.User;
import dao.impl.ProfileDAOjdbc;
import dao.impl.UserDAOjdbc;
import dao.interfaces.ProfileDAO;
import dao.interfaces.UserDAO;
import helpers.MainHelper;
import services.forms.Field;
import services.forms.Form;
import sun.applet.Main;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.http.*;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.Map;

public class UserService {

    private UserDAO userDAO = new UserDAOjdbc();
    private ProfileDAO profileDAO = new ProfileDAOjdbc();

    public User getCurrentUser(ServletRequest req) {
        HttpSession session = ((HttpServletRequest)req).getSession();
        return (User) session.getAttribute("current_user");
    }

    public Cookie getRememberCookie(ServletRequest req) {
        if (((HttpServletRequest) req).getCookies() != null) {
            for (Cookie cookie : ((HttpServletRequest) req).getCookies()) {
                if (cookie.getName().equals("remember") && !cookie.getValue().equals("")) {
                    return cookie;
                }
            }
        }
        return null;
    }

    public User authenticate(String token) {
        return userDAO.getByToken(token);
    }

    public User authenticate(HttpServletRequest req) {
        String username = req.getParameter("username");
        if (username != null) {
            User user = userDAO.getByUsername(username);
            if (user == null) {
                return null;
            }
            String password = encode(req.getParameter("password"));
            if (password.equals(user.getPassword())) {
                return user;
            }
        }
        return null;
    }

    public void authorize(User current_user, ServletRequest req) {
        HttpSession session = ((HttpServletRequest)req).getSession();
        session.setAttribute("current_user", current_user);
    }

    public void saveUser(User current_user) {
        userDAO.update(current_user);
        profileDAO.update(current_user.getProfile());
    }

    public boolean isBlank(HttpServletRequest req) {
        Map<String, String[]> params = req.getParameterMap();
        for (String key : params.keySet()) {
            if (params.get(key).length > 0) {
                return false;
            }
        }
        return true;
    }

    public User createUser(ServletContext sc, HttpServletRequest req) {
        String username = req.getParameter("username");
        String password = encode(req.getParameter("password1"));
        String email = req.getParameter("email");
        String firstName = req.getParameter("first_name");
        String lastName = req.getParameter("last_name");
        String image = null;
        try {
            image = MainHelper.saveFile(sc, req.getPart("image"), username);
        } catch (IOException | ServletException e) {
            e.printStackTrace();
        }
        Profile raw_profile = new Profile(0, firstName, lastName, email, false);
        if (image != null) {
            raw_profile.setImage(image);
        }
        User raw_user = new User(0, username, password,null, raw_profile);
        userDAO.create(raw_user);
        User user = userDAO.getByUsernameWithoutProfile(raw_user.getUsername());
        raw_profile.setId(user.getId());
        user.setProfile(raw_profile);
        profileDAO.create(raw_profile);
        return user;
    }

    public boolean validate(HttpServletRequest req) {
        Map<String, String[]> params = req.getParameterMap();
        List<Field> fields = new ArrayList<>();
        for (String key : params.keySet()) {
            if (!key.equals("remember") && !key.equals("image")) {
                fields.add(new Field(req.getParameter(key), key));
            }
        }
        return new Form(fields).validate();
    }

    public void remember(HttpServletRequest req, HttpServletResponse resp, User current_user) {
        if (req.getParameter("remember") != null) {
            String token = current_user.getUsername() + System.currentTimeMillis();
            current_user.setToken(encode(token));
            saveUser(current_user);
            Cookie cookie = new Cookie("remember", current_user.getToken());
            cookie.setMaxAge(MainHelper.getRememberTime());
            resp.addCookie(cookie);
        }
    }

    public boolean isModerator(HttpServletRequest req) {
        return ((User)req.getSession().getAttribute("current_user")).getProfile().isModerator();
    }

    public boolean hasPermission(HttpServletRequest req, int id) {
        return ((User)req.getSession().getAttribute("current_user")).getId() == id;
    }

    private String encode(String string) {
        String encoded = string;
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] hash = digest.digest(MainHelper.salt(string).getBytes(StandardCharsets.UTF_8));
            encoded = Base64.getEncoder().encodeToString(hash);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return encoded;
    }
}
