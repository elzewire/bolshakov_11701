<#import "common/base_menu.ftl" as bm>

<@bm.head>
    <title>Фотокарточки</title>

    <script type="text/javascript" src="/static/js/image-picker.js"></script>
    <link rel="stylesheet" href="/static/css/albums.css">
    <link rel="stylesheet" href="/static/css/image-picker.css">
    <script type="text/javascript" src="/static/js/albums.js"></script>
</@>

<@bm.body>
    <div class="col-10">
        <div class="row my-3">
            <div class="col">
                <div class="segment bg-light p-3">
                    <div class="row">
                        <div class="col-10 pt-2">
                            <h6>${album.name} <span class="text-secondary">${album.size}</span><h6>
                        </div>
                        <#if album.id != auth_user.defaultAlbum.id>
                            <div class="col-2">
                                <button class="btn btn-block edit text-danger"  data-toggle="modal" data-target="#album-modal">Редактировать</button>
                            </div>
                        </#if>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col">
                            <form method="post" action="/albums/${album.id}/add" enctype="multipart/form-data">
                                <input type="hidden" name="_csrf" value="${_csrf.token}">
                                <div class="form-group row">
                                    <div class="col">
                                        <label for="file">Вклеить фотокарточки</label>
                                    </div>
                                </div>
                                <div class="row pb-3">
                                    <div class="col-9 ml-2">
                                        <input type="file" class="custom-file-input" name="files" id="file" multiple accept="image/*" onchange="changeLabel(this)">
                                        <label id="inner-label" class="custom-file-label" for="file">Выберите фото</label>
                                    </div>
                                    <div class="col"></div>
                                    <div class="col-2"><button class="btn btn-block edit text-danger">Вклеить</button></div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <select disabled multiple="multiple" class="image-picker show-html" form="post-form">
                                <#list album.photos?sort_by("id") as photo>
                                    <option disabled data-img-src="/img/${photo.filename}" value="${photo.id}"></option>
                                </#list>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <#if album.id != auth_user.defaultAlbum.id>
        <!-- Modal -->
        <div class="modal fade" id="album-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Добавить альбом</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form id="album-form" method="post" action="/albums/${album.id}/edit">
                            <input type="hidden" name="_csrf" value="${_csrf.token}">
                            <div class="form-group">
                                <label for="photos">Выберите фотокарточки</label>
                                <select multiple="multiple" name="photos" id="photo-selection" class="image-picker show-html" form="post-form">
                                    <#list all_photos?sort_by("id") as photo>
                                        <option data-img-src="/img/${photo.filename}" value="${photo.id}"></option>
                                    </#list>
                                </select>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Отмена</button>
                        <button type="submit" class="btn btn-danger" form="album-form">Сохранить</button>
                    </div>
                </div>
            </div>
        </div>
    </#if>
</@>