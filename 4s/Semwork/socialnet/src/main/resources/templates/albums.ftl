<#import "common/base_menu.ftl" as bm>

<@bm.head>
    <link rel="stylesheet" href="/static/css/albums.css">
    <script type="text/javascript" src="/static/js/profile.js"></script>
    <title>Фотокарточки</title>
</@>

<@bm.body>
    <div class="col-10">
        <div class="row my-3">
            <div class="col">
                <div class="segment bg-light p-3">
                    <div class="row">
                        <div class="col-3 pt-2">
                            <h6>Мои альбомы <span class="text-secondary">${albumsCount}</span></h6>
                        </div>
                        <div class="col-7">

                        </div>
                        <div class="col-2">
                            <#if self??>
                                <button class="btn btn-block edit text-danger" data-toggle="modal" data-target="#album-modal">Добавить</button>
                            </#if>
                        </div>
                    </div>
                    <hr>
                    <#list albums?sort_by("id") as album>
                        <div class="row album pt-3">
                            <div class="col-4">
                                <#if album.preview??>
                                    <img src="/img/${album.preview.filename}" class="my-auto rounded w-100" alt="">
                                <#else>
                                    <img src="/static/res/default.png" class="my-auto rounded w-100" alt="">
                                </#if>
                            </div>
                            <div class="col-8">
                                <a href="/albums/${user.id}/${album.id}" class="stretched-link"></a>
                                <div><strong>${album.name}</strong></div>
                                <div><span class="text-secondary">${album.size}</span> фотокарточек</div>
                            </div>
                        </div>
                    </#list>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="album-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Добавить альбом</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="album-form" method="post">
                        <input type="hidden" name="_csrf" value="${_csrf.token}">
                        <div class="form-group">
                            <label for="name">Название альбома</label>
                            <input type="text" class="form-control ${(nameError??)?string('is-invalid', '')}" name="name" id="name" value="<#if album??>${album.name}</#if>">
                            <#if nameError??>
                                <div class="invalid-feedback">
                                    ${nameError}
                                </div>
                            </#if>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Отмена</button>
                    <button type="submit" class="btn btn-danger" form="album-form">Добавить</button>
                </div>
            </div>
        </div>
    </div>
</@>