<#import "common/base_nomenu.ftl" as bn>

<@bn.head>
    <link rel="stylesheet" href="/static/css/register.css">
    <script type="text/javascript" src="/static/js/register.js"></script>

    <title>Регистрация</title>
</@bn.head>

<@bn.body>
    <nav class="sticky-top bg-danger">
        <div class="container">
            <div class="row mb-0">
                <div class="col-2">
                    <div class="navbar-brand my-auto">
                        <img src="/static/res/molot_serp.png" width="30" height="30" alt="">
                    </div>
                </div>
                <div class="col-8"></div>
                <div class="col-2 sign-up">
                    <a class="nav-link text-light font-weight-bold" href="/login" id="navbarDropdown" role="button">
                        Авторизация
                    </a>
                </div>
            </div>
        </div>
    </nav>
    <div class="container">
        <div class="row mt-4">
            <div class="col"></div>
            <div class="col">
                <form id="register" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="_csrf" value="${_csrf.token}">
                    <div class="form-group">
                        <label for="username">Имя пользователя</label>
                        <input type="text" class="form-control ${(usernameError??)?string('is-invalid', '')}"
                               value="<#if user??>${user.username}</#if>" name="username" id="username">
                        <#if usernameError??>
                            <div class="invalid-feedback">
                                ${usernameError}
                            </div>
                        </#if>
                    </div>
                    <div class="form-group">
                        <label for="password">Пароль</label>
                        <input type="password" class="form-control ${(passwordError??)?string('is-invalid', '')}"
                               value="<#if user??>${user.password}</#if>" name="password" id="password">
                        <#if passwordError??>
                            <div class="invalid-feedback">
                                ${passwordError}
                            </div>
                        </#if>
                    </div>
                    <div class="form-group">
                        <label for="password2">Повторите пароль</label>
                        <input type="password" class="form-control ${(password2Error??)?string('is-invalid', '')}"
                               value="<#if password2??>${password2}</#if>" name="password2" id="password2">
                        <#if password2Error??>
                            <div class="invalid-feedback">
                                ${password2Error}
                            </div>
                        </#if>
                    </div>
                    <div class="form-group">
                        <label for="email">Электронная почта</label>
                        <input type="email" class="form-control ${(emailError??)?string('is-invalid', '')}"
                               value="<#if email??>${email}</#if>" name="email" id="email">
                        <#if emailError??>
                            <div class="invalid-feedback">
                                ${emailError}
                            </div>
                        </#if>
                    </div>
                    <div class="form-group">
                        <label for="firstName">Имя</label>
                        <input type="text" class="form-control ${(firstNameError??)?string('is-invalid', '')}"
                               value="<#if user??>${user.firstName}</#if>" name="firstName" id="firstName">
                        <#if firstNameError??>
                            <div class="invalid-feedback">
                                ${firstNameError}
                            </div>
                        </#if>
                    </div>
                    <div class="form-group">
                        <label for="lastName">Фамилия</label>
                        <input type="text" class="form-control ${(lastNameError??)?string('is-invalid', '')}"
                               value="<#if user??>${user.lastName}</#if>" name="lastName" id="lastName">
                        <#if lastNameError??>
                            <div class="invalid-feedback">
                                ${lastNameError}
                            </div>
                        </#if>
                    </div>
                    <div class="form-group">
                        <label for="birthDate">Дата рождения</label>
                        <input type="date" class="form-control" name="birthDate" id="birthDate">
                    </div>
                    <div class="form-group">
                        <label for="file">Фото</label>
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" name="file" id="file" accept="image/*" onchange="readURL(this);">
                            <label id="inner-label" class="custom-file-label" for="file">Выберите фото</label>
                        </div>
                    </div>
                    <div id="input-preview-wrap" class="mb-2">
                        <img id="input-preview" class="mr-1 my-auto" src="" width="120" height="120" alt="">
                        <button type="button" class="close" aria-label="Close" onclick="delPhoto()">
                            <span>&times;</span>
                        </button>
                    </div>
                    <button type="submit" class="btn btn-danger btn-block" form="register">Зарегистрироваться</button>
                </form>
            </div>
            <div class="col"></div>
        </div>
    </div>
</@bn.body>