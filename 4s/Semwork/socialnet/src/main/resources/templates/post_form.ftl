<#macro post_form>
    <div class="row my-3">
        <div class="col">
            <div class="post-create p-3 segment bg-light">
                <div class="row">
                    <div class="col-1">
                        <#if auth_user.avatar??>
                            <img src="/img/${auth_user.avatar.filename}" class="my-auto rounded-circle mx-2" width="30" height="30" alt="">
                        <#else>
                             <img src="/static/res/default.png" class="my-auto rounded-circle mx-2" width="30" height="30" alt="">
                        </#if>
                    </div>
                    <div class="col">
                        <form id="post-form" method="post" enctype="multipart/form-data" action="/profile/${user.id}/post">
                            <input type="hidden" name="_csrf" value="${_csrf.token}">
                            <textarea name="text" onfocus="showForm()" id="post-input" class="post-input" placeholder="Что у вас нового, товарищ?"></textarea>
                        </form>
                    </div>
                </div>
                <div id="post-hidden">
                    <hr class="">
                    <a class="text-center text-danger" data-toggle="modal" data-target="#photos">&#128247;</a>
                    <span class="text-center text-danger" id="photo-amount"></span>
                    <button id="post-button" class="btn btn-danger" type="submit" form="post-form">Высказаться</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="photos" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <select multiple="multiple" name="photos" id="photo-selection" class="image-picker show-html" form="post-form">
                        <#list photos as photo>
                            <option data-img-src="/img/${photo.filename}" value="${photo.id}"></option>
                        </#list>
                    </select>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal" onclick="updatePreview()">Сохранить</button>
                </div>
            </div>
        </div>
    </div>
</#macro>