<#import "common/base_nomenu.ftl" as bn>
<#import "common/navbar.ftl" as n>

<@bn.head>
    <link rel="stylesheet" href="/static/css/register.css">
    <link rel="stylesheet" href="/static/css/profile.css">
    <script type="text/javascript" src="/static/js/register.js"></script>

    <title>Корректировка досье</title>
</@bn.head>

<@bn.body>
<@n.navbar/>
    <div class="container">
        <div class="row mt-4">
            <div class="col"></div>
            <div class="col">
                <form id="edit" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="_csrf" value="${_csrf.token}">
                    <div class="form-group">
                        <label for="firstName">Имя</label>
                        <input type="text" class="form-control ${(firstNameError??)?string('is-invalid', '')}"
                               value="<#if user??>${user.firstName}</#if>" name="firstName" id="firstName">
                        <#if firstNameError??>
                            <div class="invalid-feedback">
                                ${firstNameError}
                            </div>
                        </#if>
                    </div>
                    <div class="form-group">
                        <label for="lastName">Фамилия</label>
                        <input type="text" class="form-control ${(lastNameError??)?string('is-invalid', '')}"
                               value="<#if user??>${user.lastName}</#if>" name="lastName" id="lastName">
                        <#if lastNameError??>
                            <div class="invalid-feedback">
                                ${lastNameError}
                            </div>
                        </#if>
                    </div>
                    <div class="form-group">
                        <label for="lastName">Статус</label>
                        <input type="text" class="form-control ${(lastNameError??)?string('is-invalid', '')}"
                               value="<#if user.status??>${user.status}</#if>" name="status" id="status">
                        <#if statusError??>
                            <div class="invalid-feedback">
                                ${statusError}
                            </div>
                        </#if>
                    </div>
                    <div class="form-group">
                        <label for="birthDate">Дата рождения</label>
                        <input type="date" class="form-control" name="birthDate" id="birthDate" value="<#if user.birthDate??>${user.birthDate?date}</#if>">
                    </div>
                    <button type="submit" class="btn btn-danger btn-block">Сохранить</button>
                </form>
            </div>
            <div class="col"></div>
        </div>
    </div>
</@bn.body>