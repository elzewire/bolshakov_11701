<#macro posts_macro>
    <#list posts as post>
        <div class="row my-3">
            <div class="col">
                <div class="post segment p-3 bg-light">
                    <div class="row">
                        <div class="col-2 pr-0">
                            <#if post.author.avatar??>
                                <img class="user-photo rounded rounded-circle my-auto" src="/img/${post.author.avatar.filename}" width="60" height="60" alt="">
                            <#else>
                                <img class="user-photo rounded rounded-circle my-auto" src="/static/res/default.png" width="60" height="60" alt="">
                            </#if>
                        </div>
                        <div class="col-9 pl-0 pt-2">
                            <a href="/profile/${post.author.id}" class="text-danger stretched-link font-weight-bold">${post.author.firstName} ${post.author.lastName}</a>
                            <div class="text-secondary">${post.datetime}</div>
                        </div>
                        <div class="col-1">
                            <a class="dropdown-toggle text-dark font-weight-bold" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            </a>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="#">Выбросить</a>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col m-2">
                            <div>${post.text}</div>
                            <#list post.photos as photo>
                                <img class="include mt-1 my-auto" src="/img/${photo.filename}" alt="">
                            </#list>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col">
                            <a class="mx-2 post-item text-danger text-decoration-none" href="">&#10084;</a><span class="post-counter text-danger">${post.likes?size}</span>
                            <a class="mx-2 post-item text-danger text-decoration-none" data-toggle="collapse" href="#commentCollapse${post.id}">&#128172;</a><span class="post-counter text-danger">${post.comments?size}</span>
                        </div>
                    </div>
                    <div class="collapse row border-top mt-3" id="commentCollapse${post.id}">
                        <div class="col">
                            <#list post.comments?sort_by("datetime") as comment>
                                <div class="comment row">
                                    <div class="col-1 pt-2">
                                        <#if comment.commentAuthor.avatar??>
                                            <img class="user-photo rounded rounded-circle my-auto" src="/img/${comment.commentAuthor.avatar.filename}"
                                                 width="30" height="30" alt="">
                                        <#else>
                                            <img class="user-photo rounded rounded-circle my-auto" src="/static/res/default.png"
                                                                                         width="30" height="30" alt="">
                                        </#if>
                                    </div>
                                    <div class="col-10 pt-2">
                                        <a href="#" class="text-danger stretched-link font-weight-bold">${comment.commentAuthor.firstName} ${comment.commentAuthor.lastName}</a>
                                        <div>${comment.text}</div>
                                        <div class="text-secondary">${comment.datetime}</div>
                                    </div>
                                </div>
                                <hr>
                            </#list>
                            <div class="row commentator">
                                <div class="col-1 pt-3">
                                    <#if auth_user.avatar??>
                                        <img src="/img/${auth_user.avatar.filename}" class="my-auto rounded-circle mx-2" width="30" height="30" alt="">
                                    <#else>
                                        <img src="/static/res/default.png" class="my-auto rounded-circle mx-2" width="30" height="30" alt="">
                                    </#if>
                                </div>
                                <div class="col-11 pt-3">
                                    <form method="post" class="form-inline" action="/profile/${user.id}/${post.id}/comment">
                                        <input type="hidden" name="_csrf" value="${_csrf.token}">
                                        <input class="comment-input" type="text" name="text" placeholder="Высказаться...">
                                        <label for="submit" class="mx-2 post-item text-danger text-decoration-none">&#128233;</label>
                                        <button type="submit" id="submit" style="display: none;"></button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </#list>
</#macro>