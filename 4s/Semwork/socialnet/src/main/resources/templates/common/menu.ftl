<#macro menu>
    <div class="col-2">
        <div class="row">
            <div class="col">
                <div class="menu mt-3">
                    <ul class="flex-column">
                        <li class="menu-item mb-2">
                            <a class="text-decoration-none" href="/profile"><span>&#127968;</span> Моё Досье</a>
                        </li>
                        <li class="menu-item mb-2">
                            <a class="text-decoration-none" href="#"><span>&#128240;</span> Информбюро</a>
                        </li>
                        <li class="menu-item mb-2">
                            <a class="text-decoration-none" href="#"><span>&#9993;</span> Телеграммы</a>
                        </li>
                        <li class="menu-item mb-2">
                            <a class="text-decoration-none" href="#"><span>&#128100;</span> Товарищи</a>
                        </li>
                        <li class="menu-item mb-2">
                            <a class="text-decoration-none" href="/albums"><span>&#128247;</span> Фотокарточки</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</#macro>