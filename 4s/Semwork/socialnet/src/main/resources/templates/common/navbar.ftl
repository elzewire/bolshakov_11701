<#macro navbar>
<nav class="sticky-top bg-danger">
    <div class="container">
        <div class="row">
            <div class="col-2">
                <div class="navbar-brand my-auto">
                    <img src="/static/res/molot_serp.png" width="30" height="30" alt="">
                </div>
            </div>
            <div class="col-10">
                <div class="row">
                    <div class="col-4 my-auto">
                        <form class="form-inline my-auto">
                            <input class="search" type="search" placeholder="&#128270; Розыск" aria-label="Search">
                        </form>
                    </div>
                    <div class="col-8 my-auto">
                        <ul class="navbar-nav ml-auto w-25">
                            <li class="nav-menu nav-item dropdown px-2">
                                <a class="nav-link dropdown-toggle text-light font-weight-bold" href="" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    ${auth_user.firstName}
                                    <#if auth_user.avatar??>
                                        <img src="/img/${auth_user.avatar.filename}" class="nav-avatar my-auto rounded-circle mx-2" width="30" height="30" alt="">
                                    <#else>
                                        <img src="/static/res/default.png" class="nav-avatar my-auto rounded-circle mx-2" width="30" height="30" alt="">
                                    </#if>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="/profile">Моё Досье</a>
                                    <a class="dropdown-item" href="#">Настройки</a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="/logout">Эмигрировать</a>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</nav>
</#macro>