<#import "common/base_nomenu.ftl" as bn>

<@bn.head>
    <link rel="stylesheet" href="/static/css/login.css">

    <title>Авторизация</title>
</@bn.head>

<@bn.body>
    <nav class="sticky-top bg-danger">
        <div class="container">
            <div class="row mb-0">
                <div class="col-2">
                    <div class="navbar-brand my-auto">
                        <img src="/static/res/molot_serp.png" width="30" height="30" alt="">
                    </div>
                </div>
                <div class="col-8"></div>
                <div class="col-2 sign-up">
                    <a class="nav-link text-light font-weight-bold" href="/registration" id="navbarDropdown" role="button">
                        Регистрация
                    </a>
                </div>
            </div>
        </div>
    </nav>
    <div class="container">
        <div class="row mt-4">
            <div class="col"></div>
            <div class="col">
                <form method="post">
                    <input type="hidden" name="_csrf" value="${_csrf.token}">
                    <div class="form-group">
                        <label for="username">Имя пользователя</label>
                        <input type="text" class="form-control" name="username" id="username">
                        <#if message??>
                            <div class="valid-feedback">
                                ${message}
                            </div>
                        </#if>
                        <#if error??>
                            <div class="invalid-feedback">
                                ${error}
                            </div>
                        </#if>
                    </div>
                    <div class="form-group">
                        <label for="password">Пароль</label>
                        <input type="password" class="form-control" name="password" id="password">
                    </div>
                    <button type="submit" class="btn btn-danger btn-block">Войти</button>
                </form>
            </div>
            <div class="col"></div>
        </div>
    </div>
</@bn.body>