<#import "common/base_menu.ftl" as bm>
<#import "post_form.ftl" as pf>
<#import "posts.ftl" as p>

<@bm.head>
    <link rel="stylesheet" href="/static/css/image-picker.css">
    <link rel="stylesheet" href="/static/css/profile.css">
    <script type="text/javascript" src="/static/js/image-picker.js"></script>
    <title>${user.firstName} ${user.lastName}</title>
</@>

<@bm.body>
    <div class="col-10">
        <div class="row">
            <div class="col-4">
                <div class="row my-3">
                    <div class="col">
                        <div class="avatar segment p-3 bg-light">
                            <#if user.avatar??>
                                <img src="/img/${user.avatar.filename}" class="my-auto rounded" alt="">
                            <#else>
                                <img src="/static/res/default.png" class="my-auto rounded" alt="">
                            </#if>
                            <#if self??>
                                <div class="mt-3">
                                    <a href="/profile/edit"><button class="btn btn-block edit text-danger">Корректировать</button></a>
                                </div>
                            </#if>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-8">
                <div class="row my-3">
                    <div class="col">
                        <div class="information segment p-3 bg-light">
                            <div class="title row">
                                <div class="title-left col">
                                    <h4>${user.firstName} ${user.lastName}</h4>
                                    <p class="text-secondary">
                                        <#if !user.status??>
                                            Тезис
                                        <#else>
                                            ${user.status}
                                        </#if>
                                    </p>
                                </div>
                            </div>
                            <hr class="my-0 mb-2">
                            <div class="extra-info row">
                                <div class="extra-info-left col-4">
                                </div>
                                <div class="extra-info-right col-8">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <@pf.post_form/>
                <@p.posts_macro/>
            </div>
        </div>
    </div>

    <script type="text/javascript" src="/static/js/profile.js"></script>
</@bm.body>