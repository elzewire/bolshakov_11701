$("select").imagepicker();

function changeLabel(input) {
    var val = ""
    for (i = 0; i < input.files.length; i++) {
      val = val + input.files[i].name +"; "
    }
    $("#inner-label").html(val)
}

function delPhoto() {	
  formData = new FormData($("#file"));
  formData.delete("file")

  $('#file').val('');
  $("#inner-label").html("Выберите фото")
}