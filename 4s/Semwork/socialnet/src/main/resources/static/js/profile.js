$("select").imagepicker();

function showForm() {

	if ($("#post-hidden").css("display") === "none") {
		$("#post-hidden").css("display", "block");
		$("#post-input").css({"height": "70px", "width" : "100%"});
		$('#post-add-photo1').css("display", "none");
	}

}

function hideForm() {

	if ($("#post-hidden").css("display") !== "none") {
		if ($("#post-input").val() === "") {
			$("#post-hidden").css("display", "none");
			$("#post-input").css({"height": "23px", "width" : "50%"});
			$('#post-add-photo1').css("display", "block");
		}
	}

}


function updatePreview() {
	var amount = $('#photo-selection').val().length;
	$('#photo-amount').html(amount);
}