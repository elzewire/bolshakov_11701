function readURL(input) {

  if (input.files && input.files[0]) {
    var reader = new FileReader();
    reader.onload = function(e) {
		$('#input-preview').attr('src', e.target.result);
		$('#input-preview-wrap').css('display', "block");
    	$("#inner-label").html(input.files[0].name)
    }


    reader.readAsDataURL(input.files[0]);
  }
}

function delPhoto() {	
	formData = new FormData($("#file"));
	formData.delete("file")

	$('#file').val('');
  	$('#input-preview').attr('src', '');
	$('#input-preview-wrap').css('display', "none");
	$("#inner-label").html("Выберите фото")
}