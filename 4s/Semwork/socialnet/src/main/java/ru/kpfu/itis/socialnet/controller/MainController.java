package ru.kpfu.itis.socialnet.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class MainController {

    @GetMapping("/asda")
    public String hello() {
        return "redirect:/profile";
    }


}
