package ru.kpfu.itis.socialnet.repos;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.kpfu.itis.socialnet.domain.Post;
import ru.kpfu.itis.socialnet.domain.User;

import java.util.Set;

public interface PostRepo extends JpaRepository<Post, Long> {
    Set<Post> getByTargetOrderByDatetimeDesc(User user);
}
