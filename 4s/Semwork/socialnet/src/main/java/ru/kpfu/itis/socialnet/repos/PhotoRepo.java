package ru.kpfu.itis.socialnet.repos;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.kpfu.itis.socialnet.domain.Photo;

public interface PhotoRepo extends JpaRepository<Photo, Long> {
    Photo getById(Long id);
}
