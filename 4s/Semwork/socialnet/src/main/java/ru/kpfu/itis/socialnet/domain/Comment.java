package ru.kpfu.itis.socialnet.domain;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.Date;

@Entity
public class Comment {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn
    private User commentAuthor;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn
    private Post commentedPost;

    @NotBlank
    private String text;

    @Temporal(TemporalType.TIMESTAMP)
    private Date datetime;

    public Comment() {
    }

    public Comment(User author, Post post, String text, Date datetime) {
        this.commentAuthor = author;
        this.commentedPost = post;
        this.text = text;
        this.datetime = datetime;
    }

    public User getCommentAuthor() {
        return commentAuthor;
    }

    public void setCommentAuthor(User author) {
        this.commentAuthor = author;
    }

    public Post getCommentedPost() {
        return commentedPost;
    }

    public void setCommentedPost(Post post) {
        this.commentedPost = post;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Date getDatetime() {
        return datetime;
    }

    public void setDatetime(Date datetime) {
        this.datetime = datetime;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
