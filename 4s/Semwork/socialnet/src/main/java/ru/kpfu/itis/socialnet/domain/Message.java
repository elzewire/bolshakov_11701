package ru.kpfu.itis.socialnet.domain;

import javax.persistence.*;
import java.util.Date;

@Entity
public class Message {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn
    private User sender;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn
    private User receiver;

    @Temporal(TemporalType.TIMESTAMP)
    private Date datetime;

    private String text;

    public Message() {
    }

    public Message(User sender, User receiver, Date datetime, String text) {
        this.sender = sender;
        this.receiver = receiver;
        this.datetime = datetime;
        this.text = text;
    }

    public User getSender() {
        return sender;
    }

    public void setSender(User sender) {
        this.sender = sender;
    }

    public User getReceiver() {
        return receiver;
    }

    public void setReceiver(User receiver) {
        this.receiver = receiver;
    }

    public Date getDatetime() {
        return datetime;
    }

    public void setDatetime(Date datetime) {
        this.datetime = datetime;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
