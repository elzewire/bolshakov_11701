package ru.kpfu.itis.socialnet.controller;

import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import ru.kpfu.itis.socialnet.domain.Album;
import ru.kpfu.itis.socialnet.domain.Photo;
import ru.kpfu.itis.socialnet.domain.User;
import ru.kpfu.itis.socialnet.repos.AlbumRepo;
import ru.kpfu.itis.socialnet.repos.PhotoRepo;
import ru.kpfu.itis.socialnet.repos.UserRepo;

import javax.validation.Valid;
import java.io.File;
import java.io.IOException;
import java.util.*;

@Controller
@RequestMapping("/albums")
public class AlbumController {

    @Autowired
    private AlbumRepo albumRepo;
    @Autowired
    private UserRepo userRepo;
    @Value("${upload.path}")
    private String uploadPath;
    @Autowired
    private PhotoRepo photoRepo;

    @Transactional
    @GetMapping("{user}")
    public String getAlbums(@AuthenticationPrincipal User auth_user, @PathVariable User user, Model model) {
        if (auth_user.getId() == user.getId()) {
            return "redirect:/albums";
        }
        Set<Album> albums = user.getAlbums();
        model.addAttribute("user", user);
        model.addAttribute("auth_user", auth_user);
        model.addAttribute("albums", albums);
        model.addAttribute("albumsCount", albums.size());
        return "albums";
    }

    @Transactional
    @GetMapping
    public String getAlbums(@AuthenticationPrincipal User user, Model model) {
        Set<Album> albums = user.getAlbums();
        model.addAttribute("user", user);
        model.addAttribute("auth_user", user);
        model.addAttribute("albums", albums);
        model.addAttribute("albumsCount", albums.size());
        model.addAttribute("self", true);
        return "albums";
    }

    @PostMapping
    public String createAlbum(@AuthenticationPrincipal User user, @Valid Album album, BindingResult bindingResult, Model model) {
        album.setHolder(user);
        Set<Album> albums = user.getAlbums();
        model.addAttribute("auth_user", user);
        model.addAttribute("albums", albums);
        model.addAttribute("albumsCount", albums.size());
        model.addAttribute("self", true);
        if (bindingResult.hasErrors()) {
            model.mergeAttributes(ControllerHelpers.getErrors(bindingResult));
            model.addAttribute("album", album);
        } else {
            albumRepo.save(album);
            user.getAlbums().add(album);
            userRepo.save(user);
            return "redirect:/albums";
        }
        return "albums";
    }

    @GetMapping("{user}/{album_id}")
    public String albumDetails(@AuthenticationPrincipal User auth_user, @PathVariable User user, @PathVariable("album_id") String album_id, Model model) {
        Album album = albumRepo.getById(Long.parseLong(album_id));
        Set<Photo> photos = album.getPhotos();
        model.addAttribute("auth_user", auth_user);
        model.addAttribute("user", user);
        model.addAttribute("photos", photos);
        model.addAttribute("album", album);
        return "album_details";
    }

    @PostMapping("/{album}/add")
    public String addToAlbum(@AuthenticationPrincipal User auth_user, @RequestParam("files") List<MultipartFile> files,
                             @PathVariable Album album, Model model) throws IOException {

        //Сохраняем каждый файл
        for (MultipartFile file : files) {
            if (file != null && file.getOriginalFilename() != null && !file.getOriginalFilename().isEmpty()) {
                //Сохранить файл
                File uploadDir = new File(uploadPath);

                if (!uploadDir.exists()) {
                    uploadDir.mkdir();
                }

                String uuidFile = UUID.randomUUID().toString();
                String resultFilename = uuidFile + "." + file.getOriginalFilename();

                file.transferTo(new File(uploadPath + "/" + resultFilename));

                //Создать вложение и сохранить его в дефолтном альбоме
                Photo photo = new Photo();
                photo.setFilename(resultFilename);
                photo.getAlbums().add(auth_user.getDefaultAlbum());
                photo.getAlbums().add(album);
                photoRepo.save(photo);
            }
        }
        return "redirect:/albums" + album.getId();
    }
}
