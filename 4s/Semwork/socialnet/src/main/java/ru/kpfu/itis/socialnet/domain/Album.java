package ru.kpfu.itis.socialnet.domain;

import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Album {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @NotBlank(message = "Пожалуйста, введите название альбома")
    @Length(max = 255, message = "Слишком длинное название альбома")
    private String name;

    @ManyToOne(fetch = FetchType.EAGER)
    private User holder;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "album_photos",
            joinColumns = { @JoinColumn(name = "album_id") },
            inverseJoinColumns = { @JoinColumn(name = "photo_id") }
    )
    private Set<Photo> photos = new HashSet<>();

    public Album() {
    }

    public Album(String name) {
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Photo> getPhotos() {
        return photos;
    }

    public void setPhotos(Set<Photo> photos) {
        this.photos = photos;
    }

    public User getHolder() {
        return holder;
    }

    public void setHolder(User holder) {
        this.holder = holder;
    }

    public int getSize() {
        return getPhotos().size();
    }

    public Photo getPreview() {
        for (Photo photo : getPhotos()) {
            return photo;
        }
        return null;
    }
}
