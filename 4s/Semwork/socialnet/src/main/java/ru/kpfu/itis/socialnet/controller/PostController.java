package ru.kpfu.itis.socialnet.controller;

import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.kpfu.itis.socialnet.domain.Comment;
import ru.kpfu.itis.socialnet.domain.Photo;
import ru.kpfu.itis.socialnet.domain.Post;
import ru.kpfu.itis.socialnet.domain.User;
import ru.kpfu.itis.socialnet.repos.CommentRepo;
import ru.kpfu.itis.socialnet.repos.PhotoRepo;
import ru.kpfu.itis.socialnet.repos.PostRepo;

import javax.validation.Valid;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Controller
@RequestMapping("/profile")
public class PostController {

    @Autowired
    private PhotoRepo photoRepo;
    @Autowired
    private PostRepo postRepo;
    @Autowired
    private CommentRepo commentRepo;

    @PostMapping("{user}/post")
    public String createPost(@AuthenticationPrincipal User auth_user, @PathVariable User user,
                             @RequestParam(value = "photos", required = false) List<Long> photos, @Valid Post post,
                             BindingResult bindingResult, Model model) {
        post.setTarget(user);
        post.setAuthor(auth_user);
        post.setDatetime(new Date());
        if (!bindingResult.hasErrors()) {

            //Сохранить сопутствующие фотографии
            if (photos != null) {
                Set<Photo> photosSet = new HashSet<>();
                for (Long id : photos) {
                    photosSet.add(photoRepo.getById(id));
                }

                post.setPhotos(photosSet);
            }
            postRepo.save(post);
        } else {
            System.out.println(ControllerHelpers.getErrors(bindingResult));
        }

        return "redirect:/profile/" + user.getId();
    }

    @PostMapping("{user}/{post}/comment")
    public String commentPost(@AuthenticationPrincipal User auth_user, @PathVariable User user,
                              @PathVariable Post post, @Valid Comment comment,
                              BindingResult bindingResult, Model model) {
        comment.setCommentAuthor(auth_user);
        comment.setCommentedPost(post);
        comment.setDatetime(new Date());
        if (!bindingResult.hasErrors()) {
            commentRepo.save(comment);
        } else {
            System.out.println(ControllerHelpers.getErrors(bindingResult));
        }
        return "redirect:/profile/" + user.getId();
    }
}
