package ru.kpfu.itis.socialnet.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import ru.kpfu.itis.socialnet.domain.User;
import ru.kpfu.itis.socialnet.repos.UserRepo;
import ru.kpfu.itis.socialnet.service.UserService;

import java.io.IOException;

@Controller
@RequestMapping("/registration")
public class RegistrationController {
    @Value("${upload.path}")
    private String uploadPath;
    @Autowired
    private UserRepo userRepo;
    @Autowired
    private UserService userService;

    @GetMapping
    public String registration() {
        return "registration";
    }

    @PostMapping
    public String createUser(@RequestParam("password2") String password2, @RequestParam("file") MultipartFile file,
                             @Validated(User.Create.class) User user, BindingResult bindingResult, Model model) throws IOException {
        if (password2.isEmpty()) {
            model.addAttribute("password2Error", "Пожалуйста повторите пароль!");
        }
        model.addAttribute("password2", password2);
        if (bindingResult.hasErrors()) {
            if (user.getPassword() != null && !user.getPassword().equals(password2)) {
                model.addAttribute("password2Error", "Пароли не совпадают!");
            }
            model.mergeAttributes(ControllerHelpers.getErrors(bindingResult));
            model.addAttribute("user", user);
        } else {
            User userFromDb = userRepo.findByUsername(user.getUsername());
            if (userFromDb != null) {
                model.addAttribute("usernameError", "Такой пользователь уже зарегистрирован!");
            } else {
                if (user.getPassword() != null && !user.getPassword().equals(password2)) {
                    model.addAttribute("password2Error", "Пароли не совпадают!");
                } else {
                    userService.createUser(file, user, uploadPath);
                    return "redirect:/login";
                }
            }
        }
        return "registration";
    }
}
