package ru.kpfu.itis.socialnet.repos;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.kpfu.itis.socialnet.domain.Comment;

public interface CommentRepo extends JpaRepository<Comment, Long> {
}
