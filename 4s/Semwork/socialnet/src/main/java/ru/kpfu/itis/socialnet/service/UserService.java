package ru.kpfu.itis.socialnet.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;
import ru.kpfu.itis.socialnet.domain.Album;
import ru.kpfu.itis.socialnet.domain.Photo;
import ru.kpfu.itis.socialnet.domain.Role;
import ru.kpfu.itis.socialnet.domain.User;
import ru.kpfu.itis.socialnet.repos.AlbumRepo;
import ru.kpfu.itis.socialnet.repos.PhotoRepo;
import ru.kpfu.itis.socialnet.repos.UserRepo;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.UUID;


@Service
public class UserService implements UserDetailsService {

    @Autowired
    private MailSender mailSender;
    @Autowired
    private UserRepo userRepo;
    @Autowired
    private AlbumRepo albumRepo;
    @Autowired
    private PhotoRepo photoRepo;
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return userRepo.findByUsername(username);
    }

    public void createUser(MultipartFile file, User user, String uploadPath) throws IOException {
        //Создать пользователя
        user.setActive(true);
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        user.setRoles(Collections.singleton(Role.USER));
        user.setActivationCode(UUID.randomUUID().toString());
        userRepo.save(user);

        if (!StringUtils.isEmpty(user.getEmail())) {
            String message = String.format(
                    "Здравствуй, товарищ!\n" +
                            "Добро пожаловать в ВСоюзе!\n" +
                            "Чтобы продолжить, используй этот ключ:\n" +
                            "http://localhost:8080/activate/%s",
                    user.getActivationCode()
            );

            //mailSender.send(user.getEmail(), "Код Активации", message);
        }

        //Создать дефолтный пользовательский альбом
        Album album = new Album();
        album.setName("Фотокарточки из моего досье");
        album.setHolder(user);

        user.setDefaultAlbum(album);

        albumRepo.save(album);

        if (file != null && file.getOriginalFilename() != null && !file.getOriginalFilename().isEmpty()) {
            //Сохранить файл
            File uploadDir = new File(uploadPath);

            if (!uploadDir.exists()) {
                uploadDir.mkdir();
            }

            String uuidFile = UUID.randomUUID().toString();
            String resultFilename = uuidFile + "." + file.getOriginalFilename();

            file.transferTo(new File(uploadPath + "/" + resultFilename));

            //Создать вложение и сохранить его в дефолтном альбоме и как аватар пользователя
            Photo photo = new Photo();
            photo.setFilename(resultFilename);
            photo.getAlbums().add(album);
            photoRepo.save(photo);

            user.setAvatar(photo);
        }
        user.setRoles(null);
        userRepo.save(user);
    }

    public boolean activateUser(String code) {
        User user = userRepo.findByActivationCode(code);
        if (user == null) {
            return false;
        }

        user.setActivationCode(null);

        userRepo.save(user);

        return true;
    }
}
