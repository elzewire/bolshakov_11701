package ru.kpfu.itis.socialnet.repos;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.kpfu.itis.socialnet.domain.Album;
import ru.kpfu.itis.socialnet.domain.User;

public interface AlbumRepo extends JpaRepository<Album, Long> {
    Album getByHolder(User holder);
    Album getById(Long id);
}
