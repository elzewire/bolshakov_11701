package ru.kpfu.itis.socialnet.domain;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Photo {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private String filename;

    @ManyToMany
    @JoinTable(
            name = "album_photos",
            joinColumns = { @JoinColumn(name = "photo_id") },
            inverseJoinColumns = { @JoinColumn(name = "album_id") }
    )
    private Set<Album> albums = new HashSet<>();

    @ManyToMany
    @JoinTable(
            name = "photo_posts",
            joinColumns = { @JoinColumn(name = "photo_id") },
            inverseJoinColumns = { @JoinColumn(name = "post_id") }
    )
    private Set<Album> posts = new HashSet<>();

    public Photo() {
    }

    public Photo(String filename) {
        this.filename = filename;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public Set<Album> getAlbums() {
        return albums;
    }

    public void setAlbums(Set<Album> albums) {
        this.albums = albums;
    }

    public Set<Album> getPosts() {
        return posts;
    }

    public void setPosts(Set<Album> posts) {
        this.posts = posts;
    }
}
