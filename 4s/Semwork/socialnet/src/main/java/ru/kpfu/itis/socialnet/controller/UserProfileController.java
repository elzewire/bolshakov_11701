package ru.kpfu.itis.socialnet.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.kpfu.itis.socialnet.domain.User;
import ru.kpfu.itis.socialnet.repos.PostRepo;
import ru.kpfu.itis.socialnet.repos.UserRepo;

@Controller
@RequestMapping("/profile")
public class UserProfileController {

    @Autowired
    private UserRepo userRepo;
    @Autowired
    private PostRepo postRepo;

    @GetMapping("{user}")
    public String getUserProfile(@PathVariable User user, @AuthenticationPrincipal User auth_user, Model model) {
        if (auth_user.getId() == user.getId()) {
            return "redirect:/profile";
        }
        model.addAttribute("user", user);
        model.addAttribute("auth_user", auth_user);
        model.addAttribute("photos", auth_user.getDefaultAlbum().getPhotos());
        model.addAttribute("posts", postRepo.getByTargetOrderByDatetimeDesc(user));
        return "profile";
    }

    @GetMapping
    public String getAuthUserProfile(@AuthenticationPrincipal User user, Model model) {
        model.addAttribute("user", user);
        model.addAttribute("auth_user", user);
        model.addAttribute("photos", user.getDefaultAlbum().getPhotos());
        model.addAttribute("posts", postRepo.getByTargetOrderByDatetimeDesc(user));
        model.addAttribute("self", true);
        return "profile";
    }

    @GetMapping("/edit")
    public String edtUser(@AuthenticationPrincipal User user, Model model) {
        model.addAttribute("user", user);
        model.addAttribute("auth_user", user);
        return "edit";
    }

    @PostMapping("/edit")
    public String editUser(@AuthenticationPrincipal User auth_user, @Validated(User.ValidationStepTwo.class) User user,
                           BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            model.mergeAttributes(ControllerHelpers.getErrors(bindingResult));
            model.addAttribute("user", user);
            System.out.println(ControllerHelpers.getErrors(bindingResult));
        } else {
            auth_user.setFirstName(user.getFirstName());
            auth_user.setLastName(user.getLastName());
            auth_user.setBirthDate(user.getBirthDate());
            auth_user.setStatus(user.getStatus());


            userRepo.save(auth_user);
            return "redirect:/profile";
        }

        return "edit";
    }
}
