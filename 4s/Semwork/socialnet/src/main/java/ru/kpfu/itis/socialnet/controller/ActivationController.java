package ru.kpfu.itis.socialnet.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import ru.kpfu.itis.socialnet.service.UserService;

@Controller
public class ActivationController {

    @Autowired
    private UserService userService;

    @GetMapping("activate/{code}")
    public String activate(Model model, @PathVariable String code) {
        boolean isActivated = userService.activateUser(code);

        if (isActivated) {
            model.addAttribute("message", "Почта успешно подтверждена!");
        } else {
            model.addAttribute("error", "Код не действителен!");
        }

        return "login";
    }
}
