import task1.FileCopier;
import task2.FilesCopier;
import task3.FilesCopierThreaded;

import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {

        FileCopier.copy("res/source/source.txt", "res/copied/");

        ArrayList<String> files = new ArrayList<>();
        files.add("res/source/f1.txt");
        files.add("res/source/f2.txt");
        files.add("res/source/f3.txt");
        files.add("res/source/f4.txt");
        FilesCopier.copy(files, "res/copied/task2/");

        FilesCopierThreaded.copy(files, "res/copied/task3/");
    }

}
