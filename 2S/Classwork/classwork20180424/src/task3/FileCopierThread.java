package task3;

import java.io.FileInputStream;
import java.io.FileOutputStream;

public class FileCopierThread implements Runnable {

    private String file;
    private String destination;
    private Thread thread;

    public FileCopierThread(String file, String destination) {
        this.file = file;
        this.destination = destination;
        thread = new Thread(this);
        thread.start();
    }

    public void copy(String file, String destination) {
        try {
            String destFile = destination + "(copy)" + getName(file);
            FileInputStream in = new FileInputStream(file);
            FileOutputStream out = new FileOutputStream(destFile);
            byte [] buffer = new byte[128];
            int data = in.read(buffer);
            while (data != -1) {
                out.write(buffer, 0, data);
                data = in.read(buffer);
            }
            in.close();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String getName(String file) {
        String name = "";
        for (int i = 0; i < file.length(); i++) {
            char c = file.charAt(i);
            if (c == '/') {
                name = "";
            } else {
                name += c;
            }
        }
        return name;
    }

    @Override
    public void run() {
        copy(file, destination);
    }
}
