package task1;

import java.io.*;

public class FileCopier {

    public static void copy(String file, String destination) {
        try {
            String destFile = destination + "(copy)" + getName(file);
            FileInputStream input = new FileInputStream(file);
            FileOutputStream output = new FileOutputStream(destFile);
            byte[] buffer = new byte[64];
            int data = input.read(buffer);
            while (data != -1) {
                output.write(buffer, 0, data);
                data = input.read(buffer);
            }
            input.close();
            output.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static String getName(String file) {
        String name = "";
        for (int i = 0; i < file.length(); i++) {
            char c = file.charAt(i);
            if (c == '/') {
                name = "";
            } else {
                name += c;
            }
        }
        return name;
    }

}
