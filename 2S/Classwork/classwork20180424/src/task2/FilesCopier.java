package task2;

import task1.FileCopier;

import java.util.ArrayList;

public class FilesCopier {

    public static void copy(ArrayList<String> files, String dest) {
        for (String file : files) {
            FileCopier.copy(file, dest);
        }
    }

}
