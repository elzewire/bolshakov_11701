package factory_method;

import factory_method.factories.AMDCPUFactory;
import factory_method.factories.CPUFactory;
import factory_method.factories.IntelCPUFactory;
import factory_method.products.CPU;

public class Main {
    public static void main(String[] args) {
        CPUFactory [] factories = {new AMDCPUFactory(), new IntelCPUFactory()};
        for (int i = 0; i < 5; i++) {
            for (CPUFactory factory : factories) {
                CPU cpu = factory.getNew();
                System.out.print("New " + cpu.toString() + " CPU created: ");
                cpu.work();
            }
        }
    }
}
