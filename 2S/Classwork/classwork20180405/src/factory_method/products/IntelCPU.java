package factory_method.products;

import factory_method.products.CPU;

public class IntelCPU implements CPU {
    @Override
    public void work() {
        System.out.println("Working with multiple threads... unsuccessful...");
    }

    @Override
    public String toString() {
        return "Intel";
    }
}
