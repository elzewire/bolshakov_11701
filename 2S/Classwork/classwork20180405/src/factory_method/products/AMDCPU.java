package factory_method.products;

public class AMDCPU implements CPU {

    @Override
    public void work() {
        System.out.println("Heating... melting...");
    }

    @Override
    public String toString() {
        return "AMD";
    }
}
