package factory_method.products;

public interface CPU {
    void work();
}
