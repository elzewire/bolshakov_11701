package factory_method.factories;

import factory_method.products.AMDCPU;
import factory_method.products.CPU;

public class AMDCPUFactory implements CPUFactory {

    @Override
    public CPU getNew() {
        return new AMDCPU();
    }
}
