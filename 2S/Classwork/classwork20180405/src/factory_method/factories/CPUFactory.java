package factory_method.factories;

import factory_method.products.CPU;

public interface CPUFactory {
    CPU getNew();
}
