package factory_method.factories;

import factory_method.products.CPU;
import factory_method.products.IntelCPU;

public class IntelCPUFactory implements CPUFactory {
    @Override
    public CPU getNew() {
        return new IntelCPU();
    }
}
