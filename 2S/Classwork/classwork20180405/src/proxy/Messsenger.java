package proxy;

public interface Messsenger {
    void authorize();
    void sendMessage();
}
