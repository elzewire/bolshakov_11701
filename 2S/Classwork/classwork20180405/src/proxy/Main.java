package proxy;

public class Main {
    public static void main(String[] args) {
        TelegramMessenger msg = new TelegramMessenger();
        TelegramMessengerProxy proxy = new TelegramMessengerProxy(msg);
        proxy.authorize();
        proxy.sendMessage();
    }
}
