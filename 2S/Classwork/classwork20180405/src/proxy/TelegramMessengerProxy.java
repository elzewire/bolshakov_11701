package proxy;

public class TelegramMessengerProxy implements Messsenger{

    private TelegramMessenger messenger;

    public TelegramMessengerProxy(TelegramMessenger messenger) {
        this.messenger = messenger;
    }

    @Override
    public void authorize() {
        messenger.authorize();
    }

    @Override
    public void sendMessage() {
        messenger.sendMessage();
    }
}
