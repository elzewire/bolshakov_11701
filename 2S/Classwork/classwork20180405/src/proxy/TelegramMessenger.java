package proxy;

public class TelegramMessenger implements Messsenger {

    private boolean authorized;

    @Override
    public void authorize() {
        authorized = true;
    }

    @Override
    public void sendMessage() {
        if (authorized) System.out.println("messaging");
    }
}
