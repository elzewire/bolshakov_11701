package singleton;

public class LevelSingleton {
    private static Level level = null;

    public static Level getLevel() {
        if (level == null) {
            level = new Level();
        }
        return level;
    }
}
