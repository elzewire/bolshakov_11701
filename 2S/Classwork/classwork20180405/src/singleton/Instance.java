package singleton;

public interface Instance {

    Level getLevel();

}
