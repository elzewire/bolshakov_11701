package singleton;

public class Enemy implements Instance {

    private Level level;
    private int x;
    private int y;

    public Enemy(Level level, int x, int y) {
        this.level = level;
        this.x = x;
        this.y = y;
    }

    public Level getLevel() {
        return level;
    }

    @Override
    public String toString() {
        return "Enemy";
    }
}
