package singleton;

public class Player implements Instance {

    private Level level;
    private int x;
    private int y;

    public Player(Level level, int x, int y) {
        this.level = level;
        this.x = x;
        this.y = y;
    }

    public Level getLevel() {
        return level;
    }

    @Override
    public String toString() {
        return "Player";
    }
}
