package singleton;

public class Main {
    public static void main(String[] args) {
        Instance[] instances = new Instance[10];
        LevelSingleton.getLevel().setWidth(1024);
        LevelSingleton.getLevel().setHeight(768);

        for (int i = 1; i < instances.length; i+=2) {
            instances[i] = new Player(LevelSingleton.getLevel(),0, 0);
            instances[i-1] = new Enemy(LevelSingleton.getLevel(),0, 0);
            LevelSingleton.getLevel().addInstance(instances[i]);
            LevelSingleton.getLevel().addInstance(instances[i-1]);
        }

        for (Instance inst : LevelSingleton.getLevel().getInstances()) {
            System.out.print(inst.toString() + ": ");
            System.out.println(inst.getLevel().toString());
        }
    }
}
