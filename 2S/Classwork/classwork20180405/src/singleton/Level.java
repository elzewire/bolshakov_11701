package singleton;

import java.util.LinkedList;
import java.util.List;

public class Level {

    private int width;
    private int height;
    private List<Instance> instances;

    public Level() {
        this.width = 0;
        this.height = 0;
        this.instances = new LinkedList<Instance>();
    }

    public void addInstance(Instance i) {
        instances.add(i);
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public List<Instance> getInstances() {
        return instances;
    }

    @Override
    public String toString() {
        return "This level is common for all instances";
    }
}
