package decorator;

public class Main {
    public static void main(String[] args) {
        JavaDeveloper jd = new JavaDeveloper();
        jd.develop();
        JavaAndPythonDeveloper jpd = new JavaAndPythonDeveloper(jd);
        jpd.develop();
        jpd.developOnPython();
    }
}
