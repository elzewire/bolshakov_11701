package decorator;

public class JavaAndPythonDeveloper implements Developer {

    private Developer developer;

    public JavaAndPythonDeveloper(JavaDeveloper developer) {
        this.developer = developer;
    }

    @Override
    public void develop() {
        developer.develop();
    }

    public void developOnPython() {
        System.out.println("Writing some python code");
    }
}
