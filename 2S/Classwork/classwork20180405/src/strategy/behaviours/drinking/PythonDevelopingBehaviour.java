package strategy.behaviours.drinking;

public class PythonDevelopingBehaviour implements DevelopingBehaviour {
    @Override
    public void develop() {
        System.out.println("Writing some python code");
    }
}
