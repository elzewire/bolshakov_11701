package strategy.behaviours.drinking;

public class JavaDevelopingBehaviour implements DevelopingBehaviour {
    @Override
    public void develop() {
        System.out.println("Writing some java code");
    }
}
