package strategy.behaviours.drinking;

public class DrinkingEspressoBehaviour implements DrinkingCoffeeBehaviour {
    @Override
    public void drinkCoffee() {
        System.out.println("Drinking some nice espresso");
    }
}
