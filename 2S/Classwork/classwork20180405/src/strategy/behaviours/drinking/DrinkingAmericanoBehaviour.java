package strategy.behaviours.drinking;

public class DrinkingAmericanoBehaviour implements DrinkingCoffeeBehaviour {
    @Override
    public void drinkCoffee() {
        System.out.println("Drinking some nice americano");
    }
}
