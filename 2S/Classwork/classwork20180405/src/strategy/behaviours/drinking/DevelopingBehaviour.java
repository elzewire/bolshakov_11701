package strategy.behaviours.drinking;

public interface DevelopingBehaviour {
    void develop();
}
