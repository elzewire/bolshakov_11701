package strategy.behaviours.drinking;

public interface DrinkingCoffeeBehaviour {
    void drinkCoffee();
}
