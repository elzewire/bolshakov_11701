package strategy;

import strategy.behaviours.drinking.DrinkingAmericanoBehaviour;
import strategy.behaviours.drinking.PythonDevelopingBehaviour;

public class PythonDeveloper extends Developer {

    public PythonDeveloper() {
        this.developingBehaviour = new PythonDevelopingBehaviour();
        this.drinkingCoffeeBehaviour = new DrinkingAmericanoBehaviour();
    }
}
