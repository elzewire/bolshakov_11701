package strategy;

import strategy.behaviours.drinking.DrinkingAmericanoBehaviour;
import strategy.behaviours.drinking.JavaDevelopingBehaviour;

public class AnotherJavaDeveloper extends Developer {

    public AnotherJavaDeveloper() {
        this.developingBehaviour = new JavaDevelopingBehaviour();
        this.drinkingCoffeeBehaviour = new DrinkingAmericanoBehaviour();
    }
}
