package strategy;

import strategy.behaviours.drinking.DrinkingEspressoBehaviour;
import strategy.behaviours.drinking.JavaDevelopingBehaviour;

public class JavaDeveloper extends Developer {

    public JavaDeveloper() {
        this.developingBehaviour = new JavaDevelopingBehaviour();
        this.drinkingCoffeeBehaviour = new DrinkingEspressoBehaviour();
    }

}
