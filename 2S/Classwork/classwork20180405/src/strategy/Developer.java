package strategy;

import strategy.behaviours.drinking.DevelopingBehaviour;
import strategy.behaviours.drinking.DrinkingCoffeeBehaviour;

public class Developer {

    protected DevelopingBehaviour developingBehaviour;
    protected DrinkingCoffeeBehaviour drinkingCoffeeBehaviour;

    public void develop() {
        developingBehaviour.develop();
    }

    public void drinkCoffee() {
        drinkingCoffeeBehaviour.drinkCoffee();
    }

}
