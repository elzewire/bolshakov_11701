package strategy;

public class Main {
    public static void main(String[] args) {
        Developer[] developers = new Developer[4];
        for (int i = 3; i < developers.length; i+=4) {
            developers[i-3] = new JavaDeveloper();
            developers[i-2] = new PythonDeveloper();
            developers[i-1] = new AnotherJavaDeveloper();
            developers[i] = new AnotherPythonDeveloper();
        }

        for (Developer developer : developers) {
            developer.develop();
            developer.drinkCoffee();
        }
    }
}
