package strategy;

import strategy.behaviours.drinking.DrinkingEspressoBehaviour;
import strategy.behaviours.drinking.PythonDevelopingBehaviour;

public class AnotherPythonDeveloper extends Developer {
    public AnotherPythonDeveloper() {
        this.developingBehaviour = new PythonDevelopingBehaviour();
        this.drinkingCoffeeBehaviour = new DrinkingEspressoBehaviour();
    }
}
