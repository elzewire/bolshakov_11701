package facade.entities.hardware;

import facade.entities.Powerable;

public class Motherboard extends Powerable {

    private HardDisk bootDisk;
    public CPU cpu;
    private RAM ram;

    public Motherboard(CPU cpu, RAM ram) {
        this.cpu = cpu;
        this.ram = ram;
        this.bootDisk = new HardDisk();
        this.power = false;
    }

    public void switchPower() {
        power = !power;
    }

    public void checkHardware() {
        cpu.check();
        ram.check();
        bootDisk.check();
    }

    public HardDisk getBootDisk() {
        return bootDisk;
    }
}
