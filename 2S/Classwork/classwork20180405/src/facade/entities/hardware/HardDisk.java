package facade.entities.hardware;

import facade.entities.Checkable;
import facade.entities.Powerable;

public class HardDisk extends Powerable implements Checkable {
    private boolean launched;

    public void launchOS() {
        launched = true;
    }

    public boolean getOSStatus() {
        return launched;
    }

    public void switchPower() {
        power = !power;
    }

    public void check() {
        System.out.println("Disk Fine");
    }
}
