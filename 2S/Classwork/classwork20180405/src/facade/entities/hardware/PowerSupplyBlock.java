package facade.entities.hardware;

public class PowerSupplyBlock {
    private Motherboard mb;
    private HardDisk disk;

    public PowerSupplyBlock(Motherboard mb) {
        this.mb = mb;
        this.disk = mb.getBootDisk();
    }

    public void powerMotherboard() {
        mb.switchPower();
    }

    private void powerHardDisk() {
        disk.switchPower();
    }

    public void activatePowerSupply() {
        powerMotherboard();
        powerHardDisk();
    }
}
