package facade.entities;

import facade.entities.hardware.HardDisk;
import facade.entities.hardware.Motherboard;

public class Monitor extends Powerable {

    public void displayHardwareStatus(Motherboard mb) {
        mb.checkHardware();
    }

    public void displayOSStatus(HardDisk disk) {
        System.out.println("OS launched: " + disk.getOSStatus());
    }
}
