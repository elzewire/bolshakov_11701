package facade.entities;

public class Powerable {
    protected boolean power;

    public void switchPower() {
        power = !power;
    }
}
