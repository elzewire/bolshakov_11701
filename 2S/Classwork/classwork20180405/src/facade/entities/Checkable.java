package facade.entities;

public interface Checkable {
    void check();
}
