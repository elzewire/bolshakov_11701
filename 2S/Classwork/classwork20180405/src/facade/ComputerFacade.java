package facade;

import facade.entities.Monitor;
import facade.entities.hardware.*;

public class ComputerFacade {

    private PowerSupplyBlock psb;
    private Motherboard mb;
    private Monitor monitor;

    public ComputerFacade() {
        CPU cpu = new CPU();
        RAM ram = new RAM();
        this.mb = new Motherboard(cpu, ram);
        this.monitor = new Monitor();
        this.psb = new PowerSupplyBlock(mb);
    }

    public void start() {
        turnOn();
        monitor.displayHardwareStatus(mb);
        HardDisk disk = mb.getBootDisk();
        disk.launchOS();
        monitor.displayOSStatus(disk);
    }

    public void turnOn() {
        psb.activatePowerSupply();
        monitor.switchPower();
    }

}
