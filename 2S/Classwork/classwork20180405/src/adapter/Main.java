package adapter;

public class Main {
    public static void main(String[] args) {
        MicrophoneSocket mSocket = new MicrophoneSocket();
        HeadphonesSocket hSocket = new MicrophoneSocketAdapter(mSocket);
        hSocket.plugInHeadphones();
    }
}
