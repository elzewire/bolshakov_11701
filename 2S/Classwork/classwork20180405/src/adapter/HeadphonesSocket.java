package adapter;

public interface HeadphonesSocket {
    void plugInHeadphones();
}
