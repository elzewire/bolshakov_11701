package adapter;

public class MicrophoneSocket {
    public void plugInMicro() {
        System.out.println("New microphone device available");
    }
}
