package adapter;

public class MicrophoneSocketAdapter implements HeadphonesSocket {

    private MicrophoneSocket mSocket;

    public MicrophoneSocketAdapter(MicrophoneSocket mSocket) {
        this.mSocket = mSocket;
    }

    @Override
    public void plugInHeadphones() {
        mSocket.plugInMicro();
    }
}
