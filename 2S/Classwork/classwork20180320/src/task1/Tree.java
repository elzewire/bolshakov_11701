package task1;

import java.util.Queue;
import java.util.Stack;
import java.util.concurrent.ConcurrentLinkedQueue;

public class Tree {
    private Node root;
    private static int num = 1;

    public Tree(int n) {
        root = new Node();
        createTree(root, n);
    }

    //Рекурсинвые в глубину 2 вида
    public void depthPassRecCLR() {
        depthPassRecCLR(root);
        System.out.println();
        num = 1;
    }

    public void depthPassRecLCR() {
        depthPassRecLCR(root);
        System.out.println();
        num = 1;
    }

    public static void depthPassRecLCR(Node node) {
        //Left Center Right
        if (node.getLeft() != null) {
            depthPassRecLCR(node.getLeft());
        }
        System.out.print(node.getValue() + "(" + num++ + ") ");
        if (node.getRight() != null) {
            depthPassRecLCR(node.getRight());
        }
    }

    public static void depthPassRecCLR(Node node) {
        //Center Left Right
        System.out.print(node.getValue() + "(" + num + ") ");
        if (node.getLeft() != null) {
            num++;
            depthPassRecCLR(node.getLeft());
        }
        if (node.getRight() != null) {
            num++;
            depthPassRecCLR(node.getRight());
        }
    }

    //Нерекурсивный в ширину
    public void widthPass() {
        Queue<Node> queue = new ConcurrentLinkedQueue<>();
        Node node;
        queue.offer(root);
        while (!queue.isEmpty()) {
            node = queue.poll();
            System.out.print(node.getValue() + "(" + num + ") ");
            if (node.getLeft() != null) {
                queue.offer(node.getLeft());
            }
            if (node.getRight() != null) {
                queue.offer(node.getRight());
            }
            num++;
        }
        System.out.println();
        num = 1;
    }

    //Нерекурсивный в глубину два вида
    public void depthPassCLR() {
        Stack<Node> stack = new Stack<>();
        Node node;
        stack.add(root);
        while (!stack.isEmpty()) {
            node = stack.pop();
            System.out.print(node.getValue() + "(" + num + ") ");
            if (node.getLeft() != null) {
                stack.add(node.getLeft());
            }
            if (node.getRight() != null) {
                stack.add(node.getRight());
            }
            num++;
        }
        System.out.println();
        num = 1;
    }

    public void depthPassLCR() {
        Stack<Node> stack = new Stack<>();
        Node node;
        stack.add(root);
        while (!stack.isEmpty()) {
            node = stack.pop();
            if (node.getLeft() != null) {
                stack.add(node.getLeft());
            }
            System.out.print(node.getValue() + "(" + num + ") ");
            if (node.getRight() != null) {
                stack.add(node.getRight());
            }
            num++;
        }
        System.out.println();
        num = 1;
    }

    public void print() {
        printTree(root, 0);
    }

    public static void createTree(Node node, int n) {
        node.setValue(n);
        if (n > 1) {
            int nl = n / 2;
            int nr = n - 1 - nl;
            if (nl > 0) {
                node.setLeft(new Node());
                createTree(node.getLeft(), nl);
            }
            if (nr > 0) {
                node.setRight(new Node());
                createTree(node.getRight(), nr);
            }
        }
    }

    public static void printTree(Node node, int h) {
        if (node != null) {
            printTree(node.getRight(), ++h);
            for (int i = 0; i < h; i++) {
                System.out.print("  ");
            }
            System.out.println(node.getValue());
            printTree(node.getLeft(), h);
        }
    }

    public static void main(String[] args) {
        Tree tree = new Tree(15);
        tree.print();
        System.out.println("В глубину:");
        System.out.println("Рекурсивные:");
        System.out.println("Корень-левое-правое");
        tree.depthPassRecCLR();
        System.out.println("Левое-корень-правое");
        tree.depthPassRecLCR();
        System.out.println("Нерекурсивные:");
        System.out.println("Корень-левое-правое");
        tree.depthPassCLR();
        System.out.println("Левое-корень-правое");
        tree.depthPassRecLCR();
        System.out.println("В ширину:");
        tree.widthPass();

    }

}