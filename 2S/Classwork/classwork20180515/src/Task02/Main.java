package Task02;

import Task02.downloaders.FileDownloader;

public class Main {
    public static void main(String[] args) {
        FileDownloader fdl = new FileDownloader("https://en.wikipedia.org/wiki/London", "jpg", "downloads/");
        fdl.start();
    }
}
