package Task01;

public class Producer extends Thread{

    private Product product;

    public Producer(Product product) {
        this.product = product;
    }

    @Override
    public void run() {
        while (true) {
            synchronized (product) {
                while (product.isUsed()) {
                    try {
                        product.wait();
                    } catch(InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                product.produce();
                product.notify();
            }
        }
    }

}
