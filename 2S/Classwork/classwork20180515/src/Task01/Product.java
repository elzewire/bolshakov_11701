package Task01;

public class Product {
    private boolean ready;
    private boolean used;

    public Product() {
        this.ready = false;
        this.used = false;
    }


    public void use() {
        used = true;
        System.out.println("I AM USED");
        used = false;
    }

    public void produce() {
        ready = false;
        System.out.println("I AM PRODUCED");
        ready = true;
    }

    public boolean isUsed() {
        return used;
    }

    public boolean isReady() {
        return ready;
    }
}
