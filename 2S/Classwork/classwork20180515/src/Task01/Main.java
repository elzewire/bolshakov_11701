package Task01;

import Task01.Consumer;
import Task01.Producer;
import Task01.Product;

public class Main {
    public static void main(String[] args) {
        Product product = new Product();
        Producer producer = new Producer(product);
        producer.start();
        Consumer consumer = new Consumer(product);
        consumer.start();
    }
}
