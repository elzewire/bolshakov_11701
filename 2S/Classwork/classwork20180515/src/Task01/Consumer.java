package Task01;

public class Consumer extends  Thread {
    private Product product;

    public Consumer(Product product) {
        this.product = product;
    }


    @Override
    public void run() {
        while (true) {
            synchronized (product) {
                while (!product.isReady()) {
                    try {
                        product.wait();
                    } catch(InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                product.use();
                product.notify();
            }
        }
    }
}
