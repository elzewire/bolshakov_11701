package task1;

import task0.MyStack;

import java.util.Arrays;
import java.util.Random;
import java.util.Stack;

public class Main {

    public static void main(String[] args) {
        MyStack stack = new MyStack();
        Random rnd = new Random();

        int n = 20;
        int [] arr = new int[n];

        for (int i = 0; i < n; i++) {
            arr[i] = rnd.nextInt(40) - 20;
        }

        System.out.println(Arrays.toString(arr));

        //Revert array using stack
        for (int i = 0; i < n; i++) {
            stack.push(arr[i]);
        }

        for (int i = 0; i < n; i++) {
            arr[i] = stack.pop();
        }

        System.out.println(Arrays.toString(arr));

    }

}
