package task0;

import java.util.Arrays;

public class MyStack {

    private int CAPACITY = 100;
    private int n;
    private int [] arr;

    public MyStack() {
        arr = new int[CAPACITY];
        n = 0;
    }

    public boolean isEmpty() { return n == 0;}

    public void push(int x) {
        arr[n] = x;
        n++;

        if (n > (CAPACITY * 2) / 3) {
            CAPACITY *= 2;
            int [] newArr = Arrays.copyOf(arr, CAPACITY);
            this.arr = newArr;
        }
    }

    public int pop() {
        n--;
        return arr[n];
    }
}
