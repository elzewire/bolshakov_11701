package task2;

public class Main {

    public static void main(String[] args) {
        String str = "(())()(()())(())";

        int stack = 0;

        for (int i = 0; i < str.length(); i++) {
            if (str.charAt(i) == '(') {
                stack++;
            } else if (str.charAt(i) == ')') {
                stack--;
            }
        }

        System.out.println(stack);

    }
}
