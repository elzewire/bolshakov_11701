package task3;

import task0.MyStack;

public class Main {

    public static void main(String[] args) {

        MyStack stack = new MyStack();
        String str = "{(({[{{}}]}){[]{}})}";
        char c1 = 0;
        char c2 = 0;

        for (int i = 0; i < str.length(); i++) {
            c1 = str.charAt(i);
            if (!stack.isEmpty()) {
                c2 = (char) stack.pop();
                switch (c1) {
                    case ')':
                        if (c2 != '(') {
                            stack.push(c2);
                            stack.push(c1);
                        }
                        break;
                    case '}':
                        if (c2 != '{') {
                            stack.push(c2);
                            stack.push(c1);
                        }
                        break;
                    case ']':
                        if (c2 != '[') {
                            stack.push(c2);
                            stack.push(c1);
                        }
                        break;
                    default:
                        stack.push(c2);
                        stack.push(c1);
                        break;
                }
            } else {
                stack.push(c1);
            }
        }

        if (!stack.isEmpty()) {
            char c = ' ';
            switch ((char)stack.pop()) {
                case '{':
                    c = '}';
                    break;
                case '[':
                    c = ']';
                    break;
                case '(':
                    c = ')';
                    break;
                case '}':
                    c = '{';
                    break;
                case ']':
                    c = '[';
                    break;
                case ')':
                    c = '(';
                    break;
            }
            System.out.print("Symbol " + c + " expected");
        }
    }

}
