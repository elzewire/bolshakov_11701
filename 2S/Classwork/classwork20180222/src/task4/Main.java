package task4;

import task0.MyStack;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        String sequence = "3 2 1 + 7 5 + + *";
        Scanner sc = new Scanner(sequence);
        MyStack stack = new MyStack();
        while (sc.hasNext()) {
            if (sc.hasNextInt()) {
                stack.push(sc.nextInt());
            } else {
                int first = stack.pop();
                int second = stack.pop();
                String s = sc.next();
                switch (s) {
                    case "+":
                        stack.push(first+second);
                        break;
                    case "*":
                        stack.push(first*second);
                        break;
                    case "-":
                        stack.push(first-second);
                        break;
                    case "/":
                        stack.push(first/second);
                        break;
                }
            }
        }

        System.out.println(stack.pop());
    }

}
