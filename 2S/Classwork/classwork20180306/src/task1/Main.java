package task1;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws FileNotFoundException {

        HashMap<Character, Integer> map = new HashMap<>();
        Scanner sc = new Scanner(new File("resources/input.txt"));

        String str;
        char c;

        while (sc.hasNext()) {
            str = sc.next();
            for (int i = 0; i < str.length(); i++) {
                c = str.charAt(i);
                if (Character.isLetter(c) && Character.isLowerCase(c)) {
                    if (!map.containsKey(c)) {
                        map.put(c, 1);
                    } else {
                        map.put(c, map.get(c) + 1);
                    }
                }
            }
        }

        for (char key :  map.keySet()) {
            System.out.println(key + ": " + map.get(key));
        }

    }

}
