package task2;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws FileNotFoundException {

        HashMap<String, Integer> map = new HashMap<>();
        Scanner sc = new Scanner(new File("resources/input.txt"));

        String str;
        char c;
        String newStr = "";
        while (sc.hasNext()) {
            str = sc.next();
            for (int i = 0; i < str.length(); i++) {
                c = str.charAt(i);
                if (Character.isLetter(c)) {
                    newStr += Character.toLowerCase(c);
                }
            }
            if (!newStr.isEmpty()) {
                if (!map.containsKey(newStr)) {
                    map.put(newStr, 1);
                } else {
                    map.put(newStr, map.get(newStr) + 1);
                }
            }
            newStr = "";
        }

        for (String key : map.keySet()) {
            System.out.println(key + ": " + map.get(key));
        }

    }

}
