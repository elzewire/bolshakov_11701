package example;

import example.Vector2D;

import java.util.Comparator;

public class ComparatorVector2DByX implements Comparator<Vector2D> {

    @Override
    public int compare(Vector2D o1, Vector2D o2) {
        return (int) Math.signum(o1.getX() - o2.getX());
    }
}
