package example;

import example.Vector2D;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Main {

    public static void main(String [] args) {

        List<Vector2D> vectors = new ArrayList<>();

        vectors.add(new Vector2D(1, 1));
        vectors.add(new Vector2D(-4, 3));
        vectors.add(new Vector2D());

        //Collections.sort(vectors, new example.ComparatorVector2DByX());

        /*Collections.sort(vectors, new Comparator<example.Vector2D>() {
            @Override
            public int compare(example.Vector2D o1, example.Vector2D o2) {
                return (int) Math.signum(o1.getX() - o2.getX());
            }
        });*/

        Collections.sort(vectors, (o1, o2) -> {return (int) Math.signum(o1.getX() - o2.getX());});
        System.out.println(vectors);

    }

}
