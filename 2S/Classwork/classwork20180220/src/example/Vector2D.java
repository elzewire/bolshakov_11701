package example;

public class Vector2D implements Comparable<Vector2D>{

    private double x;
    private double y;

    public Vector2D(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public Vector2D() {
        this(0, 0);
    }

    public Vector2D add(Vector2D vector) {
        return new Vector2D(x + vector.getX(), y + vector.getY());
    }

    public Vector2D sub(Vector2D vector) {
        return new Vector2D(x - vector.getX(), y - vector.getY());
    }

    public Vector2D mult(double mult) {
        return new Vector2D(x * mult, y * mult);
    }

    public String toString() {
        return "{" + x + "; " + y + "}";
    }

    public double length() {
        return Math.sqrt(x*x + y*y);
    }

    public double scalarProduct(Vector2D vector) {
        return x * vector.getX() + y * vector.getY();
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    @Override
    public int compareTo(Vector2D other) {
        return (int) Math.signum(this.length() - other.length());
    }
}