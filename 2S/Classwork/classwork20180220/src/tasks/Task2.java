package tasks;

import java.util.*;

import static jdk.nashorn.internal.objects.Global.println;

public class Task2 {

    public static void main(String[] args) {

        List<MyArray> list = new ArrayList<>();

        int n = 5;
        int k = 6;
        int [] arr = new int[n];
        Random rnd = new Random();

        for (int i = 0; i < k; i++) {
            for (int j = 0; j < n; j++) {
                arr[j] = rnd.nextInt(20);
            }
            list.add(new MyArray(arr));
            //System.out.println(Arrays.toString(arr));
        }

        System.out.println(list);

        Collections.sort(list, new Comparator<MyArray>() {
            @Override
            public int compare(MyArray o1, MyArray o2) {
                for (int i = 0; i < o1.getArr().length; i++) {
                    if ((int)Math.signum(o1.getArr()[i] - o2.getArr()[i]) != 0) {
                        return (int)Math.signum(o1.getArr()[i] - o2.getArr()[i]);
                    }
                }

                return 0;
            }
        });

        System.out.print(list);

    }

}
