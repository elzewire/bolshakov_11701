package tasks;

import javafx.concurrent.Task;

import java.util.*;

public class Task3 {

    public static int reverse(int x) {
        int r = 0;

        while (x > 0) {
            r += r * 10 + x % 10;
            x = x / 10;
        }

        return r;
    }

    public static void main(String[] args) {

        List<Integer> ls = new ArrayList<>();

        int n = 12;
        Random rnd = new Random();

        for (int i = 0; i < n; i++) {
            ls.add(rnd.nextInt(100));
        }

        System.out.println(ls);

        Collections.sort(ls, new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                return (int)Math.signum(Task3.reverse(o1) - Task3.reverse(o2));
            }
        });

        System.out.println(ls);
    }
}
