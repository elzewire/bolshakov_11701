package tasks;

import java.util.Arrays;

public class MyArray {
    private int[] array;

    public MyArray(int[] array) {
        this.array = array;
    }

    public int[] getArr() {
        return array;
    }

    @Override
    public String toString() {
        return Arrays.toString(array)+"\n";
    }
}
