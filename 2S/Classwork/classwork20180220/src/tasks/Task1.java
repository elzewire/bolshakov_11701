package tasks;

import java.util.*;

import example.Vector2D;

public class Task1 {

    public static void main(String [] args) {

        List<Vector2D> vectors = new ArrayList<>();

        vectors.add(new Vector2D(1, 1));
        vectors.add(new Vector2D(4, -1));
        vectors.add(new Vector2D(-4, 3));
        vectors.add(new Vector2D());
        vectors.add(new Vector2D(4, 2));

        Collections.sort(vectors, new Comparator<Vector2D>() {
            @Override
            public int compare(Vector2D o1, Vector2D o2) {
                if ((int)Math.signum(o1.getX() - o2.getX()) == 0) {
                    return (int)Math.signum(o1.getY() - o2.getY());
                }
                return (int)Math.signum(o1.getX() - o2.getX());
            }
        });

        System.out.print(vectors);

    }
}
