package MainTask;

import MainTask.downloaders.FileDownloader;

public class Main {
    public static void main(String[] args) {
        FileDownloader fdl = new FileDownloader("https://en.wikipedia.org/wiki/London", "docx", "downloads/");
        fdl.start();
    }
}
