package MainTask.downloaders;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.util.concurrent.Semaphore;

public class Downloader implements Runnable {

    private static Semaphore semaphore = new Semaphore(3, true);

    private String fileURL;
    private String destination;
    private Thread thread;

    public Downloader(String fileURL, String destination) {
        this.fileURL = fileURL;
        this.destination = destination;
        thread = new Thread(this);
        thread.start();
    }

    private String getFilename(String fileURL) {
        int lastSlash = 0;
        char c;
        for (int i = 0; i < fileURL.length(); i++) {
            c = fileURL.charAt(i);
            if (c == '/') {
                lastSlash = i + 1;
            }
        }

        return fileURL.substring(lastSlash);
    }

    private void download() {
        try {
            //
            System.out.println("Download started: " + fileURL);
            //
            URL url = new URL(fileURL);
            String filename = getFilename(fileURL);
            InputStream in = url.openStream();
            FileOutputStream out = new FileOutputStream(new File(destination + filename));
            byte[] buffer = new byte[4096];
            int data = in.read(buffer);
            while (data != -1) {
                out.write(buffer, 0, data);
                data = in.read(buffer);
            }
            in.close();
            out.close();
            //
            System.out.println("Download complete: " + fileURL);
            //
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        try {
            semaphore.acquire();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        download();
        semaphore.release();
    }
}
