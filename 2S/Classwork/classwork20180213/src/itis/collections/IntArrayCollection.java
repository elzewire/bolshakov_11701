package itis.collections;

import java.util.*;

public class IntArrayCollection implements Collection<Integer> {
    private int CAPACITY = 3;
    private int [] arr = new int[CAPACITY];
    private int n = 0;

    @Override
    public int size() {
        return n;
    }

    @Override
    public boolean isEmpty() {
        return n == 0;
    }

    @Override
    public boolean contains(Object o) {
        if (o instanceof Integer) {
            Integer i = (Integer) o;
            for (Integer x: arr) {
                if (x == i) {
                    return true;
                }
            }
            return false;
        } else {
            return false;
        }
    }

    @Override
    public Iterator<Integer> iterator() {
        return null;
    }

    @Override
    public Object[] toArray() {
        return new Object[0];
    }

    @Override
    public <T> T[] toArray(T[] a) {
        return null;
    }

    @Override
    public boolean add(Integer i) {
        arr[n] = i;
        n++;
        if (n > (CAPACITY * 2) / 3) {
            CAPACITY *= 2;
            int [] newArr = Arrays.copyOf(arr, CAPACITY);
            this.arr = newArr;
        }
        return true;
    }

    @Override
    public boolean remove(Object o){
        if (o instanceof Integer) {
            Integer i = (Integer) o;
            int temp = 0;
            for (int j = 0; j < n - 1; j++) {
                if (arr[j] == i) {
                    temp = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = temp;
                }
            }

            if (arr[n - 1] == i) {
                arr[n - 1] = 0;
                n--;
                return true;
            }

            return false;
        } else {
            return false;
        }
    }

    @Override
    public boolean containsAll(Collection<?> col) {
        for (Object o : col) {
            if (!this.contains(o)) {
                return false;
            }
        }
        return true;
    }


    @Override
    public boolean addAll(Collection<? extends Integer> c) {
        boolean pr = false;
        for (Integer i: c) {
            pr = this.add(i) || pr;
        }
        return pr;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        boolean flag = false;
        for (Object o : c) {
            flag = this.remove(o) || flag;
        }
        return flag;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        boolean flag = false;
        for (Object o : this) {
            if (!c.contains(o)) {
                flag = this.remove(o) || flag;
            }
        }
        return flag;
    }

    @Override
    public void clear() {
        n = 0;
    }
}