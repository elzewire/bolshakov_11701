package itis.collections;

import java.util.Collection;
import java.util.Iterator;

public class IntLinkedCollection implements Collection<Integer>  {

    private class Elem {
        Integer value;
        Elem next;
    }

    private Elem head;
    private int n = 0;

    @Override
    public int size() {
        return n;
    }

    @Override
    public boolean isEmpty() {
        return n == 0;
    }

    @Override
    public boolean contains(Object o) {
        if (o instanceof Integer) {
            Integer i = (Integer) o;
            Elem p = head;
            while (p != null) {
                if (p.value == i) {
                    return true;
                }
                p = p.next;
            }
            return false;
        } else {
            return false;
        }
    }

    @Override
    public Iterator<Integer> iterator() {
        return null;
    }

    @Override
    public Object[] toArray() {
        return new Object[0];
    }

    @Override
    public <T> T[] toArray(T[] a) {
        return null;
    }

    @Override
    public boolean add(Integer i) {
        Elem p = new Elem();
        p.value = i;
        p.next = head;
        head = p;
        n++;
        return true;
    }

    @Override
    public boolean remove(Object o) {
        if (o instanceof Integer) {
            Integer i = (Integer) o;
            if (head.value == i) {
                head = head.next;
                return true;
            }
            Elem p = head;

            while (p.next != null) {
                if (p.next.value == i) {
                    p.next = p.next.next;
                    return true;
                }
                p = p.next;
            }
        }
        return false;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        for (Object o : c) {
            if (!this.contains(o)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public boolean addAll(Collection<? extends Integer> c) {
        boolean flag = false;
        for (Integer i : c) {
            flag = this.add(i) || flag;
        }
        return flag;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        boolean flag = false;
        for (Object o : c) {
            flag = this.remove(o) || flag;
        }
        return flag;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        boolean flag = false;
        for (Object o : this) {
            if (!c.contains(o)) {
                flag = this.remove(o) || flag;
            }
        }
        return flag;
    }

    @Override
    public void clear() {

    }
}