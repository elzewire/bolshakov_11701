package entities;

import java.util.ArrayList;
import java.util.List;

public class Continent {

    private int id;
    private String name;
    private List<Country> countries;

    public Continent(int id, String name) {
        this.id = id;
        this.name = name;
        this.countries = new ArrayList<>();
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Country> getCountries() {
        return countries;
    }
}
