package entities;

public class WordData {

    private String word;
    private int n;

    public WordData(String word, int n) {
        this.word = word;
        this.n = n;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public int getN() {
        return n;
    }

    public void setN(int n) {
        this.n = n;
    }

}
