package entities;

public class CarData {

    private double MPG;
    private int cylinders;
    private double edispl;
    private int horsepower;
    private int weight;
    private double accelerate;
    private int year;

    public CarData(double MPG, int cylinders, double edispl, int horsepower, int weight, double accelerate, int year) {
        this.MPG = MPG;
        this.cylinders = cylinders;
        this.edispl = edispl;
        this.horsepower = horsepower;
        this.weight = weight;
        this.accelerate = accelerate;
        this.year = year;
    }

    public double getMPG() {
        return MPG;
    }

    public void setMPG(int MPG) {
        this.MPG = MPG;
    }

    public int getCylinders() {
        return cylinders;
    }

    public void setCylinders(int cylinders) {
        this.cylinders = cylinders;
    }

    public double getEdispl() {
        return edispl;
    }

    public void setEdispl(int edispl) {
        this.edispl = edispl;
    }

    public int getHorsepower() {
        return horsepower;
    }

    public void setHorsepower(int horsepower) {
        this.horsepower = horsepower;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public double getAccelerate() {
        return accelerate;
    }

    public void setAccelerate(double accelerate) {
        this.accelerate = accelerate;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }
}
