package entities;

import java.util.ArrayList;
import java.util.List;

public class Country {

    private int id;
    private String name;
    private Continent continent;
    private List<CarMaker> makers;

    public Country(int id, String name, Continent continent) {
        this.id = id;
        this.name = name;
        this.continent = continent;
        this.makers = new ArrayList<>();
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Continent getContinent() {
        return continent;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setContinent(Continent continent) {
        this.continent = continent;
    }

    public List<CarMaker> getMakers() {
        return makers;
    }

}
