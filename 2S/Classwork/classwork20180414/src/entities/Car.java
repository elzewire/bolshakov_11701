package entities;

public class Car {

    private int id;
    private Model model;
    private String name;
    private CarData data;

    public Car(int id, Model model, String name) {
        this.id = id;
        this.model = model;
        this.name = name;
        this.data = null;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Model getModel() {
        return model;
    }

    public void setModel(Model model) {
        this.model = model;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public CarData getData() {
        return data;
    }

    public void setData(CarData data) {
        this.data = data;
    }
}
