package entities;

import java.util.ArrayList;
import java.util.List;

public class Model {

    private int id;
    private CarMaker maker;
    private String name;
    private List<Car> cars;

    public Model(int id, CarMaker maker, String name) {
        this.id = id;
        this.maker = maker;
        this.name = name;
        this.cars = new ArrayList<>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public CarMaker getMaker() {
        return maker;
    }

    public void setMaker(CarMaker maker) {
        this.maker = maker;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Car> getCars() {
        return cars;
    }
}
