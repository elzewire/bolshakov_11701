import collections_singleton.ModelsCollectionSingleton;

import java.util.Map;
import java.util.OptionalDouble;
import java.util.stream.Collectors;

public class Task05b {

    public static Map<String, Double> getAverageHPForModels() {

        return ModelsCollectionSingleton.getModelsCollection()
                .stream()
                .collect(Collectors.toMap(md -> md.getName(), md ->
                        md.getCars()
                                .stream()
                                .mapToInt(car -> car.getData().getHorsepower())
                                .average()
                                .orElse(0)
                ));

    }

}
