import collections_singleton.CollectionsInitializer;

public class Main {

    public static void main(String[] args) {

        CollectionsInitializer.init();

        //Task 0
        System.out.println("Task 0:");
        System.out.println(Task00.getWordWithMaxUsageLoopStyle());
        System.out.println(Task00.getWordWithMaxUsageStreamAPIStyle());
        System.out.println();

        //Task 1
        System.out.println("Task 1:");
        System.out.println(Task01.getContinentWithMaxCountriesClassic());
        System.out.println(Task01.getWordWithMaxUsageStreamAPIStyle());
        System.out.println();

        //Task 2
        System.out.println("Task 2:");
        System.out.println(Task02.getAllMakersFromContinentAPIStyle("\'europe\'"));

        //Task 3
        System.out.println("Task 3:");
        System.out.println(Task03.getModelsAmountForCountries().toString());

        //Task 4
        System.out.println("Task 4:");
        System.out.println(Task04.getMakersProducingCarsFromYear());

        //Task 5
        System.out.println("Task 5:");
        System.out.println("a)");
        System.out.println(Task05a.getAverageHPForMakers().toString());
        System.out.println("b)");
        System.out.println(Task05b.getAverageHPForModels().toString());

    }

}
