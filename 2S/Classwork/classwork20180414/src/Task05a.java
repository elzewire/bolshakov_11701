import collections_singleton.CarMakersCollectionSingleton;
import collections_singleton.ModelsCollectionSingleton;
import entities.CarMaker;

import java.util.Map;
import java.util.stream.Collectors;

public class Task05a {

    public static Map<String, Double> getAverageHPForMakers() {

        return CarMakersCollectionSingleton.getCarMakersCollection()
                .stream()
                .collect(Collectors.toMap(cm -> cm.getFullName(), cm ->
                        cm.getModels()
                                .stream()
                                .mapToDouble(model-> model.getCars()
                                        .stream()
                                        .mapToInt(car -> car.getData().getHorsepower())
                                        .average()
                                        .orElse(0)

                                )
                                .average()
                                .orElse(0)
                ));

    }

}
