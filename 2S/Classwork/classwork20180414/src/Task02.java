import collections_singleton.*;

import entities.CarMaker;
import entities.Country;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Task02 {

    public static String getAllMakersFromContinentAPIStyle(String continentName) {
        return CarMakersCollectionSingleton.getCarMakersCollection()
                .stream()
                .filter(cm -> cm.getCountry().getContinent().getName().equals(continentName))
                .collect(Collectors.toList())
                .toString();
    }

}
