package collections_singleton;

import entities.*;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class CarsCollectionSingleton {

    private static List<Car> carsCollection;
    private static String filename = "res/car-names.csv";
    private static String datafile = "res/cars-data.csv";

    public static List<Car> getCarsCollection() {
        if (carsCollection == null) {
            carsCollection = new ArrayList<>();
            try {
                BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(filename)));
                String line = br.readLine();
                line = br.readLine();
                while (line != null) {
                    String [] attrs = line.split(",");
                    Model model = getModelByName(attrs[1]);
                    Car car = new Car(Integer.parseInt(attrs[0]), model, attrs[2]);
                    carsCollection.add(car);
                    model.getCars().add(car);
                    line = br.readLine();
                }

                //Add data to the cars
                br = new BufferedReader(new InputStreamReader(new FileInputStream(datafile)));
                line = br.readLine();
                line = br.readLine();
                while (line != null) {
                    String [] attrs = line.split(",");
                    Car car = getCarByID(Integer.parseInt(attrs[0]));
                    car.setData(new CarData(
                            Double.parseDouble(attrs[1]), Integer.parseInt(attrs[2]),
                            Double.parseDouble(attrs[3]), Integer.parseInt(attrs[4]),
                            Integer.parseInt(attrs[5]), Double.parseDouble(attrs[6]),
                            Integer.parseInt(attrs[7])
                    ));
                    line = br.readLine();
                }
            } catch (java.io.IOException e) {
                e.printStackTrace();
            }
        }
        return carsCollection;
    }

    public static Model getModelByName(String name) {

        List<Model> list = ModelsCollectionSingleton.getModelsCollection();
        for (Model model : list) {
            if (model.getName().equals(name)) {
                return model;
            }
        }

        return null;
    }

    public static Car getCarByID(int id) {

        List<Car> list = CarsCollectionSingleton.getCarsCollection();
        for (Car car : list) {
            if (car.getId() == id) {
                return car;
            }
        }

        return null;
    }
}
