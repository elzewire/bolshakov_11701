package collections_singleton;

import entities.Continent;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class ContinentsCollectionSingleton {

    private static List<Continent> continentsCollection;
    private static String filename = "res/continents.csv";

    public static List<Continent> getContinentsCollection() {
        if (continentsCollection == null) {
            continentsCollection = new ArrayList<>();
            try {
                BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(filename)));
                String line = br.readLine();
                line = br.readLine();
                while (line != null) {
                    String [] attrs = line.split(",");
                    continentsCollection.add(new Continent(Integer.parseInt(attrs[0]), attrs[1]));
                    line = br.readLine();
                }
            } catch (java.io.IOException e) {
                e.printStackTrace();
            }
        }
        return continentsCollection;
    }
}
