package collections_singleton;

import entities.CarMaker;
import entities.Continent;
import entities.Country;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class CarMakersCollectionSingleton {

    private static List<CarMaker> carMakersCollection;
    private static String filename = "res/car-makers.csv";

    public static List<CarMaker> getCarMakersCollection() {
        if (carMakersCollection == null) {
            carMakersCollection = new ArrayList<>();
            try {
                BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(filename)));
                String line = br.readLine();
                line = br.readLine();
                while (line != null) {
                    String [] attrs = line.split(",");
                    Country country = getCountryByID(Integer.parseInt(attrs[3]));
                    CarMaker maker = new CarMaker(Integer.parseInt(attrs[0]), attrs[1], attrs[2], country);
                    carMakersCollection.add(maker);
                    country.getMakers().add(maker);
                    line = br.readLine();
                }
            } catch (java.io.IOException e) {
                e.printStackTrace();
            }
        }
        return carMakersCollection;
    }

    public static Country getCountryByID(int id) {

        List<Country> list = CountriesCollectionSingleton.getCountriesCollection();
        for (Country cntry : list) {
            if (cntry.getId() == id) {
                return cntry;
            }
        }

        return null;
    }
}
