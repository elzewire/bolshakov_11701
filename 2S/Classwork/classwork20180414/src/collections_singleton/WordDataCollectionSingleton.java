package collections_singleton;

import entities.WordData;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class WordDataCollectionSingleton {

    private static List<WordData> wordDataCollection;
    private static String filename = "res/en_v1.dic";

    public static List<WordData> getWordDataCollection() {
        if (wordDataCollection == null) {
            wordDataCollection = new ArrayList<>();
            try {
                BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(filename)));
                String line = br.readLine();
                while (line != null) {
                    String [] attrs = line.split(" ");
                    wordDataCollection.add(new WordData(attrs[0], Integer.parseInt(attrs[1])));
                    line = br.readLine();
                }
            } catch (java.io.IOException e) {
                e.printStackTrace();
            }
        }
        return wordDataCollection;
    }
}
