package collections_singleton;

import entities.CarMaker;
import entities.Model;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class ModelsCollectionSingleton {

    private static List<Model> modelsCollection;
    private static String filename = "res/model-list.csv";

    public static List<Model> getModelsCollection() {
        if (modelsCollection == null) {
            modelsCollection = new ArrayList<>();
            try {
                BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(filename)));
                String line = br.readLine();
                line = br.readLine();
                while (line != null) {
                    String [] attrs = line.split(",");
                    CarMaker maker = getMakerByID(Integer.parseInt(attrs[1]));
                    Model model = new Model(Integer.parseInt(attrs[0]), maker, attrs[2]);
                    modelsCollection.add(model);
                    maker.getModels().add(model);
                    line = br.readLine();
                }
            } catch (java.io.IOException e) {
                e.printStackTrace();
            }
        }
        return modelsCollection;
    }

    public static CarMaker getMakerByID(int id) {

        List<CarMaker> list = CarMakersCollectionSingleton.getCarMakersCollection();
        for (CarMaker carMaker : list) {
            if (carMaker.getId() == id) {
                return carMaker;
            }
        }

        return null;
    }
}
