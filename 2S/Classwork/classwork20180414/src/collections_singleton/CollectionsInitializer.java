package collections_singleton;

public class CollectionsInitializer {

    public static void init() {
        ContinentsCollectionSingleton.getContinentsCollection();
        CountriesCollectionSingleton.getCountriesCollection();
        CarMakersCollectionSingleton.getCarMakersCollection();
        ModelsCollectionSingleton.getModelsCollection();
        CarsCollectionSingleton.getCarsCollection();
    }

}
