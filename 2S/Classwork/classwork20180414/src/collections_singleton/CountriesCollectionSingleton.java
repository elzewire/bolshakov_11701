package collections_singleton;

import entities.Continent;
import entities.Country;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class CountriesCollectionSingleton {

    private static List<Country> countriesCollection;
    private static String filename = "res/countries.csv";

    public static List<Country> getCountriesCollection() {
        if (countriesCollection == null) {
            countriesCollection = new ArrayList<>();
            try {
                BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(filename)));
                String line = br.readLine();
                line = br.readLine();
                while (line != null) {
                    String [] attrs = line.split(",");
                    Continent continent = getContinentByID(Integer.parseInt(attrs[2]));
                    Country country = new Country(Integer.parseInt(attrs[0]), attrs[1], continent);
                    countriesCollection.add(country);
                    continent.getCountries().add(country);
                    line = br.readLine();
                }
            } catch (java.io.IOException e) {
                e.printStackTrace();
            }
        }
        return countriesCollection;
    }

    public static Continent getContinentByID(int id) {

        List<Continent> list = ContinentsCollectionSingleton.getContinentsCollection();
        for (Continent cntnt : list) {
            if (cntnt.getId() == id) {
                return cntnt;
            }
        }

        return null;
    }
}
