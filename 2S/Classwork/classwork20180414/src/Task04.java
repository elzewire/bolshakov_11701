import collections_singleton.*;
import entities.CarMaker;
import entities.Model;

import java.util.stream.Collectors;

public class Task04 {

    public static String getMakersProducingCarsFromYear() {
        return CarMakersCollectionSingleton.getCarMakersCollection()
                .stream()
                .filter(cm ->
                        cm.getModels().stream().anyMatch(model ->
                                model.getCars().stream().anyMatch(car -> car.getData().getYear() < 1973)))
                .collect(Collectors.toList())
                .toString();
    }

}
