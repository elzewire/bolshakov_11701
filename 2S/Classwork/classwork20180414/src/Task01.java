import collections_singleton.ContinentsCollectionSingleton;
import collections_singleton.CountriesCollectionSingleton;
import collections_singleton.WordDataCollectionSingleton;
import entities.Continent;
import entities.Country;
import entities.WordData;

import java.util.*;

public class Task01 {

    public static String getContinentWithMaxCountriesClassic() {
        List<Continent> cntnts = ContinentsCollectionSingleton.getContinentsCollection();
        List<Country> cntrs = CountriesCollectionSingleton.getCountriesCollection();
        if (cntnts.size() == 0) {
            return null;
        }

        int max = getAmountOfCountires(cntnts.get(0));
        int amount;
        String name = cntnts.get(0).getName();

        for (Continent cntnt : cntnts) {
            amount = getAmountOfCountires(cntnt);
            if (amount > max) {
                max = amount;
                name = cntnt.getName();
            }
        }
        return name;
    }

    public static String getWordWithMaxUsageStreamAPIStyle() {
        return ContinentsCollectionSingleton.getContinentsCollection()
                .stream()
                .reduce((cntnt1, cntnt2) -> getAmountOfCountires(cntnt1) > getAmountOfCountires(cntnt2) ? cntnt1 : cntnt2)
                .orElse(new Continent(0, ""))
                .getName();
    }

    public static int getAmountOfCountires(Continent cntnt) {
        List<Country> cntrs = CountriesCollectionSingleton.getCountriesCollection();
        int n = 0;
        for (Country cntry : cntrs) {
            if (cntry.getContinent() == cntnt) {
                n++;
            }
        }
        return n;
    }

}
