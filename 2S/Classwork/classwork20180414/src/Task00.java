import collections_singleton.WordDataCollectionSingleton;
import entities.WordData;

import java.util.List;

public class Task00 {

    public static String getWordWithMaxUsageLoopStyle() {
        List<WordData> wdc = WordDataCollectionSingleton.getWordDataCollection();
        if (wdc.size() == 0) {
            return null;
        }
        int max = wdc.get(0).getN();
        String word = wdc.get(0).getWord();
        for (WordData wordData : wdc) {
            if (wordData.getN() > max) {
                max = wordData.getN();
                word = wordData.getWord();
            }
        }
        return word;
    }

    public static String getWordWithMaxUsageStreamAPIStyle() {
        return WordDataCollectionSingleton.getWordDataCollection()
            .stream()
            .reduce((wd1, wd2) -> wd1.getN() > wd2.getN() ? wd1 : wd2)
            .orElse(new WordData("", 0))
            .getWord();
    }

}
