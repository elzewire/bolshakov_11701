import collections_singleton.*;
import entities.Car;
import entities.CarMaker;
import entities.Country;
import entities.Model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Task03 {

    public static Map<String, Integer> getModelsAmountForCountries() {

        return CountriesCollectionSingleton.getCountriesCollection()
                .stream()
                .collect(Collectors.toMap(country -> country.getName(), country ->
                        country.getMakers().stream().mapToInt(maker -> maker.getModels().size()).sum()
                ));

    }

}
