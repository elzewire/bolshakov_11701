package tests;

import collections_singleton.ContinentsCollectionSingleton;
import entities.Continent;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

public class ContinentsTest {
    @Test
    public void testCorrectLength() {
        List<Continent> wdc = ContinentsCollectionSingleton.getContinentsCollection();
        Assert.assertEquals(5, wdc.size());
    }
}
