package tests;

import collections_singleton.CountriesCollectionSingleton;
import entities.Country;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

public class CountriesTest {
    @Test
    public void testCorrectLength() {
        List<Country> wdc = CountriesCollectionSingleton.getCountriesCollection();
        Assert.assertEquals(15, wdc.size());
    }
}
