package task1;

public class MyLinkedQueue {

    private class Item {
        Integer value;
        Item next;
    }

    private Item head;
    private Item tail;
    private int n;

    public MyLinkedQueue() {
        n = 0;
        head = null;
        tail = null;
    }

    public void offer(int x) {
        Item p = new Item();
        p.value = x;
        p.next = null;
        if (!this.isEmpty()) {
            tail.next = p;
            tail = p;
        } else {
            tail = p;
            head = p;
        }
        n++;
    }

    public int pull() {
        int x = head.value;
        head = head.next;
        n--;
        return x;
    }

    public int peek() {
        return head.value;
    }

    public boolean isEmpty() {
        return n == 0;
    }

}
