package task2;

public class MyStackedQueue {

    private MyStack head;
    private MyStack tail;

    public MyStackedQueue() {
        head = new MyStack();
        tail = new MyStack();
    }

    public void offer(int x) {
        if (head.isEmpty()) {
            head.push(x);
        } else {
            tail.push(x);
        }
    }

    public int pull() {
        if (!head.isEmpty()) {
            return head.pop();
        } else {
            transfer();
            return head.pop();
        }
    }

    public int peek() {
        if (!head.isEmpty()) {
            return head.peek();
        } else {
            transfer();
            return head.peek();
        }
    }

    public void transfer() {
        while (!tail.isEmpty()) {
            head.push(tail.pop());
        }
    }

    public boolean isEmpty() {
        return head.isEmpty();
    }

}

