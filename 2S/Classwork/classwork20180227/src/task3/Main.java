package task3;

import task1.MyLinkedQueue;

public class Main {
    public static void main(String[] args) {
        MyLinkedQueue q1 = new MyLinkedQueue();
        MyLinkedQueue q2 = new MyLinkedQueue();
        MyLinkedQueue q3 = new MyLinkedQueue();

        q1.offer(2);
        q2.offer(3);
        q3.offer(5);
        int num = 0;
        int N = 100;
        while (num < N) {
            MyLinkedQueue min = min(q1, min(q2, q3));
            num = min.pull();
            System.out.print(num + " ");
            if (min == q1) {
                q1.offer(num * 2);
                q2.offer(num * 3);
                q3.offer(num * 5);
            } else if (min == q2) {
                q2.offer(num * 3);
                q3.offer(num * 5);
            } else if (min == q3) {
                q3.offer(num * 5);
            }
        }
    }

    private static MyLinkedQueue min(MyLinkedQueue q1, MyLinkedQueue q2) {
        if (q2.peek() < q1.peek()) {
            return q2;
        }
        return q1;
    }

}
