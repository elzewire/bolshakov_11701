public class Human implements Player {
    @Override
    public void run() {
        System.out.println("Running");
    }

    @Override
    public void talk() {
        System.out.println("Talking");
    }

    @Override
    public void attack() {
        System.out.println("Attacking");
    }
}
