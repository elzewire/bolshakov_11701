/**
 * Practice Task №9
 */

public class Main {
    public static void main(String[] args) {
        //Human Player
        Player player = new Human();
        System.out.println(player.getClass().getName() + ": ");
        player.run();
        player.talk();
        player.attack();

        System.out.println();

        //Avatar Player
        player = new Avatar(player);
        System.out.println(player.getClass().getName() + ": ");
        player.run();
        player.talk();
        player.attack();

    }
}
