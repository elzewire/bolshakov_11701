public interface Player {
    void run();
    void talk();
    void attack();
}
