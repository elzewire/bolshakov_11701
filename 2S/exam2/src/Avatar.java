public class Avatar implements Player {

    private Player player;

    Avatar(Player player) {
        this.player = player;
    }

    @Override
    public void run() {
        player.run();
    }

    @Override
    public void talk() {
        player.talk();
    }

    @Override
    public void attack() {
        player.attack();
    }
}
