package graphcode;

import com.sun.org.apache.xpath.internal.SourceTree;
import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import test.GraphCodeTest;

import java.util.ArrayList;
import java.util.Arrays;

public class GraphCode {

    private int edges;
    private Edge head;
    private int [] vertices;

    public GraphCode(int [][] mi) {
        vertices = new int[mi.length];
        edges = mi[0].length;

        int flag = 0;
        Edge p;
        head = null;

        for (int i = 0; i < edges; i++) {
            p = new Edge();
            p.next = head;
            head = p;
            for (int j = 0; j < vertices.length; j++) {
                if (mi[j][i] == -1) {
                    p.endVert = j + 1;
                    vertices[j]++;
                    flag++;
                } else if (mi[j][i] == 1) {
                    p.startVert = j + 1;
                    vertices[j]++;
                    flag++;
                }

                if (flag == 2) {
                    flag = 0;
                    break;
                }
            }
        }
    }

    public GraphCode() {
        edges = 0;
        vertices = new int[10];
        head = null;
    }

    public int [][] getMI() {
        int k = vertices.length - 1;
        while (k > 0 && vertices[k] == 0) {
            k--;
        }

        int [][] mi = new int[k + 1][edges];
        Edge newHead = head;

        for (int i = 0; i < edges; i++) {
            mi[newHead.startVert - 1][i] = 1;
            mi[newHead.endVert - 1][i] = -1;
            newHead = newHead.next;
        }
        return mi;
    }

    public void insert(int i, int j) {
        Edge p = new Edge();
        p.next = head;
        head = p;
        p.startVert = i;
        p.endVert = j;
        if (i > vertices.length || j > vertices.length) {
            int [] newVertices = Arrays.copyOf(vertices, 2 * Math.max(i, j));
            vertices = newVertices;
        }
        vertices[i - 1]++;
        vertices[j - 1]++;
        edges++;
    }

    public void delete(int i, int j) {
        boolean deleted = false;
        Edge newHead = head;
        Edge previous = null;
        while (newHead != null && !deleted) {
            if (newHead.startVert == i && newHead.endVert == j) {
                if (previous != null) {
                    previous.next = newHead.next;
                } else {
                    head = newHead.next;
                }
                vertices[i - 1]--;
                vertices[j - 1]--;
                deleted = true;
                edges--;
            }
            previous = newHead;
            newHead = newHead.next;
        }
    }

    public GraphCode getEdgesWithNode(int i) {
        GraphCode graphCode = new GraphCode();
        Edge newHead = head;
        while (newHead != null) {
            if (newHead.startVert == i || newHead.endVert == i) {
                graphCode.insert(newHead.startVert, newHead.endVert);
            }
            newHead = newHead.next;
        }

        return graphCode;
    }

    public void modify(int i) {
        Edge newHead = head;
        Edge previous = null;
        while (newHead != null) {
            if (newHead.startVert == i || newHead.endVert == i) {
                if (previous != null) {
                    previous.next = newHead.next;
                } else {
                    head = newHead.next;
                }
                edges--;
            }
            previous = newHead;
            newHead = newHead.next;
        }
        vertices[i - 1] = 0;
    }

    public ArrayList<Integer> show(int m) {
        ArrayList<Integer> arr = new ArrayList<>();
        for (int i = 0; i < vertices.length; i++) {
            if (vertices[i] > m) {
                arr.add(i + 1);
            }
        }
        return arr;
    }

    //For unit tests
    public int getEdges() {
        return edges;
    }

    public boolean contains(int i, int j) {
        Edge newHead = head;
        while (newHead != null) {
            if (newHead.startVert == i && newHead.endVert == j) {
                return true;
            }
            newHead = newHead.next;
        }
        return false;
    }

    @Override
    public String toString() {
        Edge newHead = head;
        String str = "";
        while (newHead != null) {
            str += ("(" + newHead.startVert + ", " + newHead.endVert + "); ");
            newHead = newHead.next;
        }
        return str;
    }

ю
    //Testing
    public static void main(String[] args) {
        JUnitCore runner = new JUnitCore();
        Result res = runner.run(GraphCodeTest.class);
        System.out.println("Tests: " + res.getRunCount());
        System.out.println("Failed: " + res.getFailureCount());
        System.out.println("Success: " + res.wasSuccessful());
        System.out.println();

        GraphCode gc = new GraphCode();

        //1
        System.out.println("Добавление рёбер \n(4, 3), (2, 3), (1, 4), (3, 1):");
        gc.insert(4,3);
        gc.insert(2,3);
        gc.insert(1,4);
        gc.insert(3,1);
        int [][] mi = gc.getMI();
        for (int i = 0; i < mi.length; i++) {
            System.out.println(Arrays.toString(mi[i]));
        }
        System.out.println(gc);
        System.out.println();

        //2
        System.out.println("Удаление вершины 2:");
        gc.modify(2);
        mi = gc.getMI();
        for (int i = 0; i < mi.length; i++) {
            System.out.println(Arrays.toString(mi[i]));
        }
        System.out.println(gc);
        System.out.println();

        //3
        System.out.println("Удаление ребра (4, 3):");
        gc.delete(4, 3);
        mi = gc.getMI();
        for (int i = 0; i < mi.length; i++) {
            System.out.println(Arrays.toString(mi[i]));
        }
        System.out.println(gc);
        System.out.println();

        //4
        System.out.println("Создание списка ребер по матрице инцидентности:");
        int [][] arr = {
                {-1, 1, 0, 1, 0, 0, 0},
                {1, 0, -1, 0, 0, 1, 0},
                {0, 0, 0, -1, 1, 0, 0},
                {0, -1, 1, 0, -1, 0, -1},
                {0, 0, 0, 0, 0, -1, 1}
        };
        gc = new GraphCode(arr);
        mi = gc.getMI();
        for (int i = 0; i < mi.length; i++) {
            System.out.println(Arrays.toString(mi[i]));
        }
        System.out.println(gc);
        System.out.println();

        //5
        System.out.println("Вывод списка вершин со степенью > m");
        System.out.println(gc.show(2));
    }
}
