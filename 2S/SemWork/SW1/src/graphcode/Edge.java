package graphcode;

public class Edge {
    public Edge next;
    public int startVert;
    public int endVert;

    public Edge() {
        next = null;
        startVert = 0;
        endVert = 0;
    }
}
