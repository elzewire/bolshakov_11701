package test;

import graphcode.GraphCode;
import org.junit.Assert;
import org.junit.Test;
import java.util.ArrayList;

public class GraphCodeTest {

    @Test
    public void newGraphCodeShouldHaveAllTheEdges() {
        int [][] arr = {
                {0, 1, 0, 0, -1, 1},
                {-1, 0, 1, -1, 1, 0},
                {1, -1, -1, 1, 0, -1},
        };
        GraphCode gc = new GraphCode(arr);
        Assert.assertEquals(6, gc.getEdges());
    }

    @Test
    public void insertShouldAddNewEdge() {
        GraphCode gc = new GraphCode();
        gc.insert(4, 3);
        Assert.assertTrue(gc.contains(4, 3));
    }

    @Test
    public void insertShouldIncreaseAmountOfEdges() {
        GraphCode gc = new GraphCode();
        gc.insert(4, 3);
        Assert.assertEquals(1, gc.getEdges());
    }

    @Test
    public void deleteShouldDeleteEdge() {
        GraphCode gc = new GraphCode();
        gc.insert(4,3);
        gc.delete(4, 3);
        Assert.assertTrue(!gc.contains(4, 3));
    }

    @Test
    public void deleteShouldDecreaseAmountOfEdges() {
        GraphCode gc = new GraphCode();
        gc.insert(4,3);
        gc.delete(4, 3);
        Assert.assertEquals(0, gc.getEdges());
    }

    @Test
    public void getMIShouldReturnMatrixWithSameHeight() {
        int [][] arr = {
                {0, 1, 0, 0, -1, 1},
                {-1, 0, 1, -1, 1, 0},
                {1, -1, -1, 1, 0, -1},
        };
        GraphCode gc = new GraphCode(arr);
        Assert.assertEquals(arr.length, gc.getMI().length);
    }

    @Test
    public void getMIShouldReturnMatrixWithSameWidth() {
        int [][] arr = {
                {0, 1, 0, 0, -1, 1},
                {-1, 0, 1, -1, 1, 0},
                {1, -1, -1, 1, 0, -1},
        };
        GraphCode gc = new GraphCode(arr);
        Assert.assertEquals(arr[0].length, gc.getMI()[0].length);
    }

    @Test
    public void getEdgesWithNodeShouldReturnRightAmount() {
        int [][] arr = {
                {0, 1, 0, 0, -1, 1},
                {-1, 0, 1, -1, 1, 0},
                {1, -1, -1, 1, 0, -1},
        };
        GraphCode gc = new GraphCode(arr);
        Assert.assertEquals(5, gc.getEdgesWithNode(3).getEdges());
    }

    @Test
    public void modifyShouldDeleteEdges() {
        int [][] arr = {
                {0, 1, 0, 0},
                {-1, 0, 1, -1},
                {1, -1, -1, 1},
        };
        GraphCode gc = new GraphCode(arr);
        gc.modify(1);
        Assert.assertTrue(!gc.contains(1,3));
    }

    @Test
    public void modifyShouldDecreaseAmountOfEdges() {
        int [][] arr = {
                {0, 1, 0, 0, -1, 1},
                {-1, 0, 1, -1, 1, 0},
                {1, -1, -1, 1, 0, -1},
        };
        GraphCode gc = new GraphCode(arr);
        gc.modify(2);
        Assert.assertEquals(2, gc.getEdges());
    }

    @Test
    public void showShouldReturnRightAmountOfVertices() {
        int [][] arr = {
                {-1, 1, 0, 1, 0, 0, 0},
                {1, 0, -1, 0, 0, 1, 0},
                {0, 0, 0, -1, 1, 0, 0},
                {0, -1, 1, 0, -1, 0, -1},
                {0, 0, 0, 0, 0, -1, 1}
        };
        GraphCode gc = new GraphCode(arr);
        ArrayList<Integer> list = gc.show(2);
        Assert.assertEquals(3, list.size());
    }

    @Test
    public void getMIShouldChangeAfterDeletingVertex() {
        GraphCode gc = new GraphCode();
        gc.insert(4,3);
        gc.insert(2,3);
        gc.insert(1,4);
        gc.insert(3,1);
        gc.modify(4);
        Assert.assertEquals(3, gc.getMI().length);
    }

}
