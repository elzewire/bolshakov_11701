import static java.lang.Math.min;
import static java.util.Arrays.fill;

public class Prim {
    private int INF = Integer.MAX_VALUE / 2; // "Бесконечность"
    private int vNum; // количество вершин
    private int[][] graph; // матрица смежности

    /* Алгоритм Прима за O(V^2) */
    public int mstPrim() {
        boolean[] used = new boolean[vNum]; // массив пометок
        int[] dist = new int[vNum]; // массив расстояния. dist[v] = вес_ребра(MST, v)

        fill(dist, INF); // устанаавливаем расстояние до всех вершин INF
        dist[0] = 0; // для начальной вершины положим 0

        for (; ; ) {
            int v = -1;
            for (int nv = 0; nv < vNum; nv++) { // перебираем вершины
                if (!used[nv] && dist[nv] < INF && (v == -1 || dist[v] > dist[nv])) { // выбираем самую близкую непомеченную вершину
                    v = nv;
                }
            }
            if (v == -1) break; // ближайшая вершина не найдена
            used[v] = true; // помечаем ее
            for (int nv = 0; nv < vNum; nv++) {
                if (!used[nv] && graph[v][nv] < INF) { // для всех непомеченных смежных
                    dist[nv] = min(dist[nv], graph[v][nv]); // улучшаем оценку расстояния (релаксация)
                }
            }
        }
        int ret = 0; // вес MST
        for (int v = 0; v < vNum; v++) {
                ret += dist[v];
        }
        return ret;
    }
}
