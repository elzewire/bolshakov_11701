/* Класс ребра */
public class Edge implements Comparable {
    int u;
    int v;
    int w;

    /* Конструктор */
    Edge(int u, int v, int w) {
        this.u = u;
        this.v = v;
        this.w = w;
    }

    /* Компаратор */
    @Override
    public int compareTo(Object o) {
        if (o instanceof Edge) {
            Edge edge = (Edge) o;
            if (w != edge.w) return w < edge.w ? -1 : 1;
        }
        return 0;
    }
}
