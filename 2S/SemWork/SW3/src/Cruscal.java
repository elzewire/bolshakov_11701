import static java.util.Arrays.sort;

public class Cruscal {
    private int INF = Integer.MAX_VALUE / 2; // "Бесконечность"
    private int vNum; // количество вершин
    private int[][] graph; // матрица смежности

    /* Алгоритм Краскала за O(E log E) */
    int mstKruskal(Edge[] edges) {
        DSF dsf = new DSF(vNum); // СНМ
        sort(edges); // Сортируем ребра
        int ret = 0; // результат
        for (Edge e : edges) // перебираем ребра в порядке возрастания
            if (dsf.union(e.u, e.v)) // если ребра принадлежат разным компонентам
                ret += e.w; // добавляем вес ребра к стоимости MST
        return ret;
    }
}
