package generator;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;

public class Generator {

    public static void main(String[] args) throws IOException {

        String fileName = "res/input.txt";
        BufferedWriter out = new BufferedWriter(new FileWriter(fileName));
        Random rnd = new Random();
        int amount = 50;
        int length = 200;
        String arr = "";

        for (int i = 0; i < amount; i++) {
            //Generate random arrays
            out.write(Integer.toString(length) + " ");
            for (int j = 0; j < length; j++) {
                arr += Integer.toString(rnd.nextInt(length) + 1) + " ";
            }
            out.write(arr);
            out.newLine();
            arr = "";
            length += 200;
        }

        out.close();
    }

}
