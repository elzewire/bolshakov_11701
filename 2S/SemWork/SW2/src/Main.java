import timsort.TimSort;

import java.io.*;
import java.util.*;

public class Main {

    public static void main(String[] args) throws IOException {

        String inputFile = "res/input.txt";

        String outputFile1 = "res/Array/output.txt";
        String iterationsFile1 = "res/Array/iterations.txt";
        String timeFile1 = "res/Array/time.txt";

        String outputFile2 = "res/ArrayList/output.txt";
        String iterationsFile2 = "res/ArrayList/iterations.txt";
        String timeFile2 = "res/ArrayList/time.txt";

        Scanner in = new Scanner(new File(inputFile));

        BufferedWriter out1 = new BufferedWriter(new FileWriter(outputFile1));
        BufferedWriter iter1 = new BufferedWriter(new FileWriter(iterationsFile1));
        BufferedWriter time1 = new BufferedWriter(new FileWriter(timeFile1));
        BufferedWriter out2 = new BufferedWriter(new FileWriter(outputFile2));
        BufferedWriter iter2 = new BufferedWriter(new FileWriter(iterationsFile2));
        BufferedWriter time2 = new BufferedWriter(new FileWriter(timeFile2));

        int length;
        Integer [] arr;

        long startNano;
        long endNano;
        while (in.hasNext()) {
            //Create array
            length = in.nextInt();
            arr = new Integer[length];
            for (int i = 0; i < length; i++) {
                arr[i] = in.nextInt();
            }

            //Sort
            startNano = System.nanoTime();
            TimSort.sort(arr, new Comparator<Integer>() {
                @Override
                public int compare(Integer o1, Integer o2) {
                    return o1.compareTo(o2);
                }
            });
            endNano = System.nanoTime();

            //Write sorted array to output
            for (Integer integer : arr) {
                out1.write(Integer.toString(integer) + " ");
            }
            out1.newLine();

            //Save current sorting result
            iter1.write(Integer.toString(TimSort.iter));
            iter1.newLine();
            time1.write(Long.toString(endNano - startNano));
            time1.newLine();
            TimSort.iter = 0;
        }

        //Sort Array List

        in.close();
        in = new Scanner(new File(inputFile));

        ArrayList<Integer> list;
        while (in.hasNext()) {
            //Create array
            length = in.nextInt();
            list = new ArrayList<>();
            for (int i = 0; i < length; i++) {
                list.add(in.nextInt());
            }

            Integer [] newArr = new Integer[10];
            newArr = list.toArray(newArr);

            //Sort
            startNano = System.nanoTime();
            TimSort.sort(newArr, new Comparator<Integer>() {
                @Override
                public int compare(Integer o1, Integer o2) {
                    return o1.compareTo(o2);
                }

            });

            endNano = System.nanoTime();

            //Write sorted array to output
            for (Integer integer : newArr) {
                out2.write(Integer.toString(integer) + " ");
            }
            out2.newLine();

            //Save current sorting result
            iter2.write(Integer.toString(TimSort.iter));
            iter2.newLine();
            time2.write(Long.toString(endNano - startNano));
            time2.newLine();
            TimSort.iter = 0;
        }

        in.close();
        out1.close();
        iter1.close();
        time1.close();

        out2.close();
        iter2.close();
        time2.close();
    }

}
