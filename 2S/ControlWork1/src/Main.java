/**
 * @author Edward Bolshakov
 * 11-701
 * Control Work 1
 **/


import instruments.CollectionsInitializer;

public class Main {

    public static void main(String[] args) {

        CollectionsInitializer.init();

        //Task 1
        System.out.println("Task 1:");
        System.out.println(Task01.getAppellationsWithMostWinesLoopStyle());
        System.out.println(Task01.getAppellationWithMostWinesAPIStyle());
        System.out.println();

        //Task 2
        System.out.println("Task 2:");
        System.out.println(Task02.getTotalAmountOfWinesForEachGrapeLoopStyle());
        System.out.println(Task02.getTotalAmountOfWinesForEachGrapeAPIStyle());

    }

}