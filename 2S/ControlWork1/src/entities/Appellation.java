package entities;

import java.util.ArrayList;
import java.util.List;

public class Appellation {

    private int id;
    private String name;
    private List<Wine> wines;

    public Appellation(int id, String name) {
        this.id = id;
        this.name = name;
        this.wines = new ArrayList<>();
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public List<Wine> getWines() {
        return wines;
    }

}