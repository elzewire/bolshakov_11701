package entities;

public class Wine {

    private int id;
    private Grape grape;
    private Appellation appellation;
    private String name;

    public Wine(int id, Grape grape, Appellation appellation, String name) {
        this.id = id;
        this.grape = grape;
        this.appellation = appellation;
        this.name = name;
    }

    public Grape getGrape() {
        return grape;
    }

    public Appellation getAppellation() {
        return appellation;
    }

    public String getName() {
        return name;
    }

}