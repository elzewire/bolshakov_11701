import collections_singleton.GrapesCollectionSingleton;
import entities.Grape;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Task02 {

    public static Map<String, Integer> getTotalAmountOfWinesForEachGrapeAPIStyle() {

        return GrapesCollectionSingleton.getGrapesCollection()
                .stream()
                .collect(Collectors.toMap(grape -> grape.getName(), grape -> grape.getWines().size()));

    }

    public static Map<String, Integer> getTotalAmountOfWinesForEachGrapeLoopStyle() {

        Map<String, Integer> map = new HashMap<>();
        List<Grape> grapes = GrapesCollectionSingleton.getGrapesCollection();

        for (Grape grape : grapes) {
            map.put(grape.getName(), grape.getWines().size());
        }

        return map;
    }

}