import collections_singleton.AppellationsCollectionSingleton;
import entities.Appellation;

import java.util.List;

public class Task01 {

    public static String getAppellationWithMostWinesAPIStyle() {

        return AppellationsCollectionSingleton.getAppellationsCollection()
                .stream()
                .reduce((app1, app2) -> app1.getWines().size() > app2.getWines().size() ? app1 : app2)
                .orElse(new Appellation(0, ""))
                .getName();


    }

    public static String getAppellationsWithMostWinesLoopStyle() {

        List<Appellation> appellations = AppellationsCollectionSingleton.getAppellationsCollection();

        if (appellations.size() == 0) {
            return null;
        }

        int max = appellations.get(0).getWines().size();
        String name = appellations.get(0).getName();
        for (Appellation appellation : appellations) {
            if (appellation.getWines().size() > max) {
                max = appellation.getWines().size();
                name = appellation.getName();
            }
        }
        return name;

    }
}