package collections_singleton;

import entities.Appellation;
import entities.Grape;
import entities.Wine;
import instruments.Trimmer;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class WinesCollectionSingleton {

    private static List<Wine> winesCollection;
    private static String filename = "res/wine.csv";

    public static List<Wine> getWinesCollection(){
        if (winesCollection == null) {
            winesCollection = new ArrayList<>();
            try {
                BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(filename)));
                String line = br.readLine();
                line = br.readLine();

                while (line != null) {
                    String[] attrs = Trimmer.trim(line);
                    Appellation appellation = getAppellationByName(attrs[3]);
                    Grape grape = getGrapeByName(attrs[1]);
                    Wine wine = new Wine(Integer.parseInt(attrs[0]), grape, appellation, attrs[5]);
                    winesCollection.add(wine);
                    appellation.getWines().add(wine);
                    grape.getWines().add(wine);
                    line = br.readLine();
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return winesCollection;
    }

    private static Grape getGrapeByName(String name) {

        List<Grape> grapes = GrapesCollectionSingleton.getGrapesCollection();

        for (Grape grape : grapes) {
            if (grape.getName().equals(name)) {
                return grape;
            }
        }

        return null;

    }

    private static Appellation getAppellationByName(String name) {

        List<Appellation> appellations = AppellationsCollectionSingleton.getAppellationsCollection();

        for (Appellation appellation : appellations) {
            if (appellation.getName().equals(name)) {
                return appellation;
            }
        }

        return null;
    }

}