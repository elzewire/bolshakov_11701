package collections_singleton;

import entities.Grape;
import instruments.Trimmer;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class GrapesCollectionSingleton {

    private static List<Grape> grapesCollection;
    private static String filename = "res/grapes.csv";

    public static List<Grape> getGrapesCollection(){
        if (grapesCollection == null) {
            grapesCollection = new ArrayList<>();
            try {
                BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(filename)));
                String line = br.readLine();
                line = br.readLine();

                while (line != null) {
                    String[] attrs = Trimmer.trim(line);
                    Grape grape = new Grape(Integer.parseInt(attrs[0]), attrs[1]);
                    grapesCollection.add(grape);
                    line = br.readLine();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return grapesCollection;
    }

}