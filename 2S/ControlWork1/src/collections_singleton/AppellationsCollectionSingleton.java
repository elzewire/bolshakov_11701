package collections_singleton;

import entities.Appellation;
import instruments.Trimmer;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class AppellationsCollectionSingleton {

    private static List<Appellation> appellationsCollection;
    private static String filename = "res/appellations.csv";

    public static List<Appellation> getAppellationsCollection(){
        if (appellationsCollection == null) {
            appellationsCollection = new ArrayList<>();
            try {
                BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(filename)));
                String line = br.readLine();
                line = br.readLine();

                while (line != null) {
                    String[] attrs = Trimmer.trim(line);
                    Appellation appellation = new Appellation(Integer.parseInt(attrs[0]), attrs[1]);
                    appellationsCollection.add(appellation);
                    line = br.readLine();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return appellationsCollection;
    }

}