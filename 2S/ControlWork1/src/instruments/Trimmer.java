package instruments;

import java.util.ArrayList;
import java.util.List;

public class Trimmer {

    public static String [] trim(String line) {

        List<String> attrsList = new ArrayList<>();

        int s = 1;
        int i = 0;
        String smth = "";
        while (i < line.length()) {
            if (s == 1) {
                if (line.charAt(i) == '\'') {
                    s = 2;
                } else if (line.charAt(i) == ',') {
                    s = 1;
                    attrsList.add(smth);
                    smth = "";
                } else {
                    smth += line.charAt(i);
                    s = 1;
                }
            } else if (s == 2) {
                if (line.charAt(i) != '\'') {
                    smth += line.charAt(i);
                } else {
                    s = 1;
                }
            } else if (s == 4) {
                if (line.charAt(i) == ',') {
                    smth += line.charAt(i);
                } else {
                    attrsList.add(smth);
                    s = 1;
                }
            }
            i++;
        }
        attrsList.add(smth);
        String [] attrs = new String[attrsList.size()];
        attrsList.toArray(attrs);

        return attrs;
    }

}