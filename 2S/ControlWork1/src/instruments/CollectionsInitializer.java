package instruments;

import collections_singleton.AppellationsCollectionSingleton;
import collections_singleton.GrapesCollectionSingleton;
import collections_singleton.WinesCollectionSingleton;

public class CollectionsInitializer {

    public static void init() {
        AppellationsCollectionSingleton.getAppellationsCollection();
        GrapesCollectionSingleton.getGrapesCollection();
        WinesCollectionSingleton.getWinesCollection();
    }

}