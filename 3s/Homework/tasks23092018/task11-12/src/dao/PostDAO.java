package dao;

import db.entities.Post;
import db.entities.User;

import java.util.List;

public interface PostDAO {

    public Post getById(int id);
    public List<Post> getByAuthor(User author);
    public void create(User author, String text);

}
