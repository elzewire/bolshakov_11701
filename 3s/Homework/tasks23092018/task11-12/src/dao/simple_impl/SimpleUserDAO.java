package dao.simple_impl;

import dao.UserDAO;
import db.collections.UsersCollectionSingleton;
import db.entities.User;

import java.util.List;
import java.util.stream.Collectors;

public class SimpleUserDAO implements UserDAO {

    @Override
    public User getById(int id) {
        List<User> users = UsersCollectionSingleton.getUsersCollection();
        for (User user : users) {
            if (user.getId() == id) {
                return user;
            }
        }
        return null;
    }

    @Override
    public List<User> getByName(String name) {
        List<User> users = UsersCollectionSingleton.getUsersCollection();
        return users.stream().filter((user -> user.getName().equals(name))).collect(Collectors.toList());
    }

    @Override
    public User getByUsername(String username) {
        List<User> users = UsersCollectionSingleton.getUsersCollection();
        for (User user : users) {
            if (user.getUsername().equals(username)) {
                return user;
            }
        }
        return null;
    }

}
