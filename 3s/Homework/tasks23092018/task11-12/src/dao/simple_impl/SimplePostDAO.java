package dao.simple_impl;

import dao.PostDAO;
import db.collections.PostsCollectionSingleton;
import db.entities.Post;
import db.entities.User;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class SimplePostDAO implements PostDAO {
    @Override
    public Post getById(int id) {
        List<Post> posts = PostsCollectionSingleton.getPostsCollection();
        for (Post post : posts) {
            if (post.getId() == id) {
                return post;
            }
        }
        return null;
    }

    @Override
    public List<Post> getByAuthor(User author) {
        List<Post> posts = PostsCollectionSingleton.getPostsCollection();
        List<Post> newPosts = new ArrayList<>();
        for (Post post : posts) {
            if (post.getAuthor().equals(author)) {
                newPosts.add(post);
            }
        }
        return newPosts;
    }

    public void create(User user, String text) {
        int nextId = PostsCollectionSingleton.getNextId();
        Post post = new Post(nextId, user, text);
        PostsCollectionSingleton.getPostsCollection().add(post);

        try {
            FileWriter fw = new FileWriter("C:/Users/op_23/Projects/3S/Classwork/task11-12/res/db/posts.txt", true);
            fw.write("\n" + post.getId() + "," + post.getAuthor().getId() + "," + post.getText());
            fw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
