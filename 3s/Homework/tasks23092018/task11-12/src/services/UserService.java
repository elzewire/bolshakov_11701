package services;

import dao.PostDAO;
import dao.UserDAO;
import dao.simple_impl.SimplePostDAO;
import dao.simple_impl.SimpleUserDAO;
import db.entities.Post;
import db.entities.User;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

public class UserService {

    private UserDAO userDAO = new SimpleUserDAO();
    private PostDAO postDAO = new SimplePostDAO();

    public User getCurrentUser(HttpServletRequest req) {
        HttpSession session = req.getSession();
        return (User) session.getAttribute("current_user");
    }

    public User authenticate(HttpServletRequest req) {
        String username = req.getParameter("username");
        if (username != null) {
            User user = userDAO.getByUsername(username);
            if (user == null) {
                return null;
            }
            String password = req.getParameter("password");
            if (password.equals(user.getPassword())) {
                return user;
            }
        }
        return null;
    }

    public void authorize(User current_user, HttpServletRequest req) {
        HttpSession session = req.getSession();
        session.setAttribute("current_user", current_user);
    }

    public List<Post> getUserPosts(User user) {
        return (new SimplePostDAO().getByAuthor(user));
    }

    public boolean validatePost(String text) {
        if (text != null) {
            return true;
        }
        return false;
    }

    public void createPost(User user, String text) {
        postDAO.create(user, text);
    }

    public User getViewedUser(int id) {
        return userDAO.getById(id);
    }
}