package db.collections;

import dao.simple_impl.SimpleUserDAO;
import db.entities.Post;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class PostsCollectionSingleton {

    private static List<Post> postsCollection;
    private static int id = 0;

    public static int getNextId() {
        id++;
        return id;
    }

    public static List<Post> getPostsCollection() {
        if (postsCollection == null) {
            postsCollection = new ArrayList<>();
            try {
                BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream("C:/Users/op_23/Projects/3S/Classwork/task11-12/res/db/posts.txt")));
                String line = br.readLine();
                while (line != null) {
                    String[] attrs = line.split(",");
                    int post_id = Integer.parseInt(attrs[0]);
                    Post post = new Post(post_id, (new SimpleUserDAO()).getById(Integer.parseInt(attrs[1])), attrs[2]);
                    postsCollection.add(post);
                    id = Math.max(id, post_id);
                    line = br.readLine();
                }
            } catch (java.io.IOException e) {
                e.printStackTrace();
            }
        }
        return postsCollection;
    }

}
