package db.collections;

import db.entities.User;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class UsersCollectionSingleton {

    private static List<User> usersCollection;

    public static List<User> getUsersCollection() {
        if (usersCollection == null) {
            usersCollection = new ArrayList<>();
            try {
                BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream("C:/Users/op_23/Projects/3S/Classwork/task11-12/res/db/users.txt")));
                String line = br.readLine();
                while (line != null) {
                    String[] attrs = line.split(",");
                    User user = new User(Integer.parseInt(attrs[0]), attrs[1], attrs[2], attrs[3]);
                    usersCollection.add(user);
                    line = br.readLine();
                }
            } catch (java.io.IOException e) {
                e.printStackTrace();
            }
        }
        return usersCollection;
    }

}
