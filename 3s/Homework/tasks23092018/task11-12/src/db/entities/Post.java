package db.entities;

public class Post {

    private int id;
    private User author;
    private String text;

    public Post(int id, User author, String text) {
        this.id = id;
        this.author = author;
        this.text = text;
    }

    public int getId() {
        return id;
    }

    public User getAuthor() {
        return author;
    }

    public String getText() {
        return text;
    }
}
