import dao.entities.User;
import helpers.Helper;
import services.UserService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class CreatePostServlet extends HttpServlet {

    private UserService userService = new UserService();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (userService.getCurrentUser(req) != null) {
            resp.setContentType("text/html");
            Helper.render(getServletContext(), resp, "post.ftl");
        } else {
            resp.sendRedirect("/login");
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (userService.getCurrentUser(req) != null) {
            String text = req.getParameter("text");
            User user = (User) req.getSession().getAttribute("current_user");
            if (userService.validatePost(text)) {
                userService.createPost(user, text);
                resp.sendRedirect("/profile");
            } else {
                resp.sendRedirect("/post");
            }
        }
    }

}
