package helpers;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class Helper {

    public static void render(ServletContext sc, HttpServletResponse resp, String path, Map<String, Object> root) {
        Configuration cfg = ConfigSingleton.getConfig(sc);
        Template tmpl = null;
        try {
            tmpl = cfg.getTemplate(path);
            tmpl.process(root, resp.getWriter());
        } catch (TemplateException | IOException e) {
            e.printStackTrace();
        }
    }

    public static void render(ServletContext sc, HttpServletResponse resp, String path) {
        Configuration cfg = ConfigSingleton.getConfig(sc);
        Template tmpl = null;
        try {
            tmpl = cfg.getTemplate(path);
            tmpl.process(new HashMap<String, Object>(), resp.getWriter());
        } catch (TemplateException | IOException e) {
            e.printStackTrace();
        }
    }
}
