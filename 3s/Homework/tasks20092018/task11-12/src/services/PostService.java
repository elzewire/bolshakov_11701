package services;

import dao.PostDAO;
import dao.UserDAO;
import dao.postgres_impl.PostgresPostDAO;
import dao.postgres_impl.PostgresUserDAO;
import dao.entities.Post;

import java.util.List;
import java.util.regex.Pattern;

public class PostService {
    private UserDAO userDAO = new PostgresUserDAO();
    private PostDAO postDAO = new PostgresPostDAO(userDAO);


    public int getId(String rel_path) {
        Pattern p = Pattern.compile("/([0-9]+)?<id>");
        if (p.matcher(rel_path).matches()) {
            String id = p.matcher(rel_path).group("id");
            return Integer.parseInt(id);
        }
        return -1;
    }

    public boolean hasId(String rel_path) {
        Pattern p = Pattern.compile("/([0-9]+)?<id>");
        return p.matcher(rel_path).matches();
    }

    public Post getPostById(int id) {
        return postDAO.getById(id);
    }

    public List<Post> getPosts() {
        return postDAO.getAll();
    }
}
