package dao.postgres_impl;

import dao.PostDAO;
import dao.UserDAO;
import dao.entities.Post;
import dao.entities.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class PostgresPostDAO implements PostDAO{

    private Connection conn;
    private UserDAO userDAO;

    public PostgresPostDAO(UserDAO userDAO) {
        try {
            this.conn = DriverManager.getConnection("jdbc:postgresql://localhost:5433/classwork", "postgres", "Postgres");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Post getById(int id) {
        Post post = null;
        try {
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery("SELECT * FROM post WHERE \"id\" = " + id + ";");
            while (rs.next()) {
                post = new Post(rs.getInt("id"),
                        userDAO.getById(rs.getInt("author_id")),
                        rs.getString("text"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return post;
    }

    @Override
    public List<Post> getByAuthor(User author) {
        List<Post> posts = null;
        try {
            int id = author.getId();
            posts = new ArrayList<>();
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery("SELECT * FROM post WHERE author_id = " + id + ";");
            while (rs.next()) {
                posts.add(new Post(rs.getInt("id"),
                        userDAO.getById(rs.getInt("author_id")),
                        rs.getString("text")));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return posts;
    }

    @Override
    public void createPost(User user, String text) {
        try {
            Statement st = conn.createStatement();
            st.execute("INSERT INTO post (author_id, text) values " +
                    "('" + user.getId() + "','" + text + "');");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void updatePost(Post post) {
        try {
            Statement st = conn.createStatement();
            st.execute("UPDATE post SET text = '" + post.getText() + "' WHERE \"id\" = '" + post.getId() + "';");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deletePost(int id) {
        try {
            Statement st = conn.createStatement();
            st.execute("DELETE FROM post WHERE \"id\" = '" + id + "';");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<Post> getAll() {
        List<Post> posts = null;
        try {
            posts = new ArrayList<>();
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery("SELECT * FROM post;");
            while (rs.next()) {
                posts.add(new Post(rs.getInt("id"),
                        userDAO.getById(rs.getInt("author_id")),
                        rs.getString("text")));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return posts;
    }
}
