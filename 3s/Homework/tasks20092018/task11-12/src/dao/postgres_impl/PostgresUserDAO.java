package dao.postgres_impl;

import dao.UserDAO;
import dao.entities.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class PostgresUserDAO implements UserDAO{

    private Connection conn;

    public PostgresUserDAO() {
        try {
            this.conn = DriverManager.getConnection("jdbc:postgresql://localhost:5433/classwork", "postgres", "Postgres");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public User getById(int id) {
        User user = null;
        try {
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery("SELECT * FROM \"user\" WHERE \"id\" = " + id + ";");
            while (rs.next()) {
                user = new User(rs.getInt("id"),
                        rs.getString("name"),
                        rs.getString("username"),
                        rs.getString("password"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return user;
    }

    @Override
    public User getByUsername(String username) {
        User user = null;
        try {
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery("SELECT * FROM \"user\" WHERE username = " + username + ";");
            while (rs.next()) {
                user = new User(rs.getInt("id"),
                        rs.getString("name"),
                        rs.getString("username"),
                        rs.getString("password"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return user;
    }

    @Override
    public List<User> getByName(String name) {
        List<User> users = null;
        try {
            users = new ArrayList<>();
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery("SELECT * FROM \"user\" WHERE \"name\" = " + name + ";");
            while (rs.next()) {
                users.add(new User(rs.getInt("id"),
                        rs.getString("name"),
                        rs.getString("username"),
                        rs.getString("password")));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return users;
    }

    @Override
    public void createUser(String username, String password, String name) {
        try {
            Statement st = conn.createStatement();
            st.execute("INSERT INTO \"user\" (username, \"password\", \"name\")" +
                    " values ('" + username + "', '" + password + "', '" + name + "');");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void updateUser(int id) {

    }

    @Override
    public void deleteUser(int id) {

    }
}
