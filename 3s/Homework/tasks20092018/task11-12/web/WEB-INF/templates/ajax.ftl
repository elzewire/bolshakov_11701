<!DOCTYPE html>
<html>
<head>
	<title>Ajax</title>
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.js"></script>
</head>
<body>
	<button type="button" onclick="getNumber()">Get secret number!</button>
	<p id="number"></p>
	<script type="text/javascript">
		function getNumber() {
			$.ajax({
				type: "post",
				url : "/ajax",
				dataType: "json",
				success : function(data) {
					$("#number").text(data.number)
				}
			});
		}
	</script>
</body>
</html> 	