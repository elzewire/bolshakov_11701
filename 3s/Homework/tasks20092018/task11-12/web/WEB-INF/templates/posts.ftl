<html>
    <head>
        <title>Post</title>
    </head>
    <body>
        <#if posts?has_content>
            <#list posts as p>
                <p><h3>#{p.author.name}</h3></p>
                <p>#{p.text}</p>
            </#list>
        <#else>
            No posts!
        </#if>
    </body>
</html>