<html>
    <head>
        <script type="application/javascript" src="/js/jquery-1.9.1.js"></script>
    </head>
    <body>
        <p><input type="text" id="text-input" oninput="g()"/></p>
        <div id="found-list">

        </div>
        <script type="application/javascript">
            function g() {
                if ($("#text-input").val().length > 0) {
                    $.ajax({
                        url: "/doSearch",
                        type: "get",
                        dataType: "json",
                        data: {"q": $("#text-input").val()},
                        success: function (data) {
                            var lst = $("#found-list");
                            lst.html("");
                            for (var i = 0; i < data.students.length; i++) {
                                lst.append("<li>")
                            }
                        }
                    })
                } else {
                    $("#found-list").html("");
                }
            }
        </script>
    </body>
</html>