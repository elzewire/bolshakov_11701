package helpers;

import entities.Student;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import freemarker.template.TemplateExceptionHandler;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;
import javax.sql.ConnectionEvent;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainHelper {

    private static final String URL = "jdbc:postgresql://localhost:5433/classwork";
    private static final String USERNAME = "postgres";
    private static final String PASSWORD = "Postgres";
    private static Connection conn;
    private static Configuration cfg;

    public static Connection getConn() {
        try {
            Class.forName("org.postgresql.Driver");
            conn = DriverManager.getConnection(
                    URL,
                    USERNAME,
                    PASSWORD
            );
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }

        return conn;
    }

    public static Student makeORMStudent(ResultSet rs) {
        try {
            rs.next();
            return new Student(
                rs.getLong("id"),
                rs.getString("name"),
                rs.getInt("year"),
                rs.getString("city")
            );
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static List<Student> makeORMStudentList(ResultSet rs) {
        List<Student> students = new ArrayList<>();
        Student s = makeORMStudent(rs);
        while (s != null) {
            students.add(s);
            s = makeORMStudent(rs);
        }
        return students;
    }

    private static Configuration getConfig(ServletContext sc) {
        if (cfg == null) {
            cfg = new Configuration();
            cfg.setServletContextForTemplateLoading(sc, "WEB-INF/templates");
            cfg.setTemplateExceptionHandler(
                    TemplateExceptionHandler.HTML_DEBUG_HANDLER
            );
        }
        return cfg;
    }

    public static void render(ServletContext sc, HttpServletResponse resp, String path, Map<String, Object> root) {
        Configuration cfg = MainHelper.getConfig(sc);
        Template tmpl = null;
        try {
            tmpl = cfg.getTemplate(path);
            tmpl.process(root, resp.getWriter());
        } catch (TemplateException | IOException e) {
            e.printStackTrace();
        }
    }

    public static void render(ServletContext sc, HttpServletResponse resp, String path) {
        Configuration cfg = MainHelper.getConfig(sc);
        Template tmpl = null;
        try {
            tmpl = cfg.getTemplate(path);
            tmpl.process(new HashMap<String, Object>(), resp.getWriter());
        } catch (TemplateException | IOException e) {
            e.printStackTrace();
        }
    }

}
