package dao;

import java.util.List;

public interface DAO<T> {
    T get(long id);
    void update(T obj);
    void delete(long id);
    void create(T obj);
    List<T> getAll();
}
