package dao.impl;

import dao.DAO;
import dao.DAOStudent;
import entities.Student;
import helpers.MainHelper;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class StudentDAOjdbc implements DAOStudent {
    @Override
    public Student get(long id) {
        Connection conn = MainHelper.getConn();
        try {
            PreparedStatement ps = conn.prepareStatement(
                    "select * from students where id = ?;"
            );
            ps.setString(1, Long.toString(id));
            ResultSet rs = ps.executeQuery();
            return MainHelper.makeORMStudent(rs);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void update(Student obj) {

    }

    @Override
    public void delete(long id) {

    }

    @Override
    public void create(Student obj) {

    }

    @Override
    public List<Student> getAll() {
        return null;
    }

    @Override
    public List<Student> getStudentsByNameMask(String q) {
        Connection conn = MainHelper.getConn();
        try {
            PreparedStatement ps = conn.prepareStatement(
                    "select * from students where name like ?"
            );
            ps.setString(1, "%" + q + "%");
            ResultSet rs = ps.executeQuery();
            return MainHelper.makeORMStudentList(rs);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
