package login.cookie;

import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.IOException;
import java.util.Objects;

public class CookieLoginServlet extends HttpServlet{

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Cookie[] cookies = req.getCookies();
        String user = null;
        for (Cookie cookie : cookies) {
            if (cookie.getName().equals("remembered")) {
                user = cookie.getValue();
                break;
            }
        }
        if (user == null) {
            resp.setContentType("text/html");
            resp.getWriter().write("<form method=\"POST\">\n" +
                    "\t<input type=\"text\" name=\"user\">\n" +
                    "\t<button type=\"submit\">login</button>\n" +
                    "\t<label>Remember me</label>\n" +
                    "\t<input type=\"checkbox\" name=\"remember\">\n" +
                    "</form>");
        } else {
            resp.sendRedirect("/profile");
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String user = req.getParameter("user");
        String remember = req.getParameter("remember");
        if (remember != null) {
            Cookie cookie = new Cookie("remembered", user);
        }
        resp.sendRedirect("/profile");
    }
}
