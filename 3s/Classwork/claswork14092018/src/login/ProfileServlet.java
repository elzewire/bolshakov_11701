package login;

import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.IOException;

public class ProfileServlet extends HttpServlet{

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        String user = (String) session.getAttribute("user");
        if (user != null) {
            resp.setContentType("text/html");
            resp.getWriter().write("<h1>" + user + "</h1>");
        } else {
            resp.sendRedirect("/login");
        }
    }
}
