package login;

import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.IOException;

public class LoginServlet extends HttpServlet{

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        String user = (String) session.getAttribute("user");
        if (user == null) {
            resp.setContentType("text/html");
            resp.getWriter().write("<form method=\"POST\">\n" +
                    "\t<input type=\"text\" name=\"user\">\n" +
                    "\t<button type=\"submit\">login</button>\n" +
                    "\t<label>Remember me</label>\n" +
                    "\t<input type=\"checkbox\" name=\"remember\">\n" +
                    "</form>");
        } else {
            resp.sendRedirect("/profile");
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        String user = req.getParameter("user");
        session.setAttribute("user", user);
        resp.sendRedirect("/profile");
    }
}
