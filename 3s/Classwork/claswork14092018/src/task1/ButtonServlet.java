package task1;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ButtonServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String name = req.getParameter("name");
        resp.setContentType("text/html");
        String hello = "<h1>Hello</h1>";
        if (name != null) {
            hello = "<h1>Hello, " + name + "</h1>";
        }
        resp.getWriter().print(hello +
                "<form method='GET'>\n" +
                "\t<input type=\"text\" name='name'>\n" +
                "\t<button type=\"submit\">ok</button>\n" +
                "</form>");
        resp.getWriter().close();
    }
}
